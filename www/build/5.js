webpackJsonp([5],{

/***/ 379:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginLoginEsqueciPageModule", function() { return LoginLoginEsqueciPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login_login_esqueci__ = __webpack_require__(390);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var LoginLoginEsqueciPageModule = /** @class */ (function () {
    function LoginLoginEsqueciPageModule() {
    }
    LoginLoginEsqueciPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__login_login_esqueci__["a" /* LoginLoginEsqueciPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__login_login_esqueci__["a" /* LoginLoginEsqueciPage */]),
            ],
        })
    ], LoginLoginEsqueciPageModule);
    return LoginLoginEsqueciPageModule;
}());

//# sourceMappingURL=login-login-esqueci.module.js.map

/***/ }),

/***/ 390:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginLoginEsqueciPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_auth__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var LoginLoginEsqueciPage = /** @class */ (function () {
    function LoginLoginEsqueciPage(navCtrl, navParams, alertCtrl, auth, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.auth = auth;
        this.loadingCtrl = loadingCtrl;
        this.form = {
            Email: ''
        };
    }
    LoginLoginEsqueciPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginLoginEsqueciPage');
    };
    LoginLoginEsqueciPage.prototype.EsqueciSubmit = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: 'Carregando...'
        });
        var alert = this.alertCtrl.create({
            title: "OK",
            buttons: [
                {
                    text: 'Fechar',
                    role: 'cancel'
                }
            ]
        });
        loading.present();
        this.auth.postResetPassWord(this.form.Email)
            .then(function (response) {
            setTimeout(function () {
                loading.dismiss();
                alert.setTitle(response.toString());
                alert.present();
            }, 2000);
        }, function (error) {
            console.log(error);
            setTimeout(function () {
                if (error.status == 400) {
                    console.log('aaaaa');
                    _this.alertCtrl.create({
                        title: 'Erro',
                        subTitle: error.error.Message,
                        buttons: [{ text: 'Ok' }]
                    }).present();
                }
            }, 1000);
            loading.dismiss();
        });
        ;
    };
    LoginLoginEsqueciPage.prototype.goLogin = function () {
        this.navCtrl.setRoot('login');
    };
    LoginLoginEsqueciPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-login-login-esqueci',template:/*ion-inline-start:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/login/login-login-esqueci/login-login-esqueci.html"*/'<ion-content padding class="padding-fixed" color="light">\n  <header class="login-header" text-center>\n      <img src="assets/imgs/logo.png" class="brand">\n      <h3 class="title-yellow">Esqueci minha senha</h3>\n      <div class="block-text">\n        <p>Digite abaixo seu e-mail cadastrado, para que você possa alterar sua senha. Iremos te enviar um e-mail para redefinição de senha.</p>\n      </div>\n  </header>\n  <ion-list>\n\n    <ion-item>\n\n\n      <ion-input type="text" color="light" placeholder="E-mail" [(ngModel)]="form.Email"></ion-input>\n    </ion-item>\n\n  </ion-list>\n  <footer margin-top padding-top>\n    <button ion-button full text-uppercase margin-top (click)="EsqueciSubmit(form)">Enviar E-mail</button>\n    <button ion-button color="white" full outline text-uppercase margin-top (click)="goLogin()">Cancelar</button>\n  </footer>\n</ion-content>\n'/*ion-inline-end:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/login/login-login-esqueci/login-login-esqueci.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */]])
    ], LoginLoginEsqueciPage);
    return LoginLoginEsqueciPage;
}());

//# sourceMappingURL=login-login-esqueci.js.map

/***/ })

});
//# sourceMappingURL=5.js.map