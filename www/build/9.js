webpackJsonp([9],{

/***/ 375:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CadastroTipoPerfilPageModule", function() { return CadastroTipoPerfilPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__cadastro_tipoperfil__ = __webpack_require__(386);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_brmasker_ionic_3__ = __webpack_require__(64);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var CadastroTipoPerfilPageModule = /** @class */ (function () {
    function CadastroTipoPerfilPageModule() {
    }
    CadastroTipoPerfilPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__cadastro_tipoperfil__["a" /* CadastroTipoPerfilPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__cadastro_tipoperfil__["a" /* CadastroTipoPerfilPage */]),
                __WEBPACK_IMPORTED_MODULE_3_brmasker_ionic_3__["a" /* BrMaskerModule */]
            ],
        })
    ], CadastroTipoPerfilPageModule);
    return CadastroTipoPerfilPageModule;
}());

//# sourceMappingURL=cadastro-tipoperfil.module.js.map

/***/ }),

/***/ 386:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CadastroTipoPerfilPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the CadastroPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CadastroTipoPerfilPage = /** @class */ (function () {
    function CadastroTipoPerfilPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    CadastroTipoPerfilPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CadastroPage');
    };
    CadastroTipoPerfilPage.prototype.goPage = function (page) {
        this.navCtrl.push(page);
    };
    CadastroTipoPerfilPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-cadastrotipoperfil',template:/*ion-inline-start:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/cadastro/cadastro-tipoperfil.html"*/'<ion-content>\n  <div padding>\n    <header class="login-header" text-center>\n        <img src="http://via.placeholder.com/100x90" class="brand">\n        <h3 class="title-blue">Cadastre-se</h3>\n    </header>\n    <div class="container-stage disabled">\n      <div class="number">1</div>\n      <div class="text">\n        <strong>Preencha seus dados iniciais:</strong>\n        <p>*Todos os dados devem ser preenchidos</p>\n      </div>\n    </div>\n    <div class="container-stage">\n      <div class="number">2</div>\n      <div class="text">\n        <strong>Selecione seu tipo de perfil:</strong>\n        <p>De acordo com o perfil iremos selecionar a melhor maneira de passar informações.</p>\n      </div>\n    </div>\n    <ul class="card-options">\n        <li class="select">\n          <ion-icon name="arrow-dropright-circle"></ion-icon>\n          <strong>Quero receber informações do projeto, mas não sei se vou participar.</strong>\n        </li>\n        <li>\n          <ion-icon name="arrow-dropright-circle"></ion-icon>\n          <strong>Estou convencido e quero participar!</strong>\n        </li>\n        <li>\n          <ion-icon name="arrow-dropright-circle"></ion-icon>\n          <strong>Vou participar e serei voluntário</strong>\n        </li>\n        <li>\n          <ion-icon name="arrow-dropright-circle"></ion-icon>\n          <strong>Quero ser voluntário e me filiar ao partido</strong>\n        </li>\n    </ul>\n    <button ion-button full text-uppercase>Enviar</button>\n  </div>\n  \n</ion-content>\n<ion-footer>\n  <ion-toolbar (click)="goPage(\'cadastro\')" text-center>\n    Ou, volta na <strong>etapa anterior</strong>\n  </ion-toolbar>\n</ion-footer>\n\n\n'/*ion-inline-end:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/cadastro/cadastro-tipoperfil.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */]])
    ], CadastroTipoPerfilPage);
    return CadastroTipoPerfilPage;
}());

//# sourceMappingURL=cadastro-tipoperfil.js.map

/***/ })

});
//# sourceMappingURL=9.js.map