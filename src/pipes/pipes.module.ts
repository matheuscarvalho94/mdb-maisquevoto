import { NgModule } from '@angular/core';
import { ExcerptPipe } from './excerpt/excerpt';
import { NotificationsPipe } from './notifications/notifications';
import { SearchPipe } from './search/search';
import { SearchCategoriasPipe } from './search-categorias/search-categorias';
@NgModule({
	declarations: [ExcerptPipe,
    NotificationsPipe,],
	imports: [],
	exports: [ExcerptPipe,
    NotificationsPipe,]
})
export class PipesModule {}
