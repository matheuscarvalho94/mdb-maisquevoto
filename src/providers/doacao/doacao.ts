import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CONFIG_PROJECT } from '../app-config';
import { AuthProvider } from '../auth/auth';

@Injectable()
export class DoacaoProvider {

  constructor(public http: HttpClient, public auth: AuthProvider) {}

  getList() {
    var token = this.auth.getToken();
    let url = `${CONFIG_PROJECT.baseApi}/Doacao/Candidatos`;
    let header = { "headers": { "Content-Type": 'application/json', "Authorization": `Bearer ${token}` } };

    return new Promise((resolve, reject) => {
      this.http.get(url, header)
        .map(res => res)
        .subscribe((result: any) => {
          resolve(result);
        },
        (error) => {
          reject(error);
        });
    });
  }

  get() {

    var token = this.auth.getToken();
    let url = `${CONFIG_PROJECT.baseApi}/Doacao/Config`;
    let header = { "headers": { "Content-Type": 'application/json', "Authorization": `Bearer ${token}` } };

    return new Promise((resolve, reject) => {
      this.http.get(url, header)
        .map(res => res)
        .subscribe((result: any) => {
          resolve(result);
        },
        (error) => {
          reject(error);
        });
    });
  }

  post(doacao)
  {
    var token = this.auth.getToken();
    let url = `${CONFIG_PROJECT.baseApi}/Doacao`;
    let header = { "headers": { "Content-Type": 'application/json', "Authorization": `Bearer ${token}` } };

    return new Promise((resolve, reject) => {
      this.http.post(url,doacao, header)
        .map(res => res)
        .subscribe((result: any) => {
          resolve(result);
        },
        (error) => {
          reject(error);
        });
    });
  }

}