import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CONFIG_PROJECT } from '../app-config';
import { AuthProvider } from '../auth/auth';

@Injectable()
export class ConteudoProvider {

  constructor(public http: HttpClient, public auth: AuthProvider) {}

  getPage(token:string, idPage:number,) {

    let url = `${CONFIG_PROJECT.baseApi}/Paginas/${idPage}`;
    let header = { "headers": { "Content-Type": 'application/json', "Authorization": `Bearer ${token}` } };

    return new Promise((resolve, reject) => {
      this.http.get(url, header)
        .map(res => res)
        .subscribe((result: any) => {
          resolve(result);
        },
        (error) => {
          reject(error);
        });
    });
  }

  getConteudo(token:string, pag:number, skip:number, tipo:number) {

    let url = `${CONFIG_PROJECT.baseApi}/Conteudos?TipoConteudoId=${tipo}&$skip=${skip}&$top=${pag}`;
    let header = { "headers": { "Content-Type": 'application/json', "Authorization": `Bearer ${token}` } };

    return new Promise((resolve, reject) => {
      this.http.get(url, header)
        .map(res => res)
        .subscribe((result: any) => {
          resolve(result);
        },
        (error) => {
          reject(error);
        });
    });
  }

  getTiposConteudo(token:string) {

    let url = `${CONFIG_PROJECT.baseApi}/TiposConteudo`;
    let header = { "headers": { "Content-Type": 'application/json', "Authorization": `Bearer ${token}` } };

    return new Promise((resolve, reject) => {
      this.http.get(url, header)
        .map(res => res)
        .subscribe((result: any) => {
          resolve(result);
        },
        (error) => {
          reject(error);
        });
    });
  }

  getDetalhe(token:string, id:number) {
    
    let url = `${CONFIG_PROJECT.baseApi}/Conteudos/${id}`;
    let header = { "headers": { "Content-Type": 'application/json', "Authorization": `Bearer ${token}` } };
    
    return new Promise((resolve, reject) => {
      this.http.get(url, header)
        .map(res => res)
        .subscribe((result: any) => {
          resolve(result);
        },
        (error) => {
          reject(error);
        });
    });

  }

  postLike(conteudoId:number, pessoaId:number) {
    let url = `${CONFIG_PROJECT.baseApi}/Conteudos/Curtir`;
    var token = this.auth.getToken();

    let body = {
      ConteudoId: conteudoId,
      PessoaId: pessoaId
    }
    let header = { "headers": { "authorization": 'bearer ' + token } };

    return new Promise((resolve, reject) => {
      this.http.post(url, body, header)
        .map(res => res)
        .subscribe((result: any) => {
          result.Titulo == "Conteúdo curtido com sucesso!" ? result.Curtiu = true : result.Curtiu = false
          resolve(result);
        },
        (error) => {
          reject(error);
        });
    });


  }

  postShare(conteudoId:number, pessoaId:number, rede: string) {

    
    let url = `${CONFIG_PROJECT.baseApi}/Conteudos/Compartilhar`;
    var token = this.auth.getToken();

    let body = {
      ConteudoId: conteudoId,
      PessoaId: pessoaId,
      RedeSocial: rede
    }
    
    let header = { "headers": { "authorization": 'bearer ' + token } };

    return new Promise((resolve, reject) => {
      this.http.post(url, body, header)
        .map(res => res)
        .subscribe((result: any) => {
          resolve(result);
        },
        (error) => {
          reject(error);
        });
    });
}

}
