import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CONFIG_PROJECT } from '../app-config';
import {Comite} from "../../models/Comite";

@Injectable()
export class ComiteProvider {

  constructor(private http: HttpClient) {
  }

  private getHeader(token: string) {
    return {
      "headers": {
        "Content-Type": 'application/json',
        "Authorization": `Bearer ${token}`
      }
    };
  }

  private getPromise<T>(url, header): Promise<T> {
    return new Promise((resolve, reject) => {
      this.http.get(url, header)
        .map(res => res)
        .subscribe((result) => {
            resolve(result as any);
          },
          (error) => {
            reject(error);
          });
    });
  }

  findAll(token: string): Promise<Comite[]> {
    let url = `${CONFIG_PROJECT.baseApi}/Comites`,
        header = this.getHeader(token);

    return this.getPromise<Comite[]>(url, header);
  }

  findById(token: string, comiteId: number): Promise<Comite> {
    let url = `${CONFIG_PROJECT.baseApi}/Comites/${comiteId}`,
      header = this.getHeader(token);

    return this.getPromise<Comite>(url, header);
  }
}
