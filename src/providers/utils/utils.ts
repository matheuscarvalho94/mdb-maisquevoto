import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AlertController, Platform, ModalController } from 'ionic-angular';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { ModalSucessoPage } from '../../pages/modal-sucesso/modal-sucesso';
import { FileOpener } from '@ionic-native/file-opener';

declare var cordova: any;

@Injectable()
export class UtilsProvider {


  constructor(
    private http: HttpClient,
    private alertCtrl: AlertController,
    private transfer: FileTransfer,
    public platform: Platform,
    private androidPermissions: AndroidPermissions,
    public modalCtrl: ModalController,
    private fileOpener: FileOpener
  ) {
    this.initialize();

  }

  private estados: any = null;

  initialize() {
    this.http.get('assets/estados-cidades.json')
      .subscribe((result: any) => {
        this.estados = result.sigla;
      }, error => {
        console.error('Error: ', error);
      });
  }

  getEstados() {
    if (this.estados) {
      return new Promise((resolve, reject) => {
        resolve(this.estados);
      });
    }

    return new Promise((resolve, reject) => {
      let url = 'assets/estados-cidades.json';
      this.http.get(url)
        .map(res => res)
        .subscribe((result: any) => {
          resolve(result.estados);
        },
          (error) => {
            reject(error);
          });
    });
  }

  getCidades(uf: string) {
    if (this.estados) {
      return new Promise((resolve, reject) => {
        let cidades = [];
        for (var i = 0; i < this.estados.length; i++) {
          if (this.estados[i].sigla == uf) {
            cidades = this.estados[i].cidades;
            break;
          }
        }
        resolve(cidades);
      });
    }

    return new Promise((resolve, reject) => {
      let url = 'assets/estados-cidades.json';
      this.http.get(url)
        .map(res => res)
        .subscribe((result: any) => {
          let cidades = [];
          for (var i = 0; i < result.estados.length; i++) {
            if (result.estados[i].sigla == uf) {
              cidades = result.estados[i].cidades;
              break;
            }
          }
          resolve(cidades);
        },
          (error) => {
            reject(error);
          });
    });
  }

  getNameFromUf(uf: string) {
    let nome = uf;
    if (this.estados) {
      for (var i = 0; i < this.estados.length; i++) {
        if (this.estados[i].sigla == uf) {
          nome = this.estados[i].nome;
          break;
        }
      }
    }
    return nome;
  }

  getFromUf(uf: string) {
    let estado = null;
    if (this.estados) {
      for (var i = 0; i < this.estados.estados.length; i++) {
        if (this.estados.estados[i].sigla == uf) {
          estado = this.estados.estados[i];
          break;
        }
      }
    }
    return estado;
  }

  showAlert(text, message = undefined) {
    let alert = this.alertCtrl.create({
      title: "OK",
      buttons: [
        {
          text: 'Fechar',
          role: 'cancel'
        }
      ]
    })
    alert.setTitle(text);
    if (message != undefined && message != '' && message != null) {
      alert.setMessage(message);
    }
    alert.present();
  }

  showModalSucesso(titulo, mensagem, botao, clbk) {
    let modal = this.modalCtrl.create(ModalSucessoPage, { Titulo: titulo, Mensagem: mensagem, Btn: botao, error: false });
    modal.present();
    if (clbk != null) {
      modal.onDidDismiss(clbk);
    }
  }

  showModalError(titulo, mensagem, botao) {
    let modal = this.modalCtrl.create(ModalSucessoPage, { Titulo: titulo, Mensagem: mensagem, Btn: botao, error: true });
    modal.present();
  }

  downloadFile(arquivo: any, clbk) {
    if (!this.platform.is('cordova')) {
      return false;
    }

    let fileTransfer: FileTransferObject = this.transfer.create();
    let fileName = arquivo.Url.substring(arquivo.Url.lastIndexOf('/') + 1);

    let directory = '';

    if (this.platform.is('ios')) {
      directory = cordova.file.documentsDirectory;
    }
    else if (this.platform.is('android')) {
      directory = cordova.file.dataDirectory;
    }

    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE)
      .then(result => {
        console.log(result.hasPermission)
        if (!result.hasPermission) {
          this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE)
          clbk.dismiss();
        }
        else {
          fileTransfer.download(arquivo.Url, directory + fileName).then((entry) => {
            clbk.dismiss();
            this.showAlert('Download concluído');
            console.log('download complete: ' + entry.toURL());
          }, (error) => {
            clbk.dismiss();
            this.showAlert('Erro no download');
            console.log(error);
            // handle error
          });
        }
      },
        err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE)

      );
  }

  openfile(directory: string, arquivo: any) {
    this.fileOpener.open(directory, arquivo.MimeType)
      .then(() => console.log('File is opened'))
      .catch(e => console.log('Error opening file', e));
  }

  cpf(cpf: string): boolean {
    if (cpf == null) {
      return false;
    }
    if (cpf.length != 11) {
      return false;
    }
    if ((cpf == '00000000000') || (cpf == '11111111111') || (cpf == '22222222222') || (cpf == '33333333333') || (cpf == '44444444444') || (cpf == '55555555555') || (cpf == '66666666666') || (cpf == '77777777777') || (cpf == '88888888888') || (cpf == '99999999999')) {
      return false;
    }
    let numero: number = 0;
    let caracter: string = '';
    let numeros: string = '0123456789';
    let j: number = 10;
    let somatorio: number = 0;
    let resto: number = 0;
    let digito1: number = 0;
    let digito2: number = 0;
    let cpfAux: string = '';
    cpfAux = cpf.substring(0, 9);
    for (let i: number = 0; i < 9; i++) {
      caracter = cpfAux.charAt(i);
      if (numeros.search(caracter) == -1) {
        return false;
      }
      numero = Number(caracter);
      somatorio = somatorio + (numero * j);
      j--;
    }
    resto = somatorio % 11;
    digito1 = 11 - resto;
    if (digito1 > 9) {
      digito1 = 0;
    }
    j = 11;
    somatorio = 0;
    cpfAux = cpfAux + digito1;
    for (let i: number = 0; i < 10; i++) {
      caracter = cpfAux.charAt(i);
      numero = Number(caracter);
      somatorio = somatorio + (numero * j);
      j--;
    }
    resto = somatorio % 11;
    digito2 = 11 - resto;
    if (digito2 > 9) {
      digito2 = 0;
    }
    cpfAux = cpfAux + digito2;
    if (cpf != cpfAux) {
      return false;
    }
    else {
      return true;
    }
  }

  getCep(cep: any) {

    return new Promise((resolve, reject) => {
      this.http.get('https://viacep.com.br/ws/' + cep + '/json/')
        .map(res => res)
        .subscribe((result: any) => {
          resolve(result);
        },
          (error) => {
            reject(error);
          });
    });
  }
}
