import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Platform } from 'ionic-angular';
import { ConteudoProvider } from '../conteudo/conteudo';
import { UtilsProvider } from '../utils/utils';


@Injectable()
export class SharedService {

    constructor(
        private SocialSharing: SocialSharing,
        public platform: Platform,
        public conteudoprovider: ConteudoProvider,
        public utils: UtilsProvider) {
        console.log('Hello SharedServiceProvider Provider');
    }

    SwitchShare(conteudo, rede) {
        switch (rede) {
            case 'facebook':
                this.shareViaFacebook(conteudo);
                break;
            case 'twitter':
                this.shareViaTwitter(conteudo);
                break;
            case 'instagram':
                this.shareViaInstagram(conteudo);
                break;
            case 'whatsapp':
                this.shareViaWhatsApp(conteudo);
                break;
            default:
                this.sharePicker(conteudo);
                break;
        }
    }

    shareViaEmail(conteudo) {
        this.platform.ready()
            .then(() => {
                this.SocialSharing.canShareViaEmail()
                    .then(() => {
                        this.SocialSharing.shareViaEmail(conteudo.message, conteudo.subject, conteudo.sendTo)
                            .then((data) => {
                                console.log('Shared via Email');
                            })
                            .catch((err) => {
                                console.log('Not able to be shared via Email');
                            });
                    })
                    .catch((err) => {
                        console.log('Sharing via Email NOT enabled');
                    });
            });
    }

    shareViaFacebook(conteudo) {
        this.platform.ready()
            .then(() => {
                this.SocialSharing.canShareVia('facebook', conteudo.Titulo, conteudo.Titulo, conteudo.Imagem)
                    .then((data) => {

                        this.SocialSharing.shareViaFacebook(conteudo.Titulo, conteudo.Imagem)
                            .then((data) => {
                                console.log('Shared via Facebook');
                                this.conteudoprovider.postShare(conteudo.Id, conteudo.PessoaId, 'facebook')
                                    .then((response: any) => {
                                        if (response.GanhouPonto) {
                                            this.utils.showModalSucesso(response.Mensagem, "Continue interagindo", "OK");
                                        }
                                    });

                            })
                            .catch((err) => {
                                console.log('Was not shared via Facebook');
                            });

                    })
                    .catch((err) => {
                        console.log('Not able to be shared via Facebook');
                    });

            });
    }

    shareViaInstagram(conteudo) {
        this.platform.ready()
            .then(() => {

                this.SocialSharing.shareViaInstagram(conteudo.Titulo, conteudo.Imagem)
                    .then((data) => {
                        this.conteudoprovider.postShare(conteudo.Id, conteudo.PessoaId, 'instagram').then((response: any) => {
                            if (response.GanhouPonto) {
                                this.utils.showModalSucesso(response.Mensagem, "Continue interagindo", "OK");
                            }
                        });
                        console.log('Shared via shareViaInstagram');
                    })
                    .catch((err) => {
                        console.log('Was not shared via Instagram');
                    });

            });
    }

    sharePicker(conteudo) {
        this.platform.ready()
            .then(() => {

                this.SocialSharing.share(conteudo.Titulo, conteudo.Titulo, conteudo.Imagem)
                    .then((data) => {
                        console.log('Shared via SharePicker');
                    })
                    .catch((err) => {
                        console.log('Was not shared via SharePicker');
                    });

            });
    }

    shareViaTwitter(conteudo) {
        this.platform.ready()
            .then(() => {

                this.SocialSharing.canShareVia('twitter', conteudo.Titulo, conteudo.Titulo, conteudo.Imagem)
                    .then((data) => {

                        this.SocialSharing.shareViaTwitter(conteudo.Titulo, conteudo.Imagem)
                            .then((data) => {
                                this.conteudoprovider.postShare(conteudo.Id, conteudo.PessoaId, 'twitter')
                                    .then((response: any) => {
                                        if (response.GanhouPonto) {
                                            this.utils.showModalSucesso(response.Mensagem, "Continue interagindo", "OK");
                                        }
                                    });
                                console.log('Shared via Twitter');
                            })
                            .catch((err) => {
                                console.log('Was not shared via Twitter');
                            });

                    });

            })
            .catch((err) => {
                console.log('Not able to be shared via Twitter');
            });
    }

    shareViaWhatsApp(conteudo) {
        this.platform.ready()
            .then(() => {
                this.SocialSharing.shareViaWhatsApp(conteudo.Titulo, conteudo.Imagem)
                    .then((data) => {
                        this.conteudoprovider.postShare(conteudo.Id, conteudo.PessoaId, 'whatsapp')
                            .then((response: any) => {
                                if (response.GanhouPonto) {
                                    this.utils.showModalSucesso(response.Mensagem, "Continue interagindo", "OK");
                                }
                            });
                        console.log('Shared via Twitter');
                    })
                    .catch((err) => {
                        console.log('Was not shared via Twitter');
                    });

            });

    }
}