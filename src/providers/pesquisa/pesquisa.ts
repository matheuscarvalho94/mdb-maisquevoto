import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CONFIG_PROJECT } from '../app-config';
import { AuthProvider } from '../auth/auth';

@Injectable()
export class PesquisaProvider {

    constructor(public http: HttpClient, public auth: AuthProvider) {
    }


    findAll() {
        var token = this.auth.getToken();
        let url = `${CONFIG_PROJECT.baseApi}/Pesquisas`;
        let header = { "headers": { "Content-Type": 'application/json', "Authorization": `Bearer ${token}` } };

        return new Promise((resolve, reject) => {
            this.http.get(url, header)
                .map(res => res)
                .subscribe((result: any) => {
                    let lista = [];

                    for (var z = 0; z < result.length; z++) {

                        for (var j = 0; j < result[z].Pesquisas.length; j++) {

                            result[z].Pesquisas[j].Pontuacao = result[z].Pontuacao;
                            result[z].Pesquisas[j].Tema = result[z].Tema;
                            result[z].Pesquisas[j].ExibirGrafico = result[z].Status == 4
                            result[z].Pesquisas[j].Finalizada = result[z].Status == 4 || result[z].Status == 3

                            for (var i = 0; i < result[z].Pesquisas[j].PesquisasRespostas.length; i++) {
                                var value = result[z].Pesquisas[j].PesquisasRespostas[i];
                                if (value.Resposta == 'Sim' || value.Resposta == 'A Favor') {
                                    result[z].Pesquisas[j].agree = value;
                                }
                                else if (value.Resposta == 'Não' || value.Resposta == 'Contra') {
                                    result[z].Pesquisas[j].negative = value;
                                }
                            };

                            lista.push({ Tema: result.Tema, Pesquisa: result[z].Pesquisas[j] });
                        }
                    }
                    resolve(lista);
                },
                (error) => {
                    reject(error);
                });
        });
    }
    post(id:any)
    {
        var token = this.auth.getToken();
        let url = `${CONFIG_PROJECT.baseApi}/Pesquisas/Participar`;
        let header = { "headers": {  "Authorization": `Bearer ${token}` } };

        var body ={
            RespostaId :id
        };
             return new Promise((resolve, reject) => {
            this.http.post(url,body, header)
                .map(res => res)
                .subscribe((result: any) => {
                 
                    
                    resolve(result);
                },
                (error) => {
                    reject(error);
                });
        });
    }

    findById(id:any) {
        var token = this.auth.getToken();
        let url = `${CONFIG_PROJECT.baseApi}/Pesquisa/`+ id;
        let header = { "headers": { "Content-Type": 'application/json', "Authorization": `Bearer ${token}` } };

        return new Promise((resolve, reject) => {
            this.http.get(url, header)
                .map(res => res)
                .subscribe((result: any) => {

                    var resultado ={ 
                        agreeText: '',
                        agreePercent : 0,
                        negativeText: '',
                        negativePercent: 0,
                         mensagem : '',
                         ExibirGrafico : false,
                         Pontuacao: 0
                    };

                    for (var i = 0; i < result.Grafico[0].DadosGrafico.length; i++) {
                        var pergunta = result.Grafico[0].DadosGrafico[i];
                        if (pergunta.Texto == 'Sim' || pergunta.Texto == 'A Favor') {
                            resultado.agreePercent = pergunta.Porcentagem;
                            resultado.agreeText = pergunta.Texto;
                        }
                        else if (pergunta.Texto == 'Não' || pergunta.Texto == 'Contra') {
                            resultado.negativePercent = pergunta.Porcentagem;
                            resultado.negativeText = pergunta.Texto;
                        }
                    }

                    resultado.mensagem = result.Mensagem;
                    resultado.ExibirGrafico = result.ExibeResultados;
                    resultado.Pontuacao = result.Pesquisa.Pontuacao;
                    //resultado.ExibirGrafico = true;

                    resolve(resultado);
                },
                (error) => {
                    reject(error);
                });
        });
    }
}