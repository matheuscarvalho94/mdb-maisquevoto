import { Injectable } from '@angular/core';
import { IModel } from "./IModel";
@Injectable()
export class Conteudo extends IModel {
    Id: number;
    Titulo: string;
    Chamada: string;
    Hora: string;
    Local: string;
    Video: string;
    Texto: string;
    Imagem: string;
    Slug: string;
    Arquivo: string;
    Data: string;
    Curtidas: number;
}
