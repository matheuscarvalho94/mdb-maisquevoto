import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PesquisaPage, PesquisaModalPage } from './pesquisa';

@NgModule({
  declarations: [
    PesquisaPage,
    PesquisaModalPage
  ],
  imports: [
    IonicPageModule.forChild(PesquisaPage),
    IonicPageModule.forChild(PesquisaModalPage)
  ],
})
export class PesquisaPageModule {}
