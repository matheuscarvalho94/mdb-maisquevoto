import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ModalController } from 'ionic-angular';
import { PesquisaProvider } from '../../../providers/pesquisa/pesquisa';

import { SearchbarComponent } from '../../../components/searchbar/searchbar';


@IonicPage({
  name: 'pesquisa-resposta',
  segment: 'pesquisa-resposta/:id'
})
@Component({
  selector: 'page-pesquisa-pesquisa-resposta',
  templateUrl: 'pesquisa-pesquisa-resposta.html',
})
export class PesquisaPesquisaRespostaPage {

  pesquisaDetail:any;
  agree
  notagree
  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController,public modalCtrl: ModalController, public pesquisa: PesquisaProvider) {
    this.getData();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PesquisaPesquisaRespostaPage');
  }
  searchToggle() {
    let modal = this.modalCtrl.create(SearchbarComponent);
    modal.present();
  }


  getData()
  {
    this.pesquisaDetail =this.navParams.data;
    this.agree=this.pesquisaDetail.agreePercent
    this.notagree=this.pesquisaDetail.negativePercent
  }

  openPagePush(page) {
    this.navCtrl.push(page);
  }
}
