import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { QueroContribuirPage } from './quero-contribuir';
import { BrMaskerModule } from 'brmasker-ionic-3';


@NgModule({
  declarations: [
    QueroContribuirPage,
  ],
  imports: [
    IonicPageModule.forChild(QueroContribuirPage),
    BrMaskerModule

  ],
})
export class QueroContribuirPageModule {}
