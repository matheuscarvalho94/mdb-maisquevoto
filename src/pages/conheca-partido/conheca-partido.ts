import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { SearchbarComponent } from '../../components/searchbar/searchbar';
import { AuthProvider } from '../../providers/auth/auth';
import { UtilsProvider } from '../../providers/utils/utils';
import { ConteudoProvider } from '../../providers/conteudo/conteudo';
import { DetailEleicoesPage } from '../detail-eleicoes/detail-eleicoes';


@IonicPage()
@Component({
  selector: 'page-conheca-partido',
  templateUrl: 'conheca-partido.html',
})
export class ConhecaPartidoPage {
  private tipoConteudo = 10015;
  private start: number = 100;
  private skip: number = 0;
  NadaEncontrado:boolean = false;
  searching:boolean = true;
  anosList: any = [];

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public auth: AuthProvider,
    public conteudo: ConteudoProvider,
    public utils: UtilsProvider,
    public modalCtrl: ModalController,) {
      this.loadAnos()
  }
  loadAnos() {
    return new Promise(resolve => {
      this.conteudo.getConteudo(this.auth.getToken(), this.start, this.skip, this.tipoConteudo)
        .then(data => {
          this.anosList = this.anosList.concat(data);
          console.log(data);
          if(this.anosList.length==0){
            this.NadaEncontrado=true;
          }
        });

    });
  }

  searchToggle() {
    let modal = this.modalCtrl.create(SearchbarComponent);
    modal.present();

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConhecaPartidoPage');
  }

}
