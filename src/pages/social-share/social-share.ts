import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { SharedService } from '../../providers/shared-service/shared-service';
import { PersonProvider } from '../../providers/person/person';


@Component({
  selector: 'social-share',
  templateUrl: 'social-share.html',
  providers: [SharedService]
})
export class SocialSharePage {
  conteudo: any;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public shared: SharedService,
    public pessoa: PersonProvider,
    public viewCtrl: ViewController) {
    this.conteudo = navParams.data;
  }

  ionViewDidLoad() {

  }
  compartilhar(conteudo, rede) {
    this.pessoa.get().then((result: any) => {
      conteudo.PessoaId = result.Id;
      this.shared.SwitchShare(conteudo, rede);
    }, (erro: any) => {
    });
  }
  close() {
    this.viewCtrl.dismiss();
  }
}