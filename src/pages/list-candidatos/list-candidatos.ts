import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController} from 'ionic-angular';

import { DoacaoProvider } from '../../providers/doacao/doacao';
import { SearchbarComponent } from '../../components/searchbar/searchbar';
import { QueroContribuirPage } from '../quero-contribuir/quero-contribuir';

@IonicPage()
@Component({
  selector: 'page-list-candidatos',
  templateUrl: 'list-candidatos.html',
})
export class ListCandidatosPage {
  list:any
  descending: boolean = false;
  order: number;
  column: string = 'Nome';
  // searchText:string = ''
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public modalCtrl: ModalController,
    private doacao: DoacaoProvider,
  ) {
    this.getList()
  }
  searchToggle() {
    let modal = this.modalCtrl.create(SearchbarComponent);
    modal.present();
  }
  sort(){
    this.descending = !this.descending;
    this.order = this.descending ? 1 : -1;
  }
  getList(){
    return new Promise(resolve => {
      this.doacao.getList().then((result: any) => {
        this.list = result;
        console.log(this.list);
        resolve(true);
      });
    })
  }

  abrirForm(l){
    this.navCtrl.push(QueroContribuirPage, {
      candidato: l
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ListCandidatosPage');
  }

}
