import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ModalController } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { UtilsProvider } from '../../providers/utils/utils';
import { ConteudoProvider } from '../../providers/conteudo/conteudo';
import { DetailProgramaPage } from '../detail-programa/detail-programa';
import { SearchbarComponent } from '../../components/searchbar/searchbar';

@IonicPage()
@Component({
  selector: 'page-eleicoes-oque-posso-fazer',
  templateUrl: 'eleicoes-oque-posso-fazer.html',
})
export class EleicoesOquePossoFazerPage {

  // Tipo de Conteudo Noticias
  private tipoConteudo = 19;
  eleicoesdescri: any = {};;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public auth: AuthProvider,
    public conteudo: ConteudoProvider,
    public utils: UtilsProvider,
    public modalCtrl: ModalController,
    
  ) {
  }

  loadDescription() {
    return new Promise(resolve => {
      this.conteudo.getPage(this.auth.getToken(), this.tipoConteudo)
        .then(data => {
          resolve(true);
          this.eleicoesdescri = data;
          console.log(this.eleicoesdescri, 'ELEICAO');

        });

    });
  }

  searchToggle() {
    let modal = this.modalCtrl.create(SearchbarComponent);
    modal.present();

  }
  
  ionViewDidLoad() {
    this.loadDescription()
    console.log('ionViewDidLoad ListProgramaPage');
  }

}
