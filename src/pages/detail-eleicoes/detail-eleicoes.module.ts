import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailEleicoesPage } from './detail-eleicoes';

@NgModule({
  declarations: [
    DetailEleicoesPage,
  ],
  imports: [
    IonicPageModule.forChild(DetailEleicoesPage),
  ],
})
export class DetailEleicoesPageModule {}
