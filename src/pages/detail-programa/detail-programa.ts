import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { SearchbarComponent } from '../../components/searchbar/searchbar';

@IonicPage()
@Component({
  selector: 'page-detail-programa',
  templateUrl: 'detail-programa.html',
})
export class DetailProgramaPage {
  interna:any
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public modalCtrl: ModalController,) {
    this.interna=this.navParams.get('interna');
    console.log(this.interna)
  }
  searchToggle() {
    let modal = this.modalCtrl.create(SearchbarComponent);
    modal.present();

  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailProgramaPage');
  }

}
