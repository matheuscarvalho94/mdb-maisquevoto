import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { PontoProvider } from '../../providers/ponto/ponto';
import { PersonProvider } from '../../providers/person/person';
import { Events } from 'ionic-angular';

/**
 * Generated class for the ProgramaPontosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-programa-pontos',
  templateUrl: 'programa-pontos.html',
})
export class ProgramaPontosPage {

  tab: string = 'como-funciona';
  extrato: any;
  medalhas: any;
  TotalPontos: any;
  MedalhaAtual: any;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public pontos: PontoProvider,
    public loadingCtrl: LoadingController,
    public pessoa: PersonProvider,
    public events: Events) {
    if (navParams.data) {
      this.tab = navParams.data;
      this.events.publish('menu:closed', '');
    }
    this.loadAll();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProgramaPontosPage');
  }
  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      console.log('Async operation has ended');
      this.loadAll()
      refresher.complete();
    }, 1000);

  }
  loadExtrato() {
    this.pontos.getExtrato().then((result: any) => {
      this.extrato = result;
      console.log(result);
    }, (error) => {

    });
  }

  loadMedalhas() {
    this.pontos.getMedalhas().then((result: any) => {
      this.medalhas = result;
      console.log(result);
    }, (error) => {

    });
  }

  loadAll() {
    let loading = this.loadingCtrl.create({
      content: 'Carregando...'
    });
    loading.present();
    this.getPersonDetail();
    this.loadExtrato();
    this.loadMedalhas();

    loading.dismiss();
  }

  getPersonDetail() {
    this.pessoa.get().then((response: any) => {

        this.TotalPontos = response.TotalPontos;

        this.MedalhaAtual =  response.Medalha ? response.Medalha.Nome : "";

      }, (error) => {
        console.log(error);
      });
  }
}
