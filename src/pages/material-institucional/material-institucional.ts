import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { InstitucionalProvider } from '../../providers/institucional/institucional';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { UtilsProvider } from '../../providers/utils/utils';
import { SocialSharePage } from '../social-share/social-share';
import { DownloadProvider } from '../../providers/download/download';
import { SearchbarComponent } from '../../components/searchbar/searchbar';

@IonicPage()
@Component({
  selector: 'page-material-institucional',
  templateUrl: 'material-institucional.html',
})
export class MaterialInstitucionalPage {

  listMaterial: any;
  constructor(public navCtrl: NavController,
     public navParams: NavParams, 
     public loadingCtrl: LoadingController, 
     public material: InstitucionalProvider, 
     public util: UtilsProvider,
     public _download: DownloadProvider,
     public modalCtrl: ModalController) {
    this.getData();
  }
  searchToggle() {
    let modal = this.modalCtrl.create(SearchbarComponent);
    modal.present();
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad MaterialInstitucionalPage');
  }

  doInfinite(infiniteScroll) {
    console.log('Begin async operation');

    setTimeout(() => {
     
      infiniteScroll.complete();
    }, 500);
  }
  searchText:string=''
  click1(param){
    this.searchText=param;
    if(param=='Cartazes'){
      this.active1=false;
      this.active2=true;
      this.active3=false;
      this.active4=false;
    }else if(param=='Posts'){
      this.active1=false;
      this.active2=false;
      this.active3=true;
      this.active4=false;
    }else if(param=='Capas'){
      this.active1=false;
      this.active2=false;
      this.active3=false;
      this.active4=true;
    }else{
      this.active1=true;
      this.active2=false;
      this.active3=false;
      this.active4=false;

    }
  }
  clickTodos(){

    this.active1=true;
    this.active2=false;
    this.active3=false;
    this.active4=false;
    this.getData()
  }
  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      console.log('Async operation has ended');
      this.getData()
      refresher.complete();
    }, 1000);

  }
  getData()
  {
    let loading = this.loadingCtrl.create({
      content: 'Carregando...'
    });
    loading.present();
    this.material.findAll().then((result: any) => {
      loading.dismiss();
      this.listMaterial = result;
      console.log(result);
    }, (error) => {
      loading.dismiss();
      console.log(error);
    });
  }

  getFile(arquivo:any)
  { let loading = this.loadingCtrl.create({
    content: 'Carregando...'
  });
  loading.present();
    this.util.downloadFile(arquivo,loading);
    
  }
  postShare(conteudo) {
    let modal = this.modalCtrl.create(SocialSharePage, conteudo);
    modal.present();
  }
  active1:boolean=true
  active2:boolean=false
  active3:boolean=false
  active4:boolean=false

  clickCategoria(n){
    let loading = this.loadingCtrl.create({
      content: 'Carregando...'
    });
    loading.present();
    this.material.findAll().then((result: any) => {
      loading.dismiss();
      this.listMaterial = result;
      console.log(result);
    }, (error) => {
      loading.dismiss();
      console.log(error);
    });
  }

}
