import { Component, NgModule } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { PersonProvider } from '../../providers/person/person';
import { LoginPage } from '../login/login';
import { HomePage } from '../home/home';
import { BrMaskerModule } from 'brmasker-ionic-3';
import { CONFIG_PROJECT } from '../../providers/app-config';


import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { TextMaskModule } from 'angular2-text-mask';
import MaskedInput from 'angular2-text-mask'




/**
 * Generated class for the CadastroPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
  name: 'cadastro'
})
@Component({
  selector: 'page-cadastro',
  templateUrl: 'cadastro.html',
})
@NgModule({
  imports: [
    BrMaskerModule
  ]
})
export class CadastroPage {
  public maskdate = [/[0-9]/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/];
  public masktel = ['(', /[0-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  Formulario = false;
  Sucesso = false;


  public form = {
    RoleId: 2,
    Nome: '',
    Email: '',
    Numero: '',
    Senha: '',
    ConfirmarSenha: '',
    clienteId: 4
  }


  //public maskdate = [/[0-9]/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/];
  //public maskcel = ['(', /[0-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, /\d/];
  //public masktel = ['(', /[0-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, /\d/];



  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public person: PersonProvider,
    public http: Http

  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CadastroPage');
  }

  goPage(page) {
    this.navCtrl.push(page)
  }


  onRegister() {

    let loading = this.loadingCtrl.create({
      content: 'Carregando...'
    });

    let alert = this.alertCtrl.create({
      title: "OK",
      buttons: [
        {
          text: 'Fechar',
          role: 'cancel'
        }
      ]
    })
    let headers = new Headers({})
    console.log(headers)
    let options = new RequestOptions({ headers: headers });

    loading.present();
   let body = {
      Nome: this.form.Nome,
      Email: this.form.Email,
      Celular: this.form.Numero,
      Senha: this.form.Senha,
      ConfirmarSenha: this.form.ConfirmarSenha,
      RoleId: '2',
      clienteId: 4,
      
  }

  return this.http.post(`${CONFIG_PROJECT.baseApi}` + '/Pessoa/CadastroBasico', body, { headers: headers }).map(res => res.json())

  //    .subscribe((response) => {
  //      if (response.Sucess != false) {

   //       loading.dismiss(),
   //         this.Sucesso = true,
   //         console.log(response)
   //       this.Sucesso = true

      .subscribe((data) => {
        if (data.success == true) {
          loading.dismiss(),
                 this.alertCtrl.create({
            title: 'Parabéns',
            subTitle: data.Mensagem,
            buttons: [{ text: 'Ok' }],
            cssClass: 'alertCustomCssError'
          }).present();
          this.navCtrl.setRoot(LoginPage);
          return false;
        } else {
          this.alertCtrl.create({
            title: 'Erro',
            subTitle: data.Descricao,
            buttons: [{ text: 'Ok' }],
            cssClass: 'alertCustomCssError'
          }).present();

          loading.dismiss();
        }

      }
      , err => {
        console.log("ERROR!: ", err);
        //  this.loading.dismiss();ion
        var mensagem = err._body.replace('{', '').replace('"Message"', '').replace(':', '').replace('}', '').replace('"', '').replace('"', '');
          if (mensagem.indexOf('encontra cadastrado') != -1) {
            this.alertCtrl.create({
              title: 'Erro',
              subTitle: "Esse e-mail já se encontra cadastrado. Informe outro e-mail ou <span (click)='EsqueciPage()'>clique aqui</span> caso tenha esquecido sua senha.",
              buttons: [{ text: 'Ok' }],
              cssClass: 'alertCustomCssError'
            }).present();

            loading.dismiss()
          }
          else {
          this.alertCtrl.create({
            title: 'Erro',
            subTitle: mensagem,
            buttons: [{ text: 'Ok' }],
            cssClass: 'alertCustomCssError'
          }).present();
            loading.dismiss()
          }
      }


    );
    // this.person.post(model)
    //   .then((response) => {
    //     setTimeout(() => {
    //       loading.dismiss();
    //       this.navCtrl.setRoot(HomePage);
    //     }, 2000);
    //   }, (error) => {
    //     setTimeout(() => {
    //       loading.dismiss();
    //       alert.setTitle(error.error.Message);
    //       alert.present();
    //     }, 2000);
    //   });
  }

}

