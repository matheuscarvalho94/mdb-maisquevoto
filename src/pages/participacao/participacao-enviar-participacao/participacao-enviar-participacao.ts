import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage({
  name: 'form-participacao',
  segment: 'form-participacao'
})
@Component({
  selector: 'page-participacao-enviar-participacao',
  templateUrl: 'participacao-enviar-participacao.html',
})
export class ParticipacaoEnviarParticipacaoPage {

  tab = "local";

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ParticipacaoEnviarParticipacaoPage');
  }
  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      console.log('Async operation has ended');
      this.choiceCategorie()
      refresher.complete();
    }, 1000);

  }

  saveLocal() {
    this.tab = "categoria";
  }

  choiceCategorie() {
    this.tab = "tipo";
  }

  saveTipe() {
    this.tab = "descricao";
  }

}
