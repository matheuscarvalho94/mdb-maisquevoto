import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { NotificationProvider } from '../../providers/notificacao/notificacao';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';

@IonicPage()
@Component({
  selector: 'page-notificacoes',
  templateUrl: 'notificacoes.html',
})
export class NotificacoesPage {

  notificationsList: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public notification: NotificationProvider, public loadingCtrl: LoadingController) {
    this.getNotifications();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NotificacoesPage');
  
  }
  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      console.log('Async operation has ended');
      this.getNotifications()
      refresher.complete();
    }, 1000);

  }

  openDetail(page, id) {
    this.navCtrl.push(page, id)
  }

  getNotifications() {
    let loading = this.loadingCtrl.create({
      content: 'Carregando...'
    });
    loading.present();
    this.notification.Get().then((response: Object) => {
      loading.dismiss();
      this.notificationsList = response;
      console.log(response);
    }, (error) => {
      loading.dismiss();
      console.log(error);
    });
  }
}
