import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { NotificationProvider } from '../../../providers/notificacao/notificacao';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';


@IonicPage({
  name: 'notificacoes',
  segment: 'notificacoes/:id'
})
@Component({
  selector: 'page-notificacoes-notificacoes-detalhe',
  templateUrl: 'notificacoes-notificacoes-detalhe.html',
})
export class NotificacoesNotificacoesDetalhePage {

  notif:any;
  id:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public notification: NotificationProvider, public loadingCtrl: LoadingController) {
    this.getNotification();
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad NotificacoesNotificacoesDetalhePage');
  }
  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      console.log('Async operation has ended');
     this.getNotification()
      refresher.complete();
    }, 1000);

  }

  getNotification() {
    let loading = this.loadingCtrl.create({
      content: 'Carregando...'
    });
    loading.present();
    console.log(this.navParams.data);
    this.notification.GetDetail(this.navParams.data).then((response: Object) => {
      loading.dismiss();
      this.notif = response;
      console.log(response);
    }, (error) => {
      loading.dismiss();
      console.log(error);
    });
  }

}
