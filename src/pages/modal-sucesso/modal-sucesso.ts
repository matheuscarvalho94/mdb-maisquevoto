import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';


@Component({
  selector: 'modal-sucesso',
  templateUrl: 'modal-sucesso.html',
})
export class ModalSucessoPage {
  data: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController) {
    this.data = navParams.data;
  }

  ionViewDidLoad() {
  }

  close() {
    this.viewCtrl.dismiss();
  }
}