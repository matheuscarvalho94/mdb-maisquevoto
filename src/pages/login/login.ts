import { Component } from '@angular/core';

import { IonicPage, NavController, NavParams,LoadingController, AlertController } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { HomePage } from '../home/home';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { NotificationProvider } from '../../providers/notificacao/notificacao';
import { PersonProvider } from '../../providers/person/person';

@IonicPage({
  name: 'login'
})
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  user = {
    username: '',
    password: ''
  }
  profile: any;
  count: 0;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private auth: AuthProvider,
    public loadingCtrl: LoadingController, 
    public alertCtrl: AlertController,
    private fb: Facebook,
    public notification: NotificationProvider,
    public person: PersonProvider,
  ) {}

  ionViewDidLoad() { }

  goPage(page) {
    this.navCtrl.push(page);
  }

  onLogin(user) {

    let loading = this.loadingCtrl.create({
      content: 'Carregando...'
    });

    let alert = this.alertCtrl.create({
      title: "OK",
      buttons: [
        {
          text: 'Fechar',
          role: 'cancel'
        }
      ]
    })

    loading.present();

    this.auth.postLogin()
    .then((response) => {
      setTimeout(() => {
        this.auth.saveSection(response);
            loading.dismiss();
            this.navCtrl.setRoot(HomePage);
      }, 2000);
    }, (error) => {
      setTimeout(() => {
        loading.dismiss();
        alert.setTitle(error.error.error_description);
        alert.present();
      }, 2000);
    });

  }

  loginToFacebook() {

    this.fb.login(['public_profile', 'user_friends', 'email'])
    .then((res: FacebookLoginResponse) => console.log('Logged into Facebook!', res))
    .catch(e => console.log('Error logging into Facebook', e));

  }

 

}
