import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, LoadingController, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HttpModule } from '@angular/http';
import { Network } from '@ionic-native/network';
import { ComitesPage } from '../pages/comites/comites';
import { VoluntarioPage } from '../pages/voluntario/voluntario';
import { NotificacoesPage } from '../pages/notificacoes/notificacoes';
import { ParticipacaoPage } from '../pages/participacao/participacao';
import { PesquisaPage } from '../pages/pesquisa/pesquisa';
import { ProgramaGovernoPage } from '../pages/programa-governo/programa-governo';
import { HomePage } from '../pages/home/home';
import { MaterialInstitucionalPage } from '../pages/material-institucional/material-institucional';
import { ProgramaPontosPage } from '../pages/programa-pontos/programa-pontos';
import { TutorialPage } from '../pages/tutorial/tutorial';
import { LoginPage } from '../pages/login/login';
import { AuthProvider } from '../providers/auth/auth';
import { PersonProvider } from '../providers/person/person';
import { PersonDetail } from '../models/PersonDetail';
import { ListProgramaPage } from '../pages/list-programa/list-programa';
import { EleicoesOquePossoFazerPage } from '../pages/eleicoes-oque-posso-fazer/eleicoes-oque-posso-fazer';
import { EleicoesDataPrazosPage } from '../pages/eleicoes-data-prazos/eleicoes-data-prazos';
import { ConhecaPartidoPage } from '../pages/conheca-partido/conheca-partido';

import { NotificationProvider } from '../providers/notificacao/notificacao';
import { SocialSharePage } from '../pages/social-share/social-share';
import { QueroContribuirPage } from "../pages/quero-contribuir/quero-contribuir";
import { ListCandidatosPage } from "../pages/list-candidatos/list-candidatos";
import { Events } from 'ionic-angular';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  public disconnect: boolean = false;

  rootPage: any;
  pages: any;
  profile: any;
  count: 0;

  constructor(public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public auth: AuthProvider,
    public person: PersonProvider,
    public notification: NotificationProvider,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public events: Events,
    private network: Network,
) {

    events.subscribe('profile:count', (profile, count) => {
      this.profile = profile;
      this.count = count;
    });

    events.subscribe('menu:closed', () => {
      // your action here
  });
    this.initializeApp();

    this.network.onDisconnect().subscribe(() => {
      this.disconnect = true;
      this.onDisconnect();
    });
  }



  onDisconnect() {
    console.log('Sem conexão com internet. O conteúdo mais recente não poderá ser exibido. :-(');
    this.alertCtrl.create({
      // title: this.service.config.name,
      // subTitle: 'Sem conexão com internet. O conteúdo mais recente não poderá ser exibido.',
      // buttons: [{ text: 'Ok' }]
      title: 'Sem internet',
      subTitle: 'O conteúdo mais recente não poderá ser exibido.',
      buttons: ['OK'],
      cssClass: 'alertCustomCssError'
    }).present();
  }

  initializeApp() {
    this.platform.ready().then(() => {

      this.statusBar.styleDefault();
      this.splashScreen.hide();

      if (this.auth.isLogged()) {
        this.preencherPages()
        this.rootPage = HomePage;
      }
      else {
        this.preencherPages()
        this.rootPage = TutorialPage;
      }
    });
  }

  openPage(page,params) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component,params);
  }
  openPageHistorico()
  {
    this.nav.setRoot(ProgramaPontosPage,'historico');
  }

  openPagePush(page, params) {
    this.nav.push(page,params);
  }


  logoutApp() {
    this.nav.setRoot(LoginPage);
  }

  preencherPages() {
    this.pages = [
      { title: 'Quero doar', component: ListCandidatosPage, icon: 'fa fa-usd', description: null, cssClass: 'quero-contribuir' },
      { title: 'Início', component: HomePage, icon: 'fa fa-home',  cssClass: 'iniciocss' },
      { title: 'Dois anos de MDB no Governo', component: ListProgramaPage, icon: 'fa fa-star', },
      { title: 'Eleições 2018', component: EleicoesOquePossoFazerPage, icon: 'fa fa-info-circle', description: 'O que posso fazer?' },
      { title: 'Eleições 2018', component: EleicoesDataPrazosPage, icon: 'fa fa-calendar', description: 'Prazos e datas importantes' },
      { title: 'Conheça o MDB', component: ConhecaPartidoPage, icon: 'fa fa-flag ', description: null },
      { title: 'Material Institucional', component: MaterialInstitucionalPage, icon: 'fa fa-picture-o', description: 'Banners, post...' },
      { title: 'Diretórios', component: ComitesPage, icon: 'fa fa-globe' },
      { title: 'Pesquisa de Opinião', component: PesquisaPage, icon: 'fa fa-comment', description: null },
    ];
  }
}
