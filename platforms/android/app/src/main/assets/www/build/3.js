webpackJsonp([3],{

/***/ 381:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ParticipacaoParticipacaoDetalhePageModule", function() { return ParticipacaoParticipacaoDetalhePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__participacao_participacao_detalhe__ = __webpack_require__(392);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ParticipacaoParticipacaoDetalhePageModule = /** @class */ (function () {
    function ParticipacaoParticipacaoDetalhePageModule() {
    }
    ParticipacaoParticipacaoDetalhePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__participacao_participacao_detalhe__["a" /* ParticipacaoParticipacaoDetalhePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__participacao_participacao_detalhe__["a" /* ParticipacaoParticipacaoDetalhePage */]),
            ],
        })
    ], ParticipacaoParticipacaoDetalhePageModule);
    return ParticipacaoParticipacaoDetalhePageModule;
}());

//# sourceMappingURL=participacao-participacao-detalhe.module.js.map

/***/ }),

/***/ 392:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ParticipacaoParticipacaoDetalhePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the ParticipacaoParticipacaoDetalhePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ParticipacaoParticipacaoDetalhePage = /** @class */ (function () {
    function ParticipacaoParticipacaoDetalhePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ParticipacaoParticipacaoDetalhePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ParticipacaoParticipacaoDetalhePage');
    };
    ParticipacaoParticipacaoDetalhePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-participacao-participacao-detalhe',template:/*ion-inline-start:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/participacao/participacao-participacao-detalhe/participacao-participacao-detalhe.html"*/'<ion-header color="header">\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title text-center>Minha Participação</ion-title>\n    <ion-buttons end>\n      <!-- <button (click)="searchToggle()" class="search-header">\n      <ion-icon name="search"></ion-icon>\n    </button> -->\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n    <ion-refresher (ionRefresh)="doRefresh($event)">\n        <ion-refresher-content></ion-refresher-content>\n      </ion-refresher>\n    <ion-card>\n      <ion-card-content>\n        <ion-note><ion-icon  name="calendar"></ion-icon> 10 de Janeiro de 2018</ion-note>\n        <div class="block-text" margin-top>\n          <div>\n            <strong>Categoria:</strong>\n            <p>Ordem Pública</p>\n          </div>\n          <div margin-top>\n            <strong>Tipo:</strong>\n            <p>Ocupação Irregular</p>\n          </div>\n          <div margin-top>\n            <strong>Local:</strong>\n            <p>Não informado</p>\n          </div>\n          <div margin-top>\n            <strong>Descrição:</strong>\n            <p>Trago os detalhes de um problema recorrente em nossa cidade que precisa de muita atenção e foco por parte de todos. Abaixo envio fotos e detalhes. Fico à disposição para maiores esclarecimentos.</p>\n          </div>\n          <div margin-top>\n            <strong>Fotos:</strong>\n            <ul class="list-pictures">\n              <li>\n                <img src="http://www.gazetavirtual.com.br/wp-content/uploads/2013/08/Buracos-rua-Candia-prox-a-rua-Serra-de-Botucatu-Carol.jpg">\n              </li>\n              <li>\n                <img src="http://3.bp.blogspot.com/-2jFFg85GwDQ/UD2FvX8-9JI/AAAAAAAAAic/vvtqqtosxuE/s1600/buracos-rua.jpg">\n              </li>\n              <li>\n                <img src="http://1.bp.blogspot.com/_AoprKPDWOLE/S_7mjFp4B0I/AAAAAAAACF8/2vgu1kjMO_o/s1600/rua_buraco.jpg">\n              </li>\n            </ul>\n          </div>\n        </div>\n      </ion-card-content>\n    </ion-card>\n</ion-content>\n'/*ion-inline-end:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/participacao/participacao-participacao-detalhe/participacao-participacao-detalhe.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */]])
    ], ParticipacaoParticipacaoDetalhePage);
    return ParticipacaoParticipacaoDetalhePage;
}());

//# sourceMappingURL=participacao-participacao-detalhe.js.map

/***/ })

});
//# sourceMappingURL=3.js.map