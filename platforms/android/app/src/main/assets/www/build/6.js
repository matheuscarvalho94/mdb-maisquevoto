webpackJsonp([6],{

/***/ 377:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DadosComplementaresPageModule", function() { return DadosComplementaresPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__dados_complementares__ = __webpack_require__(388);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var DadosComplementaresPageModule = /** @class */ (function () {
    function DadosComplementaresPageModule() {
    }
    DadosComplementaresPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__dados_complementares__["a" /* DadosComplementaresPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__dados_complementares__["a" /* DadosComplementaresPage */]),
            ],
        })
    ], DadosComplementaresPageModule);
    return DadosComplementaresPageModule;
}());

//# sourceMappingURL=dados-complementares.module.js.map

/***/ }),

/***/ 388:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DadosComplementaresPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the DadosComplementaresPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var DadosComplementaresPage = /** @class */ (function () {
    function DadosComplementaresPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    DadosComplementaresPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DadosComplementaresPage');
    };
    DadosComplementaresPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-dados-complementares',template:/*ion-inline-start:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/dados-complementares/dados-complementares.html"*/'<ion-content>\n  <ion-list padding>\n    <header class="login-header" text-center>\n        <h3 class="title-blue">Ganhe Pontos</h3>\n        <div class="block-text">\n          <p>Insira os dados abaixo e ganhe pontos! <br/>A cada preenchimento, mais pontos você ganha.</p>\n        </div>\n    </header>\n    <button class="button-upload">\n        <ion-icon name="camera"></ion-icon>\n        <p>Selecione <strong>sua foto</strong></p>\n    </button>\n    <div class="progress-bar">\n      <div class="bar" style="width: 20%">\n        <div class="status"><strong>+10</strong> Pontos</div>\n      </div>\n    </div>\n    <div class="block">\n      <h4 class="bold" margin-top>Redes Sociais</h4>\n      <ion-item>\n        <ion-label floating text-uppercase>Facebook</ion-label>\n        <ion-input type="text"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label floating text-uppercase>Instagram</ion-label>\n        <ion-input type="text"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label floating text-uppercase>Twitter</ion-label>\n        <ion-input type="text"></ion-input>\n      </ion-item>\n    </div>\n    <div class="block">\n      <h4 class="bold" margin-top>Dados Pessoais</h4>\n      <ion-item>\n        <ion-label floating>Sexo</ion-label>\n        <ion-select okText="Ok" cancelText="Cancelar">\n          <ion-option value="masculino">Masculino</ion-option>\n          <ion-option value="feminino">Feminino</ion-option>\n          <ion-option value="outro">Outro</ion-option>\n        </ion-select>\n      </ion-item>\n      <ion-item>\n        <ion-label floating text-uppercase>Telefone Fixo</ion-label>\n        <ion-input type="text"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label floating text-uppercase>Qual é sua Profissao?</ion-label>\n        <ion-input type="text"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label floating>Estado Civil</ion-label>\n        <ion-select okText="Ok" cancelText="Cancelar">\n          <ion-option value="casado">Casado</ion-option>\n          <ion-option value="solteiro">Solteiro</ion-option>\n        </ion-select>\n      </ion-item>\n      <ion-row>\n        <ion-col col-5>\n          <ion-item>\n            <ion-label floating>Estado</ion-label>\n            <ion-select okText="Ok" cancelText="Cancelar">\n              <ion-option value="uf">UF</ion-option>\n            </ion-select>\n          </ion-item>\n        </ion-col>\n        <ion-col col-7>\n          <ion-item>\n            <ion-label floating>Cidade</ion-label>\n            <ion-select okText="Ok" cancelText="Cancelar">\n              <ion-option value="selecione">Selecione</ion-option>\n            </ion-select>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n      <ion-item>\n        <ion-label floating text-uppercase>Rua</ion-label>\n        <ion-input type="text"></ion-input>\n      </ion-item>\n      <ion-row>\n        <ion-col col-5>\n          <ion-item>\n            <ion-label floating>Número</ion-label>\n            <ion-select>\n              <ion-option value="number"></ion-option>\n            </ion-select>\n          </ion-item>\n        </ion-col>\n        <ion-col col-7>\n          <ion-item>\n            <ion-label floating text-uppercase>Complemento</ion-label>\n            <ion-input type="text"></ion-input>\n          </ion-item>\n        </ion-col>\n        <ion-col col-6>\n          <ion-item>\n            <ion-label floating text-uppercase>CEP</ion-label>\n            <ion-input type="text"></ion-input>\n          </ion-item>\n        </ion-col>\n        <ion-col col-6>\n          <ion-item>\n            <ion-label floating text-uppercase>Bairro</ion-label>\n            <ion-input type="text"></ion-input>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n    </div>\n    <div class="block">\n      <h4 class="bold" >Marque suas Preferências</h4>\n      <ul class="list-default">\n        <li>\n          <div>\n            <ion-icon name="body" item-start></ion-icon>Item 1\n          </div>\n          <ion-icon name="heart" class="heart select" item-end></ion-icon>\n        </li>\n        <li>\n          <div><ion-icon name="videocam" item-start></ion-icon>Item 1</div>\n          <ion-icon name="heart" class="heart" item-end></ion-icon>\n        </li>\n        <li>\n          <div><ion-icon name="person" item-start></ion-icon>Item 1</div>  \n          <ion-icon name="heart" class="heart" item-end></ion-icon>\n        </li>\n        <li>\n          <div><ion-icon name="list-box" item-start></ion-icon>Item 1</div>  \n          <ion-icon name="heart" class="heart" item-end></ion-icon>\n        </li>\n      </ul>\n    </div>\n    <button ion-button full text-uppercase>Ganhar Pontos!</button>\n  </ion-list>\n</ion-content>\n<ion-footer>\n  <ion-toolbar (click)="goPage(\'cadastro\')" text-center>\n    Não quero esses pontos. <strong>Ir para o app!</strong>\n  </ion-toolbar>\n</ion-footer>\n  \n  \n  '/*ion-inline-end:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/dados-complementares/dados-complementares.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */]])
    ], DadosComplementaresPage);
    return DadosComplementaresPage;
}());

//# sourceMappingURL=dados-complementares.js.map

/***/ })

});
//# sourceMappingURL=6.js.map