webpackJsonp([1],{

/***/ 383:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VoluntarioDetalhePageModule", function() { return VoluntarioDetalhePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__voluntario_detalhe__ = __webpack_require__(394);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var VoluntarioDetalhePageModule = /** @class */ (function () {
    function VoluntarioDetalhePageModule() {
    }
    VoluntarioDetalhePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__voluntario_detalhe__["a" /* VoluntarioDetalhePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__voluntario_detalhe__["a" /* VoluntarioDetalhePage */]),
            ],
        })
    ], VoluntarioDetalhePageModule);
    return VoluntarioDetalhePageModule;
}());

//# sourceMappingURL=voluntario-detalhe.module.js.map

/***/ }),

/***/ 394:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VoluntarioDetalhePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__indique_amigos__ = __webpack_require__(268);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_searchbar_searchbar__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_voluntario_voluntario__ = __webpack_require__(124);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular_components_loading_loading_controller__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_person_person__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_utils_utils__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__social_share_social_share__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_conteudo_conteudo__ = __webpack_require__(23);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var VoluntarioDetalhePage = /** @class */ (function () {
    function VoluntarioDetalhePage(navCtrl, navParams, modalCtrl, voluntario, loadingCtrl, utils, person, _conteudo) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.voluntario = voluntario;
        this.loadingCtrl = loadingCtrl;
        this.utils = utils;
        this.person = person;
        this._conteudo = _conteudo;
        this.getData();
    }
    VoluntarioDetalhePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad VoluntarioDetalhePage');
    };
    VoluntarioDetalhePage.prototype.doRefresh = function (refresher) {
        var _this = this;
        console.log('Begin async operation', refresher);
        setTimeout(function () {
            console.log('Async operation has ended');
            _this.getData();
            refresher.complete();
        }, 1000);
    };
    VoluntarioDetalhePage.prototype.openModal = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__indique_amigos__["a" /* IndiqueAmigosPage */]);
        modal.present();
    };
    VoluntarioDetalhePage.prototype.searchToggle = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__components_searchbar_searchbar__["a" /* SearchbarComponent */]);
        modal.present();
    };
    VoluntarioDetalhePage.prototype.getData = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: 'Carregando...'
        });
        loading.present();
        this.voluntario.findById(this.navParams.data).then(function (response) {
            loading.dismiss();
            _this.vol = response;
            console.log(response);
        }, function (error) {
            loading.dismiss();
            console.log(error);
        });
    };
    VoluntarioDetalhePage.prototype.postLike = function (conteudo) {
        var _this = this;
        var idPost = conteudo.Id;
        this.person.get()
            .then(function (response) {
            var person = response;
            _this._conteudo.postLike(idPost, person.Id)
                .then(function (response) {
                if (response.Curtiu && !conteudo.JaCurtiu) {
                    conteudo.Curtidas += 1;
                }
                else {
                    conteudo.Curtidas -= 1;
                }
                conteudo.JaCurtiu = response.Curtiu;
                if (response.GanhouPonto) {
                    _this.utils.showModalSucesso(response.Mensagem, "Continue interagindo", "OK");
                }
            });
        });
    };
    VoluntarioDetalhePage.prototype.postShare = function (conteudo) {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_8__social_share_social_share__["a" /* SocialSharePage */], conteudo);
        modal.present();
    };
    VoluntarioDetalhePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-voluntario-detalhe',template:/*ion-inline-start:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/voluntario/voluntario-detalhe/voluntario-detalhe.html"*/'<ion-header color="header">\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title text-center>Seja um Voluntário</ion-title>\n    <ion-buttons end>\n      <!-- <button (click)="searchToggle()" class="search-header">\n      <ion-icon name="search"></ion-icon>\n    </button> -->\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content *ngIf="vol">\n    <ion-refresher (ionRefresh)="doRefresh($event)">\n        <ion-refresher-content></ion-refresher-content>\n      </ion-refresher>\n  <img src="{{vol.Imagem}}" class="img-featured">\n  <div padding>\n    <header class="buttons-event right">\n      <button ion-button icon-left clear small [ngClass]="{\'liked\': vol.JaCurtiu}" (click)="postLike(vol)">\n        <ion-icon name="heart"></ion-icon>\n        <div>{{vol.Curtidas}}</div>\n      </button>\n      <button class="share" ion-button icon-right text-left clear small (click)="postShare(vol)">\n        <ion-icon name="share"></ion-icon>\n      </button>\n    </header>\n    <h2 class="bold">{{vol.Titulo}}</h2>\n    <div class="block-text" margin-top>\n      <p [innerHtml]="vol.Texto"></p>\n    </div>\n    <!-- <footer padding-top>\n      <button ion-button full text-uppercase (click)="openModal()">Convide Amigos</button>\n    </footer> -->\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/voluntario/voluntario-detalhe/voluntario-detalhe.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_voluntario_voluntario__["a" /* VoluntarioProvider */],
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular_components_loading_loading_controller__["a" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_7__providers_utils_utils__["a" /* UtilsProvider */],
            __WEBPACK_IMPORTED_MODULE_6__providers_person_person__["a" /* PersonProvider */],
            __WEBPACK_IMPORTED_MODULE_9__providers_conteudo_conteudo__["a" /* ConteudoProvider */]])
    ], VoluntarioDetalhePage);
    return VoluntarioDetalhePage;
}());

//# sourceMappingURL=voluntario-detalhe.js.map

/***/ })

});
//# sourceMappingURL=1.js.map