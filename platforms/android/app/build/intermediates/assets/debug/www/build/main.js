webpackJsonp([10],[
/* 0 */,
/* 1 */,
/* 2 */,
/* 3 */,
/* 4 */,
/* 5 */,
/* 6 */,
/* 7 */,
/* 8 */,
/* 9 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export User */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_config__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_catch__ = __webpack_require__(178);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_catch__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_toPromise__ = __webpack_require__(179);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_toPromise___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_toPromise__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__ = __webpack_require__(180);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var User = /** @class */ (function () {
    function User() {
    }
    return User;
}());

var AuthProvider = /** @class */ (function () {
    function AuthProvider(http) {
        this.http = http;
    }
    AuthProvider.prototype.postLogin = function () {
        var _this = this;
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/token";
        var body = "grant_type=password&username=mdb_app@mypolis.com.br&password=Dvm86gzARG&appId=" + __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].appId;
        var header = { "headers": { "Content-Type": 'application/x-www-form-urlencoded' } };
        return new Promise(function (resolve, reject) {
            _this.http.post(url, body, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                localStorage.setItem('access_token', result.access_token);
            }, function (error) {
                reject(error);
            });
        });
    };
    AuthProvider.prototype.postResetPassWord = function (email) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/pessoa/RecuperarSenha";
            var data = {
                Email: email,
                ClienteId: __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].clienteId
            };
            _this.http.post(url, data)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    AuthProvider.prototype.saveSection = function (data) {
        localStorage.setItem('access_token', data.access_token);
    };
    AuthProvider.prototype.isLogged = function () {
        if (this.getToken()) {
            return true;
        }
        return false;
    };
    AuthProvider.prototype.logoutUser = function () {
        localStorage.removeItem('acesss_token');
    };
    AuthProvider.prototype.getToken = function () {
        return localStorage.getItem('access_token');
    };
    AuthProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], AuthProvider);
    return AuthProvider;
}());

//# sourceMappingURL=auth.js.map

/***/ }),
/* 10 */,
/* 11 */,
/* 12 */,
/* 13 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchbarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SearchbarComponent = /** @class */ (function () {
    function SearchbarComponent(navCtrl) {
        this.navCtrl = navCtrl;
        this.searchQuery = '';
        this.initializeItems();
    }
    SearchbarComponent.prototype.initializeItems = function () {
        this.items = [
            'Amsterdam',
            'Bogota'
        ];
    };
    SearchbarComponent.prototype.getItems = function (ev) {
        // Reset items back to all of the items
        this.initializeItems();
        // set val to the value of the searchbar
        var val = ev.target.value;
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.items = this.items.filter(function (item) {
                return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
            });
        }
    };
    SearchbarComponent.prototype.backPage = function () {
        this.navCtrl.pop();
    };
    SearchbarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'searchbar',template:/*ion-inline-start:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/components/searchbar/searchbar.html"*/'<ion-searchbar placeholder="Buscar" (ionInput)="getItems($event)" (ionCancel)="onCancel($event)"></ion-searchbar>\n<strong class="title">Sugestões:</strong>\n<ion-list>\n  <ion-item *ngFor="let item of items">\n    {{ item }}\n  </ion-item>\n</ion-list>\n<div>\n  <button ion-button clear icon-left (click)="backPage()"> <ion-icon name="ios-arrow-back"></ion-icon> Voltar</button>\n</div>'/*ion-inline-end:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/components/searchbar/searchbar.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */]])
    ], SearchbarComponent);
    return SearchbarComponent;
}());

//# sourceMappingURL=searchbar.js.map

/***/ }),
/* 14 */,
/* 15 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UtilsProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_file_transfer__ = __webpack_require__(92);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_android_permissions__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_modal_sucesso_modal_sucesso__ = __webpack_require__(182);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var UtilsProvider = /** @class */ (function () {
    function UtilsProvider(http, transfer, platform, androidPermissions, modalCtrl, alertCtrl) {
        this.http = http;
        this.transfer = transfer;
        this.platform = platform;
        this.androidPermissions = androidPermissions;
        this.modalCtrl = modalCtrl;
        this.alertCtrl = alertCtrl;
        this.estados = null;
        this.initialize();
    }
    UtilsProvider.prototype.initialize = function () {
        var _this = this;
        this.http.get('../assets/estados-cidades.json')
            .subscribe(function (result) {
            _this.estados = result.estados;
        }, function (error) {
            console.error('Error: ', error);
        });
    };
    UtilsProvider.prototype.getEstados = function () {
        var _this = this;
        if (this.estados) {
            return new Promise(function (resolve, reject) {
                resolve(_this.estados);
            });
        }
        return new Promise(function (resolve, reject) {
            var url = '../assets/estados-cidades.json';
            _this.http.get(url)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result.estados);
            }, function (error) {
                reject(error);
            });
        });
    };
    UtilsProvider.prototype.getCidades = function (uf) {
        var _this = this;
        if (this.estados) {
            return new Promise(function (resolve, reject) {
                var cidades = [];
                for (var i = 0; i < _this.estados.length; i++) {
                    if (_this.estados[i].sigla == uf) {
                        cidades = _this.estados[i].cidades;
                        break;
                    }
                }
                resolve(cidades);
            });
        }
        return new Promise(function (resolve, reject) {
            var url = '../assets/estados-cidades.json';
            _this.http.get(url)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                var cidades = [];
                for (var i = 0; i < result.estados.length; i++) {
                    if (result.estados[i].sigla == uf) {
                        cidades = result.estados[i].cidades;
                        break;
                    }
                }
                resolve(cidades);
            }, function (error) {
                reject(error);
            });
        });
    };
    UtilsProvider.prototype.getNameFromUf = function (uf) {
        var nome = uf;
        if (this.estados) {
            for (var i = 0; i < this.estados.estados.length; i++) {
                if (this.estados.estados[i].sigla == uf) {
                    nome = this.estados.estados[i].nome;
                    break;
                }
            }
        }
        return nome;
    };
    UtilsProvider.prototype.getFromUf = function (uf) {
        var estado = null;
        if (this.estados) {
            for (var i = 0; i < this.estados.estados.length; i++) {
                if (this.estados.estados[i].sigla == uf) {
                    estado = this.estados.estados[i];
                    break;
                }
            }
        }
        return estado;
    };
    UtilsProvider.prototype.showAlert = function (text, message) {
        if (message === void 0) { message = undefined; }
        var alert = this.alertCtrl.create({
            title: "OK",
            buttons: [
                {
                    text: 'Fechar',
                    role: 'cancel'
                }
            ]
        });
        alert.setTitle(text);
        if (message != undefined && message != '' && message != null) {
            alert.setMessage(message);
        }
        alert.present();
    };
    UtilsProvider.prototype.showModalSucesso = function (titulo, mensagem, botao) {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_5__pages_modal_sucesso_modal_sucesso__["a" /* ModalSucessoPage */], { Titulo: titulo, Mensagem: mensagem, Btn: botao });
        modal.present();
    };
    UtilsProvider.prototype.downloadFile = function (url) {
        var _this = this;
        if (!this.platform.is('cordova')) {
            return false;
        }
        var fileTransfer = this.transfer.create();
        var fileName = url.substring(url.lastIndexOf('/') + 1);
        var directory = '';
        if (this.platform.is('ios')) {
            directory = cordova.file.documentsDirectory;
        }
        else if (this.platform.is('android')) {
            directory = 'file:///storage/emulated/0/Download/';
        }
        // this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE)
        //   .then(result => {
        //     console.log(result.hasPermission)
        //     if (!result.hasPermission) {
        //       this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE)
        //     }
        //     else {
        fileTransfer.download(url, directory + fileName).then(function (entry) {
            _this.showAlert('Download concluído');
            console.log('download complete: ' + entry.toURL());
        }, function (error) {
            console.log(error);
            // handle error
        });
        // }
        // },
        //   err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE)
        // );
    };
    UtilsProvider.prototype.cpf = function (cpf) {
        if (cpf == null) {
            return false;
        }
        if (cpf.length != 11) {
            return false;
        }
        if ((cpf == '00000000000') || (cpf == '11111111111') || (cpf == '22222222222') || (cpf == '33333333333') || (cpf == '44444444444') || (cpf == '55555555555') || (cpf == '66666666666') || (cpf == '77777777777') || (cpf == '88888888888') || (cpf == '99999999999')) {
            return false;
        }
        var numero = 0;
        var caracter = '';
        var numeros = '0123456789';
        var j = 10;
        var somatorio = 0;
        var resto = 0;
        var digito1 = 0;
        var digito2 = 0;
        var cpfAux = '';
        cpfAux = cpf.substring(0, 9);
        for (var i = 0; i < 9; i++) {
            caracter = cpfAux.charAt(i);
            if (numeros.search(caracter) == -1) {
                return false;
            }
            numero = Number(caracter);
            somatorio = somatorio + (numero * j);
            j--;
        }
        resto = somatorio % 11;
        digito1 = 11 - resto;
        if (digito1 > 9) {
            digito1 = 0;
        }
        j = 11;
        somatorio = 0;
        cpfAux = cpfAux + digito1;
        for (var i = 0; i < 10; i++) {
            caracter = cpfAux.charAt(i);
            numero = Number(caracter);
            somatorio = somatorio + (numero * j);
            j--;
        }
        resto = somatorio % 11;
        digito2 = 11 - resto;
        if (digito2 > 9) {
            digito2 = 0;
        }
        cpfAux = cpfAux + digito2;
        if (cpf != cpfAux) {
            return false;
        }
        else {
            return true;
        }
    };
    UtilsProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_file_transfer__["a" /* FileTransfer */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["m" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_android_permissions__["a" /* AndroidPermissions */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* AlertController */]])
    ], UtilsProvider);
    return UtilsProvider;
}());

//# sourceMappingURL=utils.js.map

/***/ }),
/* 16 */,
/* 17 */,
/* 18 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CONFIG_PROJECT; });
var CONFIG_PROJECT = {
    name: "Voluntários da Pátria",
    appId: 'mdb',
    // baseApi: 'http://138.97.105.194:170/api',
    baseApi: 'http://mypolis.com.br/api',
    clienteId: 1
};
//# sourceMappingURL=app-config.js.map

/***/ }),
/* 19 */,
/* 20 */,
/* 21 */,
/* 22 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PersonProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_config__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__auth_auth__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__ = __webpack_require__(178);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_toPromise__ = __webpack_require__(179);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_toPromise___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_toPromise__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map__ = __webpack_require__(180);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var PersonProvider = /** @class */ (function () {
    function PersonProvider(http, auth) {
        this.http = http;
        this.auth = auth;
    }
    PersonProvider.prototype.post = function (person) {
        var _this = this;
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/pessoa/cadastrobasico";
        person.ClienteId = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].clienteId;
        return new Promise(function (resolve, reject) {
            _this.http.post(url, person)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    PersonProvider.prototype.get = function () {
        var _this = this;
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/pessoa";
        var token = this.auth.getToken();
        var header = { "headers": { "authorization": 'bearer ' + token } };
        return new Promise(function (resolve, reject) {
            _this.http.get(url, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    PersonProvider.prototype.put = function (user) {
        var _this = this;
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/pessoa/editar";
        var token = this.auth.getToken();
        var header = { "headers": { "authorization": 'bearer ' + token } };
        user.ClienteId = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].clienteId;
        return new Promise(function (resolve, reject) {
            _this.http.post(url, user, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    PersonProvider.prototype.putFoto = function (image) {
        var _this = this;
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/pessoa/UploadFotoBase64";
        var token = this.auth.getToken();
        var header = { "headers": { "authorization": 'bearer ' + token } };
        return new Promise(function (resolve, reject) {
            _this.http.post(url, { Base64: image }, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    PersonProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_3__auth_auth__["a" /* AuthProvider */]])
    ], PersonProvider);
    return PersonProvider;
}());

//# sourceMappingURL=person.js.map

/***/ }),
/* 23 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConteudoProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_config__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__auth_auth__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ConteudoProvider = /** @class */ (function () {
    function ConteudoProvider(http, auth) {
        this.http = http;
        this.auth = auth;
    }
    ConteudoProvider.prototype.getPage = function (token, idPage) {
        var _this = this;
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/Paginas/" + idPage;
        var header = { "headers": { "Content-Type": 'application/json', "Authorization": "Bearer " + token } };
        return new Promise(function (resolve, reject) {
            _this.http.get(url, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    ConteudoProvider.prototype.getConteudo = function (token, pag, skip, tipo) {
        var _this = this;
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/Conteudos?TipoConteudoId=" + tipo + "&$skip=" + skip + "&$top=" + pag;
        var header = { "headers": { "Content-Type": 'application/json', "Authorization": "Bearer " + token } };
        return new Promise(function (resolve, reject) {
            _this.http.get(url, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    ConteudoProvider.prototype.getTiposConteudo = function (token) {
        var _this = this;
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/TiposConteudo";
        var header = { "headers": { "Content-Type": 'application/json', "Authorization": "Bearer " + token } };
        return new Promise(function (resolve, reject) {
            _this.http.get(url, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    ConteudoProvider.prototype.getDetalhe = function (token, id) {
        var _this = this;
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/Conteudos/" + id;
        var header = { "headers": { "Content-Type": 'application/json', "Authorization": "Bearer " + token } };
        return new Promise(function (resolve, reject) {
            _this.http.get(url, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    ConteudoProvider.prototype.postLike = function (conteudoId, pessoaId) {
        var _this = this;
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/Conteudos/Curtir";
        var token = this.auth.getToken();
        var body = {
            ConteudoId: conteudoId,
            PessoaId: pessoaId
        };
        var header = { "headers": { "authorization": 'bearer ' + token } };
        return new Promise(function (resolve, reject) {
            _this.http.post(url, body, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                result.Titulo == "Conteúdo curtido com sucesso!" ? result.Curtiu = true : result.Curtiu = false;
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    ConteudoProvider.prototype.postShare = function (conteudoId, pessoaId, rede) {
        var _this = this;
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/Conteudos/Compartilhar";
        var token = this.auth.getToken();
        var body = {
            ConteudoId: conteudoId,
            PessoaId: pessoaId,
            RedeSocial: rede
        };
        var header = { "headers": { "authorization": 'bearer ' + token } };
        return new Promise(function (resolve, reject) {
            _this.http.post(url, body, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    ConteudoProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_3__auth_auth__["a" /* AuthProvider */]])
    ], ConteudoProvider);
    return ConteudoProvider;
}());

//# sourceMappingURL=conteudo.js.map

/***/ }),
/* 24 */,
/* 25 */,
/* 26 */,
/* 27 */,
/* 28 */,
/* 29 */,
/* 30 */,
/* 31 */,
/* 32 */,
/* 33 */,
/* 34 */,
/* 35 */,
/* 36 */,
/* 37 */,
/* 38 */,
/* 39 */,
/* 40 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SocialSharePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_shared_service_shared_service__ = __webpack_require__(183);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_person_person__ = __webpack_require__(22);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SocialSharePage = /** @class */ (function () {
    function SocialSharePage(navCtrl, navParams, shared, pessoa, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.shared = shared;
        this.pessoa = pessoa;
        this.viewCtrl = viewCtrl;
        this.conteudo = navParams.data;
    }
    SocialSharePage.prototype.ionViewDidLoad = function () {
    };
    SocialSharePage.prototype.compartilhar = function (conteudo, rede) {
        var _this = this;
        this.pessoa.get().then(function (result) {
            conteudo.PessoaId = result.Id;
            _this.shared.SwitchShare(conteudo, rede);
        }, function (erro) {
        });
    };
    SocialSharePage.prototype.close = function () {
        this.viewCtrl.dismiss();
    };
    SocialSharePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'social-share',template:/*ion-inline-start:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/social-share/social-share.html"*/'<div class="container-round">\n    <div class="box">\n       <button class="button-close2" (click)="close()"><ion-icon name="close"></ion-icon></button>\n        <div (click)="compartilhar(conteudo,\'whatsapp\')" class="icon-social">\n            <img src="../www/assets/imgs/social/whatsapp.jpg" alt="" width="40">\n        </div>\n        <div (click)="compartilhar(conteudo,\'facebook\')" class="icon-social">\n            <img src="../www/assets/imgs/social/facebook.jpg" alt="" width="40">\n        </div>\n        <div (click)="compartilhar(conteudo,\'twitter\')" class="icon-social">\n            <img src="../www/assets/imgs/social/twitter.jpg" alt="" width="40">\n        </div>\n        <div (click)="compartilhar(conteudo,\'instagram\')" class="icon-social">\n            <img src="../www/assets/imgs/social/instagram.jpg" alt="" width="40">\n        </div>\n    </div>\n</div>'/*ion-inline-end:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/social-share/social-share.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_2__providers_shared_service_shared_service__["a" /* SharedService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_shared_service_shared_service__["a" /* SharedService */],
            __WEBPACK_IMPORTED_MODULE_3__providers_person_person__["a" /* PersonProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */]])
    ], SocialSharePage);
    return SocialSharePage;
}());

//# sourceMappingURL=social-share.js.map

/***/ }),
/* 41 */,
/* 42 */,
/* 43 */,
/* 44 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotificationProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_config__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__auth_auth__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var NotificationProvider = /** @class */ (function () {
    function NotificationProvider(http, auth) {
        this.http = http;
        this.auth = auth;
    }
    NotificationProvider.prototype.Count = function () {
        var _this = this;
        var token = this.auth.getToken();
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/Mensagens";
        var header = { "headers": { "Content-Type": 'application/json', "Authorization": "Bearer " + token } };
        return new Promise(function (resolve, reject) {
            _this.http.get(url, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result.length);
            }, function (error) {
                reject(error);
            });
        });
    };
    NotificationProvider.prototype.Get = function () {
        var _this = this;
        var token = this.auth.getToken();
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/Mensagens";
        var header = { "headers": { "Content-Type": 'application/json', "Authorization": "Bearer " + token } };
        return new Promise(function (resolve, reject) {
            _this.http.get(url, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    NotificationProvider.prototype.GetDetail = function (id) {
        var _this = this;
        var token = this.auth.getToken();
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/Mensagens/" + id;
        var header = { "headers": { "Content-Type": 'application/json', "Authorization": "Bearer " + token } };
        return new Promise(function (resolve, reject) {
            _this.http.get(url, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    NotificationProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_3__auth_auth__["a" /* AuthProvider */]])
    ], NotificationProvider);
    return NotificationProvider;
}());

//# sourceMappingURL=notificacao.js.map

/***/ }),
/* 45 */,
/* 46 */,
/* 47 */,
/* 48 */,
/* 49 */,
/* 50 */,
/* 51 */,
/* 52 */,
/* 53 */,
/* 54 */,
/* 55 */,
/* 56 */,
/* 57 */,
/* 58 */,
/* 59 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_searchbar_searchbar__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_agenda_agenda__ = __webpack_require__(122);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_auth_auth__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_utils_utils__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_conteudo_conteudo__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_person_person__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_social_sharing__ = __webpack_require__(95);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_shared_service_shared_service__ = __webpack_require__(183);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__social_share_social_share__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__providers_notificacao_notificacao__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pesquisa_pesquisa__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__quero_contribuir_quero_contribuir__ = __webpack_require__(97);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};















var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, navParams, actionsheetCtrl, platform, modalCtrl, agenda, auth, utils, conteudo, person, notification, events, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.actionsheetCtrl = actionsheetCtrl;
        this.platform = platform;
        this.modalCtrl = modalCtrl;
        this.agenda = agenda;
        this.auth = auth;
        this.utils = utils;
        this.conteudo = conteudo;
        this.person = person;
        this.notification = notification;
        this.events = events;
        this.loadingCtrl = loadingCtrl;
        this.PesquisaPage = __WEBPACK_IMPORTED_MODULE_12__pesquisa_pesquisa__["b" /* PesquisaPage */];
        this.QueroContribuirPage = __WEBPACK_IMPORTED_MODULE_13__quero_contribuir_quero_contribuir__["a" /* QueroContribuirPage */];
        this.tab = "noticias";
        this.agendaList = [];
        this.noticiasList = [];
        this.start = 3;
        this.skip = 0;
        this.FiltroEventos = [
            { name: 'Todos', id: 0, active: true },
            { name: 'Hoje', id: 1, active: false },
            { name: 'Essa Semana', id: 2, active: false },
            { name: 'Esse mês', id: 3, active: false },
        ];
        // Tipo de Conteudo Noticias
        this.tipoConteudo = 2;
        this.estados = new Array();
        this.filter = {
            cidade: '',
            estado: ''
        };
        this.selectOptEstados = {
            title: 'Selecione o estado',
            subTitle: 'Selecione o estado',
            checked: true
        };
        this.selectOptCidades = {
            title: 'Selecione a cidade',
            subTitle: 'Selecione a cidade',
            checked: true
        };
        this.loadDataUser();
    }
    HomePage.prototype.doRefresh = function (refresher) {
        var _this = this;
        console.log('Begin async operation', refresher);
        setTimeout(function () {
            console.log('Async operation has ended');
            _this.loadNews();
            refresher.complete();
        }, 1000);
    };
    HomePage.prototype.openDetail = function (page, id) {
        this.navCtrl.push(page, { id: id });
    };
    HomePage.prototype.optionsContribuition = function () {
        var actionSheet = this.actionsheetCtrl.create({
            cssClass: 'action-sheets-basic-page',
            buttons: [
                {
                    text: 'Colobare para mudanças',
                    icon: 'ios-thumbs-up-outline',
                    handler: function () {
                    }
                },
                {
                    text: 'Responda uma pesquisa',
                    icon: 'ios-paper-outline',
                    handler: function () {
                    }
                },
                {
                    text: 'Participe enviando uma sugestão',
                    icon: 'ios-text-outline',
                    handler: function () {
                        console.log('Share clicked');
                    }
                },
                {
                    text: 'Conheça a proposta de governo',
                    icon: 'ios-search-outline',
                    handler: function () {
                        console.log('Play clicked');
                    }
                },
                {
                    role: 'cancel',
                    icon: !this.platform.is('ios') ? 'close' : null,
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });
        actionSheet.present();
    };
    HomePage.prototype.searchToggle = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__components_searchbar_searchbar__["a" /* SearchbarComponent */]);
        modal.present();
    };
    HomePage.prototype.loadNews = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.conteudo.getConteudo(_this.auth.getToken(), _this.start, _this.skip, _this.tipoConteudo)
                .then(function (data) {
                _this.noticiasList = _this.noticiasList.concat(data);
                console.log(data);
                resolve(true);
            });
        });
    };
    HomePage.prototype.loadAgenda = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.agenda.getLista(_this.auth.getToken(), _this.start, _this.skip)
                .then(function (data) {
                _this.agendaList = _this.agendaList.concat(data);
                resolve(true);
            });
        });
    };
    HomePage.prototype.doInfinite = function (infiniteScroll, tab) {
        var _this = this;
        this.start += 3;
        this.skip += 3;
        setTimeout(function () {
            if (_this.tab == 'agenda') {
                _this.loadAgenda().then(function () {
                    infiniteScroll.complete();
                });
            }
            else {
                _this.loadNews().then(function () {
                    infiniteScroll.complete();
                });
            }
        }, 600);
    };
    HomePage.prototype.onSelectCidade = function (uf) {
        var _this = this;
        this.utils.getCidades(uf).then(function (result) { _this.cidades = result; }, function (error) { });
    };
    HomePage.prototype.getEstados = function () {
        var _this = this;
        this.utils.getEstados().then(function (result) { _this.estados = result; }, function (error) { });
    };
    HomePage.prototype.filterByCity = function (filter) {
        var _this = this;
        this.start = 3;
        this.skip = 0;
        return new Promise(function (resolve) {
            _this.agenda.filterCity(_this.auth.getToken(), filter, _this.start, _this.skip)
                .then(function (data) {
                if (data != null) {
                    _this.agendaList = data;
                }
                else {
                    _this.utils.showAlert("Nenhum resultado encontrado!");
                    _this.start = 2;
                }
                resolve(true);
            });
        });
    };
    HomePage.prototype.postLike = function (conteudo) {
        var _this = this;
        var idPost = conteudo.Id;
        this.person.get()
            .then(function (response) {
            var person = response;
            _this.conteudo.postLike(idPost, person.Id)
                .then(function (response) {
                if (response.Curtiu && !conteudo.JaCurtiu) {
                    conteudo.Curtidas += 1;
                }
                else {
                    conteudo.Curtidas -= 1;
                }
                conteudo.JaCurtiu = response.Curtiu;
                if (response.GanhouPonto) {
                    _this.utils.showModalSucesso(response.Mensagem, "Continue interagindo", "OK");
                }
            });
        });
    };
    HomePage.prototype.ionViewDidLoad = function () {
        this.getEstados();
        this.loadAgenda();
        this.loadNews();
    };
    HomePage.prototype.toggleClass = function (element, type) {
        var floorElements = document.getElementsByClassName("filtroeventos");
        for (var i = 0; i < floorElements.length; i++) {
            if (floorElements[i].innerText != element.currentTarget.innerText) {
                floorElements[i].classList.remove('active');
            }
        }
        for (var j = 0; j < floorElements.length; j++) {
            if (floorElements[j].innerText == element.currentTarget.innerText) {
                floorElements[j].classList.add('active');
            }
        }
        this.filterByData(type);
    };
    HomePage.prototype.filterByData = function (type) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.agenda.filterData(_this.auth.getToken(), type)
                .then(function (data) {
                _this.agendaList = data;
                resolve(true);
            });
        });
    };
    HomePage.prototype.showShare = function (conteudo) {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_10__social_share_social_share__["a" /* SocialSharePage */], conteudo);
        modal.present();
    };
    HomePage.prototype.loadDataUser = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: 'Carregando...'
        });
        loading.present();
        this.getPersonDetail().then(function (result) {
            _this.getNotificationsCount().then(function (result2) {
                loading.dismiss();
                _this.events.publish('profile:count', _this.profile, _this.count);
            });
        });
    };
    HomePage.prototype.getPersonDetail = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.person.get()
                .then(function (response) {
                _this.profile = response;
                console.log(response);
                resolve(true);
            }, function (error) {
                console.log(error);
            });
        });
    };
    HomePage.prototype.getNotificationsCount = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.notification.Count().then(function (response) {
                _this.count = response;
                resolve(true);
            }, function (error) {
                _this.count = 0;
                console.log(error);
                resolve(true);
            });
        });
    };
    HomePage.prototype.navPage = function (page) {
        this.navCtrl.setRoot(page);
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-home',template:/*ion-inline-start:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/home/home.html"*/'<ion-header color="header">\n    <ion-navbar>\n      <button ion-button menuToggle>\n        <!-- <ion-icon name="menu"></ion-icon> -->\n        <i class="fa fa-bars" aria-hidden="true"></i>\n      </button>\n      <ion-title text-center>MaisQueVoto MDB</ion-title>\n      <ion-buttons end>\n        <button (click)="searchToggle()" class="search-header">\n        <!-- <ion-icon name="search"></ion-icon> -->\n        <i class="fa fa-search" aria-hidden="true"></i>\n      </button>\n      </ion-buttons>\n    </ion-navbar>\n</ion-header>\n\n<ion-content>\n    <ion-refresher (ionRefresh)="doRefresh($event)">\n        <ion-refresher-content></ion-refresher-content>\n      </ion-refresher>\n      <div class="bannerItem" (click)="navPage(PesquisaPage)">\n        <button class="button" ion-button (click)="navPage(QueroContribuirPage)">\n            Fazer Minha doação\n        </button>\n      </div>\n\n      <div class="noticias-tab-content tab-content">\n          <section padding>\n          <h2>Notícias</h2>\n          <div>\n              <ion-card *ngFor="let conteudo of noticiasList">\n                <img (click)="openDetail(\'noticias\', conteudo.Id)" [src]="conteudo.Imagem"/>\n                <ion-card-content (click)="openDetail(\'noticias\', conteudo.Id)">\n                  <span class="spanCategoria">{{conteudo.CategoriaConteudo.Nome}}</span>\n                  <ion-card-title>{{conteudo.Titulo}}</ion-card-title>\n                  <div [innerHtml]="conteudo.Texto | excerpt:120"></div>\n                </ion-card-content>\n                <ion-row class="footer-card">\n                  <ion-col>\n                    <button ion-button icon-left clear small>\n                      <div>{{conteudo.Hora}}</div>\n                    </button>\n                  </ion-col>\n                  <ion-col>\n                    <button ion-button icon-right text-left clear small (click)="showShare(conteudo)">\n                      <i class="fa fa-share-alt" aria-hidden="true"></i>\n                    </button>\n                  </ion-col>\n                </ion-row>\n              </ion-card>\n            </div>\n          </section>\n        </div>\n \n</ion-content>\n'/*ion-inline-end:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/home/home.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_8__ionic_native_social_sharing__["a" /* SocialSharing */], __WEBPACK_IMPORTED_MODULE_9__providers_shared_service_shared_service__["a" /* SharedService */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_agenda_agenda__["a" /* AgendaProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_utils_utils__["a" /* UtilsProvider */],
            __WEBPACK_IMPORTED_MODULE_6__providers_conteudo_conteudo__["a" /* ConteudoProvider */],
            __WEBPACK_IMPORTED_MODULE_7__providers_person_person__["a" /* PersonProvider */],
            __WEBPACK_IMPORTED_MODULE_11__providers_notificacao_notificacao__["a" /* NotificationProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),
/* 60 */,
/* 61 */,
/* 62 */,
/* 63 */,
/* 64 */,
/* 65 */,
/* 66 */,
/* 67 */,
/* 68 */,
/* 69 */,
/* 70 */,
/* 71 */,
/* 72 */,
/* 73 */,
/* 74 */,
/* 75 */,
/* 76 */,
/* 77 */,
/* 78 */,
/* 79 */,
/* 80 */,
/* 81 */,
/* 82 */,
/* 83 */,
/* 84 */,
/* 85 */,
/* 86 */,
/* 87 */,
/* 88 */,
/* 89 */,
/* 90 */,
/* 91 */,
/* 92 */,
/* 93 */,
/* 94 */,
/* 95 */,
/* 96 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return PesquisaPage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PesquisaModalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular_components_loading_loading_controller__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_pesquisa_pesquisa__ = __webpack_require__(123);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_searchbar_searchbar__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var PesquisaPage = /** @class */ (function () {
    function PesquisaPage(navCtrl, navParams, modalCtrl, loadingCtrl, pesquisa) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.loadingCtrl = loadingCtrl;
        this.pesquisa = pesquisa;
        this.getData();
    }
    PesquisaPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PesquisaPage');
    };
    PesquisaPage.prototype.searchToggle = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__components_searchbar_searchbar__["a" /* SearchbarComponent */]);
        modal.present();
    };
    PesquisaPage.prototype.presentProfileModal = function (pesquisa) {
        if (pesquisa.JaVotou) {
            this.getDetail(pesquisa.Id);
        }
        else {
            var profileModal = this.modalCtrl.create(PesquisaModalPage, pesquisa, {
                showBackdrop: true,
                cssClass: 'modal-box'
            });
            profileModal.present();
        }
    };
    PesquisaPage.prototype.doRefresh = function (refresher) {
        var _this = this;
        console.log('Begin async operation', refresher);
        setTimeout(function () {
            console.log('Async operation has ended');
            _this.getData();
            refresher.complete();
        }, 1000);
    };
    PesquisaPage.prototype.doInfinite = function (infiniteScroll) {
        console.log('Begin async operation');
        setTimeout(function () {
            infiniteScroll.complete();
        }, 500);
    };
    PesquisaPage.prototype.getData = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: 'Carregando...'
        });
        loading.present();
        this.pesquisa.findAll().then(function (result) {
            loading.dismiss();
            _this.listaPesquisa = result;
            console.log(result);
            if (_this.listaPesquisa == '') {
                _this.NadaEncontrado = true;
            }
        }, function (error) {
            loading.dismiss();
            _this.NadaEncontrado = true;
            console.log(error);
        });
    };
    PesquisaPage.prototype.getDetail = function (id) {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: 'Carregando...'
        });
        loading.present();
        this.pesquisa.findById(id).then(function (result2) {
            loading.dismiss();
            _this.navCtrl.push('pesquisa-resposta', result2);
            console.log(result2);
        }, function (error2) {
            loading.dismiss();
            console.log(error2);
        });
    };
    PesquisaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-pesquisa',template:/*ion-inline-start:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/pesquisa/pesquisa.html"*/'<ion-header color="header">\n  <ion-navbar>\n   \n    <button ion-button menuToggle>\n      <i class="fa fa-bars" aria-hidden="true"></i>\n    </button>\n    <ion-title text-center>Pesquisa de Opinião</ion-title>\n    <ion-buttons end>\n      <button (click)="searchToggle()" class="search-header">\n      <ion-icon name="search"></ion-icon>\n    </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n    <ion-refresher (ionRefresh)="doRefresh($event)">\n        <ion-refresher-content></ion-refresher-content>\n      </ion-refresher>\n  <ion-list margin-top>\n    <ion-card (click)="presentProfileModal(p.Pesquisa)" *ngFor="let p of listaPesquisa">\n      <ion-card-content class="flex-card">\n        <div>\n          <!-- <small class="blue-color" text-uppercase>{{p.Pesquisa.Tema}}</small> -->\n          <span class="spanCategoria">{{p.Pesquisa.Tema}}</span>\n          <ion-card-title>{{p.Pesquisa.Pergunta}}</ion-card-title>\n        </div>\n        <ion-icon class="set" name="ios-arrow-forward"></ion-icon>\n      </ion-card-content>\n    </ion-card>\n    </ion-list>\n    <div class="NoContentmsg" *ngIf="NadaEncontrado">\n      <!-- <img src="" alt=""> -->\n      <i class="fa fa-frown-o" aria-hidden="true"></i>\n      <p>Nenhuma pesquisa encontrada</p>\n    </div>\n  <ion-infinite-scroll (ionInfinite)="doInfinite($event)">\n    <ion-infinite-scroll-content></ion-infinite-scroll-content>\n  </ion-infinite-scroll>\n</ion-content>\n\n'/*ion-inline-end:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/pesquisa/pesquisa.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ModalController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular_components_loading_loading_controller__["a" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_3__providers_pesquisa_pesquisa__["a" /* PesquisaProvider */]])
    ], PesquisaPage);
    return PesquisaPage;
}());

var PesquisaModalPage = /** @class */ (function () {
    function PesquisaModalPage(params, navCtrl, modalCtrl, loadingCtrl, pesquisa) {
        this.navCtrl = navCtrl;
        this.modalCtrl = modalCtrl;
        this.loadingCtrl = loadingCtrl;
        this.pesquisa = pesquisa;
        this.pesquisaDetail = params.data;
        console.log('objeto: ', params.data);
    }
    PesquisaModalPage_1 = PesquisaModalPage;
    PesquisaModalPage.prototype.close = function () {
        this.navCtrl.pop();
    };
    PesquisaModalPage.prototype.getDetail = function (id) {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: 'Carregando...'
        });
        loading.present();
        this.pesquisa.findById(id).then(function (result2) {
            loading.dismiss();
            _this.navCtrl.push('pesquisa-resposta', result2);
            console.log(result2);
        }, function (error2) {
            loading.dismiss();
            console.log(error2);
        });
    };
    PesquisaModalPage.prototype.answerQuestion = function (status) {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: 'Carregando...'
        });
        loading.present();
        this.pesquisa.post(status).then(function (result) {
            var profileModal = _this.modalCtrl.create(PesquisaModalPage_1, result, {
                showBackdrop: true,
                cssClass: 'modal-box'
            });
            profileModal.dismiss();
            _this.getDetail(result.Id);
            // this.navCtrl.push('pesquisa-resposta', { ExibirGrafico: false, Pontuacao: result.Pontuacao });
        }, function (error) {
            loading.dismiss();
            console.log(error);
        });
    };
    PesquisaModalPage = PesquisaModalPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'modal-pesquisa',template:/*ion-inline-start:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/pesquisa/pesquisa-modal.html"*/'<div class="container-round" text-center>\n    <button class="button-close" (click)="close()"><ion-icon name="close"></ion-icon></button>\n    <header>\n      <strong text-uppercase>Pesquisa de Opinião</strong>\n      <p>Responda e ganhe {{pesquisaDetail.Pontuacao}} pontos</p>\n    </header>\n    <div class="box">\n      <small class="blue-color" text-uppercase>Tema</small>\n      <h4 class="bold">{{pesquisaDetail.Tema}}</h4>\n      <div class="block-text">\n        <p>{{pesquisaDetail.Pergunta}}</p>\n      </div>\n      <div class="options-modal" >\n        <button class="negative" (click)="answerQuestion(pesquisaDetail.negative.Id)">\n          <div class="ico">\n              <ion-icon name="close"></ion-icon>\n          </div>\n          <small text-uppercase>{{pesquisaDetail.negative.Resposta}}</small>\n        </button>\n        <button class="agree" (click)="answerQuestion(pesquisaDetail.agree.Id)">\n          <div class="ico">\n              <ion-icon name="checkmark"></ion-icon>\n          </div>\n          <small text-uppercase>{{pesquisaDetail.agree.Resposta}}</small>\n        </button>\n      </div>\n    </div>\n</div>\n\n'/*ion-inline-end:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/pesquisa/pesquisa-modal.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ModalController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular_components_loading_loading_controller__["a" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_3__providers_pesquisa_pesquisa__["a" /* PesquisaProvider */]])
    ], PesquisaModalPage);
    return PesquisaModalPage;
    var PesquisaModalPage_1;
}());

//# sourceMappingURL=pesquisa.js.map

/***/ }),
/* 97 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return QueroContribuirPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_searchbar_searchbar__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_brmasker_ionic_3__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_doacao_doacao__ = __webpack_require__(98);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_stripe__ = __webpack_require__(331);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_utils_utils__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_common__ = __webpack_require__(31);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var QueroContribuirPage = /** @class */ (function () {
    function QueroContribuirPage(navCtrl, navParams, modalCtrl, formBuilder, BrMaskerModule, doacao, loadingCtrl, stripe, utils, currencyPipe) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.formBuilder = formBuilder;
        this.BrMaskerModule = BrMaskerModule;
        this.doacao = doacao;
        this.loadingCtrl = loadingCtrl;
        this.stripe = stripe;
        this.utils = utils;
        this.currencyPipe = currencyPipe;
        this.step = 1;
        this.formaPgto = 'cartao';
        this.bandeira = '';
        this.model = {
            ClienteId: 0,
            CPFCNPJ: '',
            Nome: '',
            Email: '',
            Celular: '',
            Sexo: 0,
            Valor2: '',
            CEP: '',
            Complemento: '',
            Endereco: '',
            Cidade: '',
            Estado: '',
            Bairro: '',
            EnderecoNumero: '',
            EventoId: null,
            BemServico: '',
            EspecieRecurso: '',
            StatusSistema: 1,
            NumeroDocumento: '',
            NumeroAutorizacao: '',
            ValidadeDocumento: '',
            NomeDocumento: '',
            DataNascimentoDocumento: '',
            BandeiraDocumento: 0,
            MesDocumento: '',
            AnoDocumento: '',
            check1: false,
            check2: false
        };
        this.years = new Array();
        this.erroParte1 = true;
        this.erroParte2 = true;
        this.errorNome = false;
        this.errorEmail = false;
        this.errorEmail2 = false;
        this.errorCelular = false;
        this.errorCPF = false;
        this.errorCPF2 = false;
        this.errorSexo = false;
        this.errorValor = false;
        this.errorCheck1 = false;
        this.errorCheck2 = false;
        this.candidato = this.navParams.get('candidato');
        console.log(this.candidato, 'CAndidato Info');
        this.getDoacaoFromStorage();
    }
    QueroContribuirPage_1 = QueroContribuirPage;
    QueroContribuirPage.prototype.ngOnInit = function () {
        this.contribuirForm = new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["FormGroup"]({
            nome: new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["FormControl"](this.model.Nome, {
                validators: [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required]
            }),
            email: this.formBuilder.control(this.model.Email, [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required, __WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].email]),
            cpf: this.formBuilder.control(this.model.CPFCNPJ, [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required]),
            celular: this.formBuilder.control(this.model.Celular, [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required]),
            sexo: this.formBuilder.control(this.model.Sexo, [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required])
        });
        this.contribuirStep2Form = new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["FormGroup"]({
            valor: this.formBuilder.control('', [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required]),
            check1: this.formBuilder.control(false, [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required]),
            check2: this.formBuilder.control(false, [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required]),
        });
        this.contribuirStep3Form = new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["FormGroup"]({
            cep: this.formBuilder.control('', [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required]),
            endereco: this.formBuilder.control('', [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required]),
            cidade: this.formBuilder.control('', [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required]),
            estado: this.formBuilder.control('', [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required]),
            bairro: this.formBuilder.control('', [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required]),
            complemento: this.formBuilder.control(''),
            numero: this.formBuilder.control(''),
        });
        this.contribuirStep4Form = new __WEBPACK_IMPORTED_MODULE_4__angular_forms__["FormGroup"]({
            recurso: this.formBuilder.control('', [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required]),
            documento: this.formBuilder.control('', [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required]),
            cvv: this.formBuilder.control('', [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required]),
            mes: this.formBuilder.control('', [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required]),
            ano: this.formBuilder.control('', [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required]),
            nomeimpresso: this.formBuilder.control('', [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required]),
            data: this.formBuilder.control('', [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["Validators"].required]),
        });
    };
    QueroContribuirPage.prototype.searchToggle = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__components_searchbar_searchbar__["a" /* SearchbarComponent */]);
        modal.present();
    };
    QueroContribuirPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.carregarConfig().then(function () {
            var step = parseInt(_this.navParams.get('step'));
            if (step) {
                _this.step = step;
            }
            else {
                _this.getDoacaoFromStorage();
                _this.step = 1;
            }
        });
    };
    QueroContribuirPage.prototype.goBack = function () {
        var _this = this;
        this.navCtrl.pop().catch(function () {
            _this.navCtrl.setRoot('HomePage');
        });
    };
    QueroContribuirPage.prototype.goToStep = function (step, model) {
        if (model != undefined) {
            localStorage.setItem('doacao', JSON.stringify(model));
        }
        this.validaCampos1(model);
        this.navCtrl.push(QueroContribuirPage_1, { step: step });
    };
    QueroContribuirPage.prototype.carregarConfig = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.doacao.get().then(function (result) {
                _this.configuracao = result;
                console.log(result);
                resolve(true);
            });
        });
    };
    QueroContribuirPage.prototype.enviarDoacao = function (model) {
        var _this = this;
        var vish = '';
        this.stripe.getCardType(model.NumeroDocumento)
            .then(function (response) {
            _this.bandeira = response;
        });
    };
    QueroContribuirPage.prototype.loadYears = function () {
        var y = (new Date()).getFullYear();
        for (var i = 0; i <= 12; i++) {
            this.years.push(y + i);
        }
    };
    QueroContribuirPage.prototype.validaPart1 = function () {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var validaemail = re.test(this.model.Email);
        var validacpf = this.utils.cpf(this.model.CPFCNPJ.replace(".", "").replace(".", "").replace("-", ""));
        if (this.model.Nome != '' && (this.model.Email != '' && validaemail) && this.model.Celular != '' && (this.model.CPFCNPJ != '' && validacpf) && this.model.Sexo > 0) {
            this.erroParte1 = false;
            return true;
        }
        else {
            return false;
        }
    };
    QueroContribuirPage.prototype.validaPart2 = function () {
        if (this.model.Valor2 != '' &&
            parseFloat(this.model.Valor2.replace("R$ ", "")) >= this.configuracao.DoacaoValorMinimo &&
            parseFloat(this.model.Valor2.replace("R$ ", "")) <= this.configuracao.DoacaoValorMaximo &&
            this.model.check1 &&
            this.model.check2) {
            this.erroParte2 = false;
            return true;
        }
        else {
            return false;
        }
    };
    QueroContribuirPage.prototype.validaCampos1 = function (model) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (model.Nome == '') {
            this.errorNome = true;
            this.erroParte1 = true;
        }
        else if (model.Email == '') {
            this.errorEmail = true;
            this.erroParte1 = true;
        }
        else if (!re.test(model.Email)) {
            this.errorEmail2 = true;
            this.erroParte1 = true;
        }
        else if (model.Celular == '') {
            this.errorCelular = true;
            this.erroParte1 = true;
        }
        else if (model.CPFCNPJ == '') {
            this.errorCPF = true;
            this.erroParte1 = true;
        }
        else if (!this.utils.cpf(model.CPFCNPJ.replace(".", "").replace(".", "").replace("-", ""))) {
            this.errorCPF2 = true;
            this.erroParte1 = true;
        }
        else {
            this.erroParte1 = false;
        }
        console.log(this.erroParte1);
    };
    QueroContribuirPage.prototype.getDoacaoFromStorage = function () {
        if (localStorage.getItem('doacao') != null) {
            this.model = JSON.parse(localStorage.getItem('doacao'));
            console.log(this.model);
            this.validaCampos1(this.model);
        }
    };
    QueroContribuirPage.prototype.selecionarValor = function (valor) {
        this.model.Valor2 = valor;
    };
    QueroContribuirPage.prototype.convert = function (amount) {
        var money = amount.target.value.replace(",", ".");
        this.model.Valor2 = this.currencyPipe.transform(money, 'R$', true, '1.2-2');
        this.validaPart2();
    };
    QueroContribuirPage = QueroContribuirPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-quero-contribuir',template:/*ion-inline-start:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/quero-contribuir/quero-contribuir.html"*/'<!-- @TODO aplicar html e css para iOS -->\n<ion-header color="header">\n  <style>\n   .qc-group-btn button {\n\n    font-weight: 700;\n}\n    </style>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title text-center>Quero contribuir</ion-title>\n    <ion-buttons end>\n      <!-- <button (click)="searchToggle()" class="search-header">\n        <ion-icon name="search"></ion-icon>\n      </button> -->\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content *ngIf="step === 1">\n\n  <header class="qc-header">\n    <div class="photo" [ngStyle]="{\'background\': \'url(\' + candidato.Imagem + \') no-repeat\'}"></div>\n    <div class="mask"></div>\n    <h2>{{candidato.Nome}}</h2>\n    <h5>Faça sua doação</h5>\n  </header>\n\n  <form class="qc-form" [formGroup]="contribuirForm" novalidate>\n    <h3>Identifique-se</h3>\n\n    <ion-list>\n\n      <ion-item no-lines>\n        <ion-input type="text" placeholder="Nome" name="nome" formControlName="nome" [(ngModel)]= "model.Nome" (keyup)="validaPart1()"></ion-input>\n        <span class="error" ng-show="errorNome">O campo Nome é obrigatório</span>\n      </ion-item>\n\n      <ion-item no-lines>\n        <ion-input type="email" placeholder="E-mail" name="email" formControlName="email" [(ngModel)]= "model.Email" (keyup)="validaPart1()"></ion-input>\n        <span class="error" ng-show="errorEmail">O campo E-mail é obrigatório</span>\n        <span class="error" ng-show="errorEmail2">O campo E-mail é invalido</span>\n      </ion-item>\n\n      <ion-item no-lines>\n        <ion-input type="tel" placeholder="CPF" [brmasker]="{person: true}" name="cpf" formControlName="cpf" [(ngModel)]= "model.CPFCNPJ" (keyup)="validaPart1()"></ion-input>\n        <span class="error" ng-show="errorCPF">O campo CPF é obrigatório</span>\n        <span class="error" ng-show="errorCPF2">O campo CPF é invalido</span>\n      </ion-item>\n\n      <ion-item no-lines>\n        <ion-input type="tel" placeholder="Celular"   [brmasker]="{phone: true}" name="celular" formControlName="celular" [(ngModel)]= "model.Celular" (keyup)="validaPart1()"></ion-input>\n        <span class="error" ng-show="errorCelular">O campo Celular é obrigatório</span>\n      </ion-item>\n\n      <ion-item no-lines>\n        <ion-select name="sexo" placeholder="Sexo" cancelText="Cancelar" formControlName="sexo" [selectOptions]="{title: \'Sexo\'}" [(ngModel)]= "model.Sexo" (ionChange)="validaPart1()">\n          <ion-option value="2">Feminino</ion-option>\n          <ion-option value="1">Masculino</ion-option>\n        </ion-select>\n        <span class="error" ng-show="errorSexo">O campo Sexo é obrigatório</span>\n      </ion-item>\n\n      <ion-item no-lines no-margin no-padding class="qc-buttons" style="width: 97%;\n      margin: 0 auto;">\n        <ion-row>\n          <ion-col col-5>\n            <button class="qc-btn-cancel" ion-button block (click)="goBack()">Voltar</button>\n          </ion-col>\n          <ion-col col-7>\n            <button class="qc-btn-submit" ion-button block color="header" (click)="goToStep(2,model)" [disabled]="erroParte1">Continuar</button>\n          </ion-col>\n        </ion-row>\n      </ion-item>\n\n    </ion-list>\n  </form>\n\n</ion-content><!-- / Passo 1 -->\n\n<ion-content *ngIf="step === 2">\n  <header class="qc-header">\n    <div class="photo" [ngStyle]="{\'background\': \'url(\' + candidato.Imagem + \') no-repeat\'}"></div>\n    <div class="mask"></div>\n    <h2>{{candidato.Nome}}</h2>\n    <h5>Faça sua doação</h5>\n  </header>\n\n  <form class="qc-form" [formGroup]="contribuirStep2Form" novalidate>\n    <h3>Selecione o Valor</h3>\n\n    <ion-list>\n\n      <ion-item no-lines class="qc-group-btn">\n        <ion-row>\n          <ion-col col-6>\n            <button ion-button full (click)= "selecionarValor(configuracao.DoacaoValor1)">{{configuracao.DoacaoValor1}}</button>\n          </ion-col>\n          <ion-col col-6>\n            <button ion-button full (click)= "selecionarValor(configuracao.DoacaoValor2)">{{configuracao.DoacaoValor2}}</button>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-6>\n            <button ion-button full (click)= "selecionarValor(configuracao.DoacaoValor3)">{{configuracao.DoacaoValor3}}</button>\n          </ion-col>\n          <ion-col col-6>\n            <button ion-button full  (click)= "selecionarValor(configuracao.DoacaoValor4)">{{configuracao.DoacaoValor4}}</button>\n          </ion-col>\n        </ion-row>\n      </ion-item>\n      <input type="hidden" name="quantia">\n\n      <ion-item no-lines>\n        <ion-label class="qc-label">Outro Valor</ion-label>\n        <ion-input type="text" placeholder="R$" name="valor" value="" [brmasker]="{ money:true }" formControlName="valor" [(ngModel)]= "model.Valor2" (keyup)="convert($event)"></ion-input>\n      </ion-item>\n\n      <ion-item text-wrap no-lines class="qc-info">\n          <ion-icon name="alert" item-start style="    margin: 0!important;\n          color: #333333;    font-size: 1.5em!important;"></ion-icon>\n        <div item-end ><p style="font-size: 1.4rem;\n    color: #333333;\n    font-weight: bold;font-size: 1.5rem!important;">Para valores acima de R$ 1.064,00 a doação deverá ser realizada via TED bancário.</p></div>\n      </ion-item>\n\n      <ion-item text-wrap no-lines>\n        <ion-checkbox align-self-start formControlName="check1" (ionChange)="validaPart2()"></ion-checkbox>\n        <ion-label><p class="p33">Declaro ter ciência de que a legislação eleitoral limita as doações a 10% (dez por cento) dos\n            rendimentos brutos auferidos pelo doador no ano anterior à eleição.</p></ion-label>\n      </ion-item>\n\n      <ion-item text-wrap no-lines>\n        <ion-checkbox align-self-start formControlName="check2" (ionChange)="validaPart2()"></ion-checkbox>\n        <ion-label><p class="p33">Declaro, ainda, que os recursos as serem doados, além de possuírem origem lícita, não são\n            provenientes de fonte vedada, nos <strong>termos do art. 33 da Resolução n<sup>o</sup> 23.553/17 do TSE.</strong></p></ion-label>\n      </ion-item>\n\n      <ion-item no-lines no-margin no-padding class="qc-buttons" style="width: 97%;\n      margin: 0 auto;">\n      <ion-row>\n          <ion-col col-5>\n            <button class="qc-btn-cancel" ion-button block (click)="goToStep(1)">Voltar</button>\n          </ion-col>\n          <ion-col col-7>\n            <button class="qc-btn-submit" ion-button block color="header" (click)="goToStep(3,model)" [disabled]="erroParte2">Continuar</button>\n          </ion-col>\n        </ion-row>\n      </ion-item>\n\n    </ion-list>\n  </form>\n</ion-content><!-- / Passo 2 -->\n\n<ion-content *ngIf="step === 3">\n  <header class="qc-header">\n    <div class="photo" [ngStyle]="{\'background\': \'url(\' + candidato.Imagem + \') no-repeat\'}"></div>\n    <div class="mask"></div>\n    <h2>{{candidato.Nome}}</h2>\n    <h5>Faça sua doação</h5>\n  </header>\n  <form class="qc-form" [formGroup]="contribuirStep3Form" novalidate>\n    <h3>Digite seu endereço</h3>\n\n    <ion-list>\n\n      <ion-item no-lines>\n        <ion-input type="cel" [brmasker]="{mask:\'00000-000\', len:10}" placeholder="CEP" name="cep" formControlName="cep"[(ngModel)]= "model.CEP"></ion-input>\n      </ion-item>\n\n      <ion-item no-lines>\n        <ion-input type="text" placeholder="Estado" name="estado" formControlName="estado" [(ngModel)]= "model.Estado"></ion-input>\n      </ion-item>\n\n      <ion-item no-lines>\n        <ion-input type="text" placeholder="Cidade" name="cidade" formControlName="cidade" [(ngModel)]= "model.Cidade"></ion-input>\n      </ion-item>\n\n      <ion-item no-lines>\n        <ion-input type="text" placeholder="Bairro" name="bairro" formControlName="bairro" [(ngModel)]= "model.Bairro"></ion-input>\n      </ion-item>\n\n      <ion-item no-lines>\n        <ion-input type="text" placeholder="Endereço" name="endereco" formControlName="endereco" [(ngModel)]= "model.Endereco"></ion-input>\n      </ion-item>\n\n      <ion-item no-lines class="qc-two">\n        <ion-input type="text" placeholder="Número" name="numero" size="4" formControlName="numero" [(ngModel)]= "model.EnderecoNumero"></ion-input>\n        <ion-input type="text" placeholder="Complemento" name="complemento" formControlName="complemento"[(ngModel)]= "model.Complemento"></ion-input>\n      </ion-item>\n\n      <ion-item no-lines no-margin no-padding class="qc-buttons" style="width: 97%;\n      margin: 0 auto;">\n      <ion-row>\n          <ion-col col-5>\n            <button class="qc-btn-cancel" ion-button block (click)="goToStep(2)">Voltar</button>\n          </ion-col>\n          <ion-col col-7>\n            <button class="qc-btn-submit" ion-button block color="header" (click)="goToStep(4,model)">Continuar</button>\n          </ion-col>\n        </ion-row>\n      </ion-item>\n\n    </ion-list>\n  </form>\n</ion-content><!-- / Passo 3 -->\n\n<ion-content *ngIf="step === 4">\n  <header class="qc-header">\n    <div class="photo" [ngStyle]="{\'background\': \'url(\' + candidato.Imagem + \') no-repeat\'}"></div>\n    <div class="mask"></div>\n    <h2>{{candidato.Nome}}</h2>\n    <h5>Faça sua doação</h5>\n  </header>\n\n  <form class="qc-form" [formGroup]="contribuirStep4Form" novalidate>\n    <h3>Forma de Pagamento</h3>\n\n    <ion-segment [(ngModel)]="formaPgto" [ngModelOptions]="{standalone: true}">\n      <ion-segment-button value="cartao" text-wrap align-items-center>\n        <ion-icon name="card"></ion-icon>\n        <p style="    line-height: 1;\n        margin-top: 7px;\n        font-size: 1.8rem;\n        width: 55%;">Cartão de Crédito</p>\n      </ion-segment-button>\n      <ion-segment-button value="boleto" text-wrap align-items-center>\n        <ion-icon name="barcode"></ion-icon>\n        <p>Boleto</p>\n      </ion-segment-button>\n    </ion-segment>\n\n    <div [ngSwitch]="formaPgto">\n      <div *ngSwitchCase="\'cartao\'">\n\n        <ion-list padding-top>\n\n          <ion-item no-lines>\n            <ion-label color="dark" stacked>Número do cartão</ion-label>\n            <ion-input type="tel" placeholder="" [brmasker]="{mask:\'00000 0000 0000 0000\', len:20}" name="cartao_numero" formControlName="documento" [(ngModel)]= "model.NumeroDocumento"></ion-input>\n          </ion-item>\n\n          <ion-item no-lines>\n            <ion-label color="dark" stacked>Nome Impresso no Cartão</ion-label>\n            <ion-input type="text" placeholder="" name="cartao_nome" formControlName="nomeimpresso" [(ngModel)]= "model.NomeDocumento"></ion-input>\n          </ion-item>\n\n          <ion-item no-lines>\n            <ion-label color="dark" stacked>Data de Nascimento</ion-label>\n            <ion-input type="tel" [brmasker]="{mask:\'00/00/0000\', len:10}" placeholder="dd/mm/aaaa" name="cartao_dt_nasc" formControlName="data" [(ngModel)]= "model.DataNascimentoDocumento"></ion-input>\n          </ion-item>\n\n          <ion-item no-lines class="no-margin-bottom">\n            <ion-label color="dark" stacked>Validade</ion-label>\n          </ion-item>\n\n          <ion-item no-lines class="qc-two">\n\n            <ion-select name="cartao_val_mes" placeholder="Mês" cancelText="Cancelar" [selectOptions]="{title: \'Mês\'}" formControlName="mes" [(ngModel)]= "model.MesDocumento">\n              <ion-option value="01">Janeiro</ion-option>\n              <ion-option value="02">Fevereiro</ion-option>\n              <ion-option value="03">Março</ion-option>\n              <ion-option value="04">Abril</ion-option>\n              <ion-option value="05">Maio</ion-option>\n              <ion-option value="06">Junho</ion-option>\n              <ion-option value="07">Julho</ion-option>\n              <ion-option value="08">Agosto</ion-option>\n              <ion-option value="09">Setembro</ion-option>\n              <ion-option value="10">Outubro</ion-option>\n              <ion-option value="11">Novembro</ion-option>\n              <ion-option value="12">Dezembro</ion-option>\n            </ion-select>\n\n            <ion-select name="cartao_val_ano" placeholder="Ano" cancelText="Cancelar" [selectOptions]="{title: \'Ano\'}" formControlName="ano" [(ngModel)]= "model.AnoDocumento">\n              <ion-option *ngFor="let ano of years" [value]="ano">{{ano}}</ion-option>\n            </ion-select>\n          </ion-item>\n\n          <ion-item no-lines>\n            <ion-label color="dark" stacked>Código de segurança</ion-label>\n            <ion-input type="text" placeholder="CVV" name="cartao_cvv" maxlength="4" formControlName="cvv" [(ngModel)]= "model.NumeroAutorizacao"></ion-input>\n          </ion-item>\n\n          <ion-item no-lines padding text-center>\n            <img src="/assets/imgs/visa.png" alt="visa">\n            <img src="/assets/imgs/mastercard.png" alt="mastercard">\n          </ion-item>\n\n          <ion-item text-wrap no-lines class="qc-info">\n            <ion-icon name="lock" md="md-lock" item-start></ion-icon>\n            <div item-end>esse ambiente é seguro e os dados do seu cartão não serão armazenados</div>\n          </ion-item>\n\n          <ion-item no-lines no-margin no-padding class="qc-buttons" style="width: 97%;\n          margin: 0 auto;">\n            <ion-row>\n              <ion-col col-5>\n                <button class="qc-btn-cancel" ion-button block (click)="goToStep(3)">Voltar</button>\n              </ion-col>\n              <ion-col col-7>\n                <button class="qc-btn-submit" ion-button block color="header" (click)="enviarDoacao(model)">Continuar</button>\n              </ion-col>\n            </ion-row>\n          </ion-item>\n\n        </ion-list>\n\n      </div>\n\n      <div *ngSwitchCase="\'boleto\'">\n\n        <ion-list>\n          <ion-item>\n            <p text-center>O boleto será enviado para o seu e-mail.</p>\n          </ion-item>\n\n          <ion-item no-lines no-margin no-padding class="qc-buttons">\n            <ion-row>\n              <ion-col col-4>\n                <button class="qc-btn-cancel" ion-button block (click)="goToStep(1)">Voltar</button>\n              </ion-col>\n              <ion-col col-8>\n                <button class="qc-btn-submit" ion-button block color="header" (click)="goToStep(5)">Continuar</button>\n              </ion-col>\n            </ion-row>\n          </ion-item>\n        </ion-list>\n      </div>\n    </div>\n  </form>\n</ion-content><!-- / Passo 4 -->\n\n<ion-content *ngIf="step === 5">\n  <ion-grid>\n    <ion-row align-items-center>\n      <ion-col class="qc-confirmation">\n        <h2>Confirmação</h2>\n\n        <div text-center>\n          <ion-icon name="ios-checkmark-circle-outline"></ion-icon>\n        </div>\n\n        <p>Sua doação foi armazenada com sucesso!</p>\n\n        <ion-list class="qc-form">\n          <ion-item class="qc-buttons">\n            <button class="qc-btn-submit" ion-button block color="header" (click)="goToStep(4)">Doar novamente</button>\n          </ion-item>\n        </ion-list>\n\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>\n\n<ion-footer class="qc-footer">\n  <ion-toolbar>\n    <ion-title *ngIf="step <= 4">Passo <strong>{{step}} de 4</strong></ion-title>\n    <ion-title *ngIf="step == 5">Doação <strong>Concluída</strong></ion-title>\n  </ion-toolbar>\n</ion-footer>\n\n\n'/*ion-inline-end:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/quero-contribuir/quero-contribuir.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_6__ionic_native_stripe__["a" /* Stripe */], __WEBPACK_IMPORTED_MODULE_8__angular_common__["c" /* CurrencyPipe */]]
        }),
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_3_brmasker_ionic_3__["a" /* BrMaskerModule */]
            ]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_4__angular_forms__["FormBuilder"],
            __WEBPACK_IMPORTED_MODULE_3_brmasker_ionic_3__["a" /* BrMaskerModule */],
            __WEBPACK_IMPORTED_MODULE_5__providers_doacao_doacao__["a" /* DoacaoProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_stripe__["a" /* Stripe */],
            __WEBPACK_IMPORTED_MODULE_7__providers_utils_utils__["a" /* UtilsProvider */],
            __WEBPACK_IMPORTED_MODULE_8__angular_common__["c" /* CurrencyPipe */]])
    ], QueroContribuirPage);
    return QueroContribuirPage;
    var QueroContribuirPage_1;
}());

//# sourceMappingURL=quero-contribuir.js.map

/***/ }),
/* 98 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DoacaoProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_config__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__auth_auth__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var DoacaoProvider = /** @class */ (function () {
    function DoacaoProvider(http, auth) {
        this.http = http;
        this.auth = auth;
    }
    DoacaoProvider.prototype.getList = function () {
        var _this = this;
        var token = this.auth.getToken();
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/Doacao/Candidatos";
        var header = { "headers": { "Content-Type": 'application/json', "Authorization": "Bearer " + token } };
        return new Promise(function (resolve, reject) {
            _this.http.get(url, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    DoacaoProvider.prototype.get = function () {
        var _this = this;
        var token = this.auth.getToken();
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/Doacao/Config";
        var header = { "headers": { "Content-Type": 'application/json', "Authorization": "Bearer " + token } };
        return new Promise(function (resolve, reject) {
            _this.http.get(url, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    DoacaoProvider.prototype.post = function (doacao) {
        var _this = this;
        var token = this.auth.getToken();
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/Doacao";
        var header = { "headers": { "Content-Type": 'application/json', "Authorization": "Bearer " + token } };
        return new Promise(function (resolve, reject) {
            _this.http.post(url, doacao, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    DoacaoProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_3__auth_auth__["a" /* AuthProvider */]])
    ], DoacaoProvider);
    return DoacaoProvider;
}());

//# sourceMappingURL=doacao.js.map

/***/ }),
/* 99 */,
/* 100 */,
/* 101 */,
/* 102 */,
/* 103 */,
/* 104 */,
/* 105 */,
/* 106 */,
/* 107 */,
/* 108 */,
/* 109 */,
/* 110 */,
/* 111 */,
/* 112 */,
/* 113 */,
/* 114 */,
/* 115 */,
/* 116 */,
/* 117 */,
/* 118 */,
/* 119 */,
/* 120 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComiteProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_config__ = __webpack_require__(18);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ComiteProvider = /** @class */ (function () {
    function ComiteProvider(http) {
        this.http = http;
    }
    ComiteProvider.prototype.getHeader = function (token) {
        return {
            "headers": {
                "Content-Type": 'application/json',
                "Authorization": "Bearer " + token
            }
        };
    };
    ComiteProvider.prototype.getPromise = function (url, header) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.get(url, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    ComiteProvider.prototype.findAll = function (token) {
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/Comites", header = this.getHeader(token);
        return this.getPromise(url, header);
    };
    ComiteProvider.prototype.findById = function (token, comiteId) {
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/Comites/" + comiteId, header = this.getHeader(token);
        return this.getPromise(url, header);
    };
    ComiteProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], ComiteProvider);
    return ComiteProvider;
}());

//# sourceMappingURL=comite.js.map

/***/ }),
/* 121 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_auth__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__home_home__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_facebook__ = __webpack_require__(184);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_notificacao_notificacao__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_person_person__ = __webpack_require__(22);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, navParams, auth, loadingCtrl, alertCtrl, fb, notification, person) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.auth = auth;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.fb = fb;
        this.notification = notification;
        this.person = person;
        this.user = {
            username: '',
            password: ''
        };
    }
    LoginPage.prototype.ionViewDidLoad = function () { };
    LoginPage.prototype.goPage = function (page) {
        this.navCtrl.push(page);
    };
    LoginPage.prototype.onLogin = function (user) {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: 'Carregando...'
        });
        var alert = this.alertCtrl.create({
            title: "OK",
            buttons: [
                {
                    text: 'Fechar',
                    role: 'cancel'
                }
            ]
        });
        loading.present();
        this.auth.postLogin()
            .then(function (response) {
            setTimeout(function () {
                _this.auth.saveSection(response);
                loading.dismiss();
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */]);
            }, 2000);
        }, function (error) {
            setTimeout(function () {
                loading.dismiss();
                alert.setTitle(error.error.error_description);
                alert.present();
            }, 2000);
        });
    };
    LoginPage.prototype.loginToFacebook = function () {
        this.fb.login(['public_profile', 'user_friends', 'email'])
            .then(function (res) { return console.log('Logged into Facebook!', res); })
            .catch(function (e) { return console.log('Error logging into Facebook', e); });
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-login',template:/*ion-inline-start:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/login/login.html"*/'<ion-content padding class="padding-fixed" color="light">\n    <header class="login-header" text-center>\n        <img src="assets/imgs/logo.png" class="brand">\n        <h3 class="title-white">Seja Bem Vindo!</h3>\n        <div class="block-text">\n          <p>Antes de seguir, você precisa se identificar</p>\n        </div>\n    </header>\n    <form autocomplete="off" #loginform="ngForm" (ngSubmit)="onLogin(user)">\n      <ion-list>\n        <ion-item class="input-block-color">\n          <ion-input type="email" required color="light" placeholder="Insira seu e-mail" [(ngModel)]="user.username" name="username"></ion-input>\n        </ion-item>\n      \n        <ion-item class="input-block-color">\n          <ion-input type="password" required placeholder="Insira sua Senha" [(ngModel)]="user.password"  name="password"></ion-input>\n        </ion-item>\n      </ion-list>\n      <button ion-button full color="light" text-uppercase [disabled]="!loginform.form.valid">Entrar</button>\n    </form>\n    \n    <div (click)="goPage(\'esqueci-senha\')" text-center class="link">Esqueci minha senha</div>\n\n    <button ion-button outline full text-uppercase color="light" icon-left (click)="loginToFacebook()"> \n      <img class="ico-general-button" src="assets/imgs/ico-facebook.png" width="11" height="19"> Logar pelo Facebook\n    </button>\n\n    <footer text-center>\n      <button ion-button clear text-uppercase color="secondary" icon-left (click)="goPage(\'cadastro\')"> Cadastre-se!</button>\n    </footer>\n</ion-content>\n'/*ion-inline-end:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/login/login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_facebook__["a" /* Facebook */],
            __WEBPACK_IMPORTED_MODULE_5__providers_notificacao_notificacao__["a" /* NotificationProvider */],
            __WEBPACK_IMPORTED_MODULE_6__providers_person_person__["a" /* PersonProvider */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),
/* 122 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AgendaProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_config__ = __webpack_require__(18);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AgendaProvider = /** @class */ (function () {
    function AgendaProvider(http) {
        this.http = http;
    }
    AgendaProvider.prototype.getLista = function (token, pag, skip) {
        var _this = this;
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/Agenda?$skip=" + skip + "&$top=" + pag;
        var header = { "headers": { "Content-Type": 'application/json', "Authorization": "Bearer " + token } };
        return new Promise(function (resolve, reject) {
            _this.http.get(url, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    AgendaProvider.prototype.filterCity = function (token, filter, pag, skip) {
        var _this = this;
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/Agenda?$skip=" + skip + "&$top=" + pag + "&$filter=Local eq '" + (filter.cidade + ', ' + filter.estado) + "'";
        var header = { "headers": { "Content-Type": 'application/json', "Authorization": "Bearer " + token } };
        return new Promise(function (resolve, reject) {
            _this.http.get(url, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    AgendaProvider.prototype.getDetalhe = function (token, id) {
        var _this = this;
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/Agenda/" + id;
        var header = { "headers": { "Content-Type": 'application/json', "Authorization": "Bearer " + token } };
        return new Promise(function (resolve, reject) {
            _this.http.get(url, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    AgendaProvider.prototype.filterData = function (token, filtro) {
        var _this = this;
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/Agenda/Filtro/" + filtro;
        var header = { "headers": { "Content-Type": 'application/json', "Authorization": "Bearer " + token } };
        return new Promise(function (resolve, reject) {
            _this.http.get(url, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    AgendaProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], AgendaProvider);
    return AgendaProvider;
}());

//# sourceMappingURL=agenda.js.map

/***/ }),
/* 123 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PesquisaProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_config__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__auth_auth__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var PesquisaProvider = /** @class */ (function () {
    function PesquisaProvider(http, auth) {
        this.http = http;
        this.auth = auth;
    }
    PesquisaProvider.prototype.findAll = function () {
        var _this = this;
        var token = this.auth.getToken();
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/Pesquisas";
        var header = { "headers": { "Content-Type": 'application/json', "Authorization": "Bearer " + token } };
        return new Promise(function (resolve, reject) {
            _this.http.get(url, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                var lista = [];
                for (var z = 0; z < result.length; z++) {
                    for (var j = 0; j < result[z].Pesquisas.length; j++) {
                        result[z].Pesquisas[j].Pontuacao = result[z].Pontuacao;
                        result[z].Pesquisas[j].Tema = result[z].Tema;
                        result[z].Pesquisas[j].ExibirGrafico = result[z].Status == 4;
                        result[z].Pesquisas[j].Finalizada = result[z].Status == 4 || result[z].Status == 3;
                        for (var i = 0; i < result[z].Pesquisas[j].PesquisasRespostas.length; i++) {
                            var value = result[z].Pesquisas[j].PesquisasRespostas[i];
                            if (value.Resposta == 'Sim' || value.Resposta == 'A Favor') {
                                result[z].Pesquisas[j].agree = value;
                            }
                            else if (value.Resposta == 'Não' || value.Resposta == 'Contra') {
                                result[z].Pesquisas[j].negative = value;
                            }
                        }
                        ;
                        lista.push({ Tema: result.Tema, Pesquisa: result[z].Pesquisas[j] });
                    }
                }
                resolve(lista);
            }, function (error) {
                reject(error);
            });
        });
    };
    PesquisaProvider.prototype.post = function (id) {
        var _this = this;
        var token = this.auth.getToken();
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/Pesquisas/Participar";
        var header = { "headers": { "Authorization": "Bearer " + token } };
        var body = {
            RespostaId: id
        };
        return new Promise(function (resolve, reject) {
            _this.http.post(url, body, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    PesquisaProvider.prototype.findById = function (id) {
        var _this = this;
        var token = this.auth.getToken();
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/Pesquisa/" + id;
        var header = { "headers": { "Content-Type": 'application/json', "Authorization": "Bearer " + token } };
        return new Promise(function (resolve, reject) {
            _this.http.get(url, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                var resultado = {
                    agreeText: '',
                    agreePercent: 0,
                    negativeText: '',
                    negativePercent: 0,
                    mensagem: '',
                    ExibirGrafico: false,
                    Pontuacao: 0
                };
                for (var i = 0; i < result.Grafico[0].DadosGrafico.length; i++) {
                    var pergunta = result.Grafico[0].DadosGrafico[i];
                    if (pergunta.Texto == 'Sim' || pergunta.Texto == 'A Favor') {
                        resultado.agreePercent = pergunta.Porcentagem;
                        resultado.agreeText = pergunta.Texto;
                    }
                    else if (pergunta.Texto == 'Não' || pergunta.Texto == 'Contra') {
                        resultado.negativePercent = pergunta.Porcentagem;
                        resultado.negativeText = pergunta.Texto;
                    }
                }
                resultado.mensagem = result.Mensagem;
                resultado.ExibirGrafico = result.ExibeResultados;
                resultado.Pontuacao = result.Pesquisa.Pontuacao;
                //resultado.ExibirGrafico = true;
                resolve(resultado);
            }, function (error) {
                reject(error);
            });
        });
    };
    PesquisaProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_3__auth_auth__["a" /* AuthProvider */]])
    ], PesquisaProvider);
    return PesquisaProvider;
}());

//# sourceMappingURL=pesquisa.js.map

/***/ }),
/* 124 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VoluntarioProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_config__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__auth_auth__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var VoluntarioProvider = /** @class */ (function () {
    function VoluntarioProvider(http, auth) {
        this.http = http;
        this.auth = auth;
    }
    VoluntarioProvider.prototype.findAll = function () {
        var _this = this;
        var token = this.auth.getToken();
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/Conteudos?TipoConteudoId=15";
        var header = { "headers": { "Content-Type": 'application/json', "Authorization": "Bearer " + token } };
        return new Promise(function (resolve, reject) {
            _this.http.get(url, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    VoluntarioProvider.prototype.findById = function (id) {
        var _this = this;
        var token = this.auth.getToken();
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/Conteudos/" + id;
        var header = { "headers": { "Content-Type": 'application/json', "Authorization": "Bearer " + token } };
        return new Promise(function (resolve, reject) {
            _this.http.get(url, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    VoluntarioProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_3__auth_auth__["a" /* AuthProvider */]])
    ], VoluntarioProvider);
    return VoluntarioProvider;
}());

//# sourceMappingURL=voluntario.js.map

/***/ }),
/* 125 */,
/* 126 */,
/* 127 */,
/* 128 */,
/* 129 */,
/* 130 */,
/* 131 */,
/* 132 */,
/* 133 */,
/* 134 */,
/* 135 */,
/* 136 */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 136;

/***/ }),
/* 137 */,
/* 138 */,
/* 139 */,
/* 140 */,
/* 141 */,
/* 142 */,
/* 143 */,
/* 144 */,
/* 145 */,
/* 146 */,
/* 147 */,
/* 148 */,
/* 149 */,
/* 150 */,
/* 151 */,
/* 152 */,
/* 153 */,
/* 154 */,
/* 155 */,
/* 156 */,
/* 157 */,
/* 158 */,
/* 159 */,
/* 160 */,
/* 161 */,
/* 162 */,
/* 163 */,
/* 164 */,
/* 165 */,
/* 166 */,
/* 167 */,
/* 168 */,
/* 169 */,
/* 170 */,
/* 171 */,
/* 172 */,
/* 173 */,
/* 174 */,
/* 175 */,
/* 176 */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/cadastro/cadastro-tipoperfil.module": [
		374,
		9
	],
	"../pages/cadastro/cadastro.module": [
		376,
		8
	],
	"../pages/comites/comites-comites-detalhe/comites-comites-detalhe.module": [
		375,
		7
	],
	"../pages/comites/comites.module": [
		185
	],
	"../pages/conheca-partido/conheca-partido.module": [
		190
	],
	"../pages/convite/convite.module": [
		187
	],
	"../pages/dados-complementares/dados-complementares.module": [
		377,
		6
	],
	"../pages/detail-eleicoes/detail-eleicoes.module": [
		188
	],
	"../pages/detail-programa/detail-programa.module": [
		192
	],
	"../pages/eleicoes-data-prazos/eleicoes-data-prazos.module": [
		194
	],
	"../pages/eleicoes-oque-posso-fazer/eleicoes-oque-posso-fazer.module": [
		196
	],
	"../pages/evento/evento.module": [
		378,
		0
	],
	"../pages/list-candidatos/list-candidatos.module": [
		198
	],
	"../pages/list-programa/list-programa.module": [
		200
	],
	"../pages/login/login-login-esqueci/login-login-esqueci.module": [
		379,
		5
	],
	"../pages/login/login.module": [
		202
	],
	"../pages/material-institucional/material-institucional.module": [
		203
	],
	"../pages/noticias-noticia-detalhe/noticias-noticia-detalhe.module": [
		208
	],
	"../pages/notificacoes/notificacoes-notificacoes-detalhe/notificacoes-notificacoes-detalhe.module": [
		380,
		4
	],
	"../pages/notificacoes/notificacoes.module": [
		210
	],
	"../pages/participacao/participacao-enviar-participacao/participacao-enviar-participacao.module": [
		211
	],
	"../pages/participacao/participacao-participacao-detalhe/participacao-participacao-detalhe.module": [
		381,
		3
	],
	"../pages/perfil/perfil.module": [
		212
	],
	"../pages/pesquisa/pesquisa-pesquisa-resposta/pesquisa-pesquisa-resposta.module": [
		382,
		2
	],
	"../pages/pesquisa/pesquisa.module": [
		215
	],
	"../pages/programa-governo/programa-governo.module": [
		216
	],
	"../pages/programa-pontos/programa-pontos.module": [
		219
	],
	"../pages/quero-contribuir/quero-contribuir.module": [
		222
	],
	"../pages/voluntario/voluntario-detalhe/voluntario-detalhe.module": [
		383,
		1
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 176;
module.exports = webpackAsyncContext;

/***/ }),
/* 177 */,
/* 178 */,
/* 179 */,
/* 180 */,
/* 181 */,
/* 182 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ModalSucessoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ModalSucessoPage = /** @class */ (function () {
    function ModalSucessoPage(navCtrl, navParams, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.data = navParams.data;
    }
    ModalSucessoPage.prototype.ionViewDidLoad = function () {
    };
    ModalSucessoPage.prototype.close = function () {
        this.viewCtrl.dismiss();
    };
    ModalSucessoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'modal-sucesso',template:/*ion-inline-start:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/modal-sucesso/modal-sucesso.html"*/'<div class="container-round">\n    <div class="box">\n        <button class="btnClose" (click)="close()">\n            <ion-icon name="close"></ion-icon>\n        </button>\n        <ion-icon name="checkmark" class="check"></ion-icon>\n        <div class="text">{{data.Titulo}}</div>\n        <div class="text">{{data.Mensagem}}</div>\n        <button class="btnOk" (click)="close()">{{data.Btn}}</button>\n    </div>\n</div>'/*ion-inline-end:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/modal-sucesso/modal-sucesso.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */]])
    ], ModalSucessoPage);
    return ModalSucessoPage;
}());

//# sourceMappingURL=modal-sucesso.js.map

/***/ }),
/* 183 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SharedService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_social_sharing__ = __webpack_require__(95);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__conteudo_conteudo__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__utils_utils__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var SharedService = /** @class */ (function () {
    function SharedService(SocialSharing, platform, conteudoprovider, utils) {
        this.SocialSharing = SocialSharing;
        this.platform = platform;
        this.conteudoprovider = conteudoprovider;
        this.utils = utils;
        console.log('Hello SharedServiceProvider Provider');
    }
    SharedService.prototype.SwitchShare = function (conteudo, rede) {
        switch (rede) {
            case 'facebook':
                this.shareViaFacebook(conteudo);
                break;
            case 'twitter':
                this.shareViaTwitter(conteudo);
                break;
            case 'instagram':
                this.shareViaInstagram(conteudo);
                break;
            case 'whatsapp':
                this.shareViaWhatsApp(conteudo);
                break;
            default:
                this.sharePicker(conteudo);
                break;
        }
    };
    SharedService.prototype.shareViaEmail = function (conteudo) {
        var _this = this;
        this.platform.ready()
            .then(function () {
            _this.SocialSharing.canShareViaEmail()
                .then(function () {
                _this.SocialSharing.shareViaEmail(conteudo.message, conteudo.subject, conteudo.sendTo)
                    .then(function (data) {
                    console.log('Shared via Email');
                })
                    .catch(function (err) {
                    console.log('Not able to be shared via Email');
                });
            })
                .catch(function (err) {
                console.log('Sharing via Email NOT enabled');
            });
        });
    };
    SharedService.prototype.shareViaFacebook = function (conteudo) {
        var _this = this;
        this.platform.ready()
            .then(function () {
            _this.SocialSharing.canShareVia('facebook', conteudo.Titulo, conteudo.Titulo, conteudo.Imagem)
                .then(function (data) {
                _this.SocialSharing.shareViaFacebook(conteudo.Titulo, conteudo.Imagem)
                    .then(function (data) {
                    console.log('Shared via Facebook');
                    _this.conteudoprovider.postShare(conteudo.Id, conteudo.PessoaId, 'facebook')
                        .then(function (response) {
                        if (response.GanhouPonto) {
                            _this.utils.showModalSucesso(response.Mensagem, "Continue interagindo", "OK");
                        }
                    });
                })
                    .catch(function (err) {
                    console.log('Was not shared via Facebook');
                });
            })
                .catch(function (err) {
                console.log('Not able to be shared via Facebook');
            });
        });
    };
    SharedService.prototype.shareViaInstagram = function (conteudo) {
        var _this = this;
        this.platform.ready()
            .then(function () {
            _this.SocialSharing.shareViaInstagram(conteudo.Titulo, conteudo.Imagem)
                .then(function (data) {
                _this.conteudoprovider.postShare(conteudo.Id, conteudo.PessoaId, 'instagram').then(function (response) {
                    if (response.GanhouPonto) {
                        _this.utils.showModalSucesso(response.Mensagem, "Continue interagindo", "OK");
                    }
                });
                console.log('Shared via shareViaInstagram');
            })
                .catch(function (err) {
                console.log('Was not shared via Instagram');
            });
        });
    };
    SharedService.prototype.sharePicker = function (conteudo) {
        var _this = this;
        this.platform.ready()
            .then(function () {
            _this.SocialSharing.share(conteudo.Titulo, conteudo.Titulo, conteudo.Imagem)
                .then(function (data) {
                console.log('Shared via SharePicker');
            })
                .catch(function (err) {
                console.log('Was not shared via SharePicker');
            });
        });
    };
    SharedService.prototype.shareViaTwitter = function (conteudo) {
        var _this = this;
        this.platform.ready()
            .then(function () {
            _this.SocialSharing.canShareVia('twitter', conteudo.Titulo, conteudo.Titulo, conteudo.Imagem)
                .then(function (data) {
                _this.SocialSharing.shareViaTwitter(conteudo.Titulo, conteudo.Imagem)
                    .then(function (data) {
                    _this.conteudoprovider.postShare(conteudo.Id, conteudo.PessoaId, 'twitter')
                        .then(function (response) {
                        if (response.GanhouPonto) {
                            _this.utils.showModalSucesso(response.Mensagem, "Continue interagindo", "OK");
                        }
                    });
                    console.log('Shared via Twitter');
                })
                    .catch(function (err) {
                    console.log('Was not shared via Twitter');
                });
            });
        })
            .catch(function (err) {
            console.log('Not able to be shared via Twitter');
        });
    };
    SharedService.prototype.shareViaWhatsApp = function (conteudo) {
        var _this = this;
        this.platform.ready()
            .then(function () {
            _this.SocialSharing.shareViaWhatsApp(conteudo.Titulo, conteudo.Imagem)
                .then(function (data) {
                _this.conteudoprovider.postShare(conteudo.Id, conteudo.PessoaId, 'whatsapp')
                    .then(function (response) {
                    if (response.GanhouPonto) {
                        _this.utils.showModalSucesso(response.Mensagem, "Continue interagindo", "OK");
                    }
                });
                console.log('Shared via Twitter');
            })
                .catch(function (err) {
                console.log('Was not shared via Twitter');
            });
        });
    };
    SharedService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_native_social_sharing__["a" /* SocialSharing */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["m" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_3__conteudo_conteudo__["a" /* ConteudoProvider */],
            __WEBPACK_IMPORTED_MODULE_4__utils_utils__["a" /* UtilsProvider */]])
    ], SharedService);
    return SharedService;
}());

//# sourceMappingURL=shared-service.js.map

/***/ }),
/* 184 */,
/* 185 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ComitesPageModule", function() { return ComitesPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__comites__ = __webpack_require__(186);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ComitesPageModule = /** @class */ (function () {
    function ComitesPageModule() {
    }
    ComitesPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__comites__["a" /* ComitesPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__comites__["a" /* ComitesPage */])
            ]
        })
    ], ComitesPageModule);
    return ComitesPageModule;
}());

//# sourceMappingURL=comites.module.js.map

/***/ }),
/* 186 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComitesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_searchbar_searchbar__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_comite_comite__ = __webpack_require__(120);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_auth_auth__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_utils_utils__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ComitesPage = /** @class */ (function () {
    function ComitesPage(navCtrl, 
        //private navParams: NavParams,
        modalCtrl, comiteProvider, utils, auth) {
        this.navCtrl = navCtrl;
        this.modalCtrl = modalCtrl;
        this.comiteProvider = comiteProvider;
        this.utils = utils;
        this.auth = auth;
        this.itemsPerPage = 4;
        this.currentPage = 1;
        this.totalPages = 1;
        this.items = [];
        this._itemsResult = [];
        this.itemsEnded = false;
        this.estados = [];
        this.cidades = [];
    }
    ComitesPage.prototype.ionViewDidLoad = function () {
        this.getData();
    };
    ComitesPage.prototype.searchToggle = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__components_searchbar_searchbar__["a" /* SearchbarComponent */]);
        modal.present().then().catch();
    };
    ComitesPage.prototype.getData = function () {
        var _this = this;
        this.comiteProvider.findAll(this.auth.getToken()).then(function (result) {
            // Filter
            if (_this.modelCidade && _this.modelEstado) {
                _this._itemsResult = _this.filterResult(result);
            }
            else {
                _this._itemsResult = result;
            }
            _this.totalPages = Math.ceil(_this._itemsResult.length / _this.itemsPerPage);
            _this.items = _this._itemsResult.slice(0, _this.itemsPerPage);
        }, function (err) {
            console.log('Erro:', err);
        });
    };
    ComitesPage.prototype.doRefresh = function (refresher) {
        var _this = this;
        console.log('Begin async operation', refresher);
        setTimeout(function () {
            console.log('Async operation has ended');
            _this.getData();
            refresher.complete();
        }, 1000);
    };
    ComitesPage.prototype.doInfinite = function (infiniteScroll) {
        if (!this.itemsEnded && this.currentPage < this.totalPages) {
            var sliceItems = this._itemsResult.slice(this.currentPage * this.itemsPerPage, ++this.currentPage * this.itemsPerPage);
            this.items = this.items.concat(sliceItems);
            this.currentPage++;
        }
        else {
            this.itemsEnded = true;
        }
        setTimeout(function () {
            infiniteScroll.complete();
        }, 500);
    };
    ComitesPage.prototype.openDetail = function (page, id) {
        this.navCtrl.push(page, { id: id });
    };
    ComitesPage.prototype.getNameFromUf = function (uf) {
        return this.utils.getNameFromUf(uf);
    };
    ComitesPage.prototype.changeEstado = function () {
        if (this.modelEstado) {
            var estado = this.utils.getFromUf(this.modelEstado);
            this.cidades = estado.cidades;
        }
    };
    ComitesPage.prototype.changeCidade = function () {
        this.getData();
    };
    ComitesPage.prototype.filterResult = function (result) {
        var _this = this;
        if (result.length) {
            var filtered = result.filter(function (elem) {
                return elem.Cidade == _this.modelCidade && elem.Estado == _this.modelEstado;
            });
            return filtered;
        }
        return result;
    };
    ComitesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-comites',template:/*ion-inline-start:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/comites/comites.html"*/'<ion-header color="header">\n    <ion-navbar>\n      <button ion-button menuToggle>\n        <i class="fa fa-bars" aria-hidden="true"></i>\n      </button>\n      <ion-title text-center>Diretórios</ion-title>\n      <ion-buttons end>\n        <button (click)="searchToggle()" class="search-header">\n        <i class="fa fa-search" aria-hidden="true"></i>\n      </button>\n      </ion-buttons>\n    </ion-navbar>\n  </ion-header>\n\n<ion-content padding>\n    <ion-refresher (ionRefresh)="doRefresh($event)">\n        <ion-refresher-content></ion-refresher-content>\n      </ion-refresher>\n\n  <ion-row>\n    <ion-col col-5>\n      <ion-item>\n        <ion-select placeholder="Filtrar por estado" [(ngModel)]="modelEstado" (ionChange)="changeEstado()">\n          <ion-option value="AP">Amapá</ion-option>\n          <ion-option value="RJ">Rio de Janeiro</ion-option>\n          <ion-option value="ES">Espírito Santo</ion-option>\n        </ion-select>\n      </ion-item>\n    </ion-col>\n    <ion-col col-7>\n      <ion-item>\n        <!-- <ion-label stacked>Cidade</ion-label> -->\n        <ion-select placeholder="Filtrar por cidade" [(ngModel)]="modelCidade" (ionChange)="changeCidade()">\n          <ion-option *ngFor="let cidade of cidades" [value]="cidade" [innerHtml]="cidade"></ion-option>\n        </ion-select>\n      </ion-item>\n    </ion-col>\n  </ion-row>\n  <ion-list margin-top *ngIf="items.length">\n    <ion-card *ngFor="let item of items" (click)="openDetail(\'comites-detalhe\', item.Id)">\n      <ion-card-content class="flex-card">\n        <div>\n          <ion-card-title [innerHtml]="item.Cidade"></ion-card-title>\n          <p [innerHtml]="getNameFromUf(item.Estado)"></p>\n          <ion-note>\n            <ion-icon name="pin"></ion-icon>\n            {{ item.Endereco }}\n          </ion-note>\n        </div>\n        <ion-icon class="set" name="ios-arrow-forward"></ion-icon>\n      </ion-card-content>\n    </ion-card>\n  </ion-list>\n\n  <!-- <ion-list margin-top *ngIf="items.length == 0">\n    <ion-card>\n      <ion-card-content class="flex-card">\n        <div>\n          <p>Nenhum comitê encontrado.</p>\n        </div>\n      </ion-card-content>\n    </ion-card>\n  </ion-list> -->\n  <div class="NoContentmsg" *ngIf="items.length == 0">\n      <!-- <img src="" alt=""> -->\n      <i class="fa fa-frown-o" aria-hidden="true"></i>\n      <p>Nenhuma pesquisa encontrada</p>\n    </div>\n  <ion-infinite-scroll (ionInfinite)="doInfinite($event)" *ngIf="!itemsEnded">\n    <ion-infinite-scroll-content></ion-infinite-scroll-content>\n  </ion-infinite-scroll>\n</ion-content>\n'/*ion-inline-end:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/comites/comites.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_comite_comite__["a" /* ComiteProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_utils_utils__["a" /* UtilsProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_auth_auth__["a" /* AuthProvider */]])
    ], ComitesPage);
    return ComitesPage;
}());

//# sourceMappingURL=comites.js.map

/***/ }),
/* 187 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConvitePageModule", function() { return ConvitePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__convite__ = __webpack_require__(332);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ConvitePageModule = /** @class */ (function () {
    function ConvitePageModule() {
    }
    ConvitePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__convite__["a" /* ConvitePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__convite__["a" /* ConvitePage */]),
            ],
        })
    ], ConvitePageModule);
    return ConvitePageModule;
}());

//# sourceMappingURL=convite.module.js.map

/***/ }),
/* 188 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetailEleicoesPageModule", function() { return DetailEleicoesPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__detail_eleicoes__ = __webpack_require__(189);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var DetailEleicoesPageModule = /** @class */ (function () {
    function DetailEleicoesPageModule() {
    }
    DetailEleicoesPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__detail_eleicoes__["a" /* DetailEleicoesPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__detail_eleicoes__["a" /* DetailEleicoesPage */]),
            ],
        })
    ], DetailEleicoesPageModule);
    return DetailEleicoesPageModule;
}());

//# sourceMappingURL=detail-eleicoes.module.js.map

/***/ }),
/* 189 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetailEleicoesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_searchbar_searchbar__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DetailEleicoesPage = /** @class */ (function () {
    function DetailEleicoesPage(navCtrl, navParams, modalCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.interna = this.navParams.get('interna');
        console.log(this.interna);
    }
    DetailEleicoesPage.prototype.searchToggle = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__components_searchbar_searchbar__["a" /* SearchbarComponent */]);
        modal.present();
    };
    DetailEleicoesPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DetailProgramaPage');
    };
    DetailEleicoesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-detail-eleicoes',template:/*ion-inline-start:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/detail-eleicoes/detail-eleicoes.html"*/'<ion-header color="header">\n  <ion-navbar>\n    <ion-title text-center>Eleiçoes 2018</ion-title>\n    <ion-buttons end>\n      <button (click)="searchToggle()" class="search-header">\n      <i class="fa fa-search" aria-hidden="true"></i>\n    </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <div class="headerClass">\n    <h2>\n      {{interna.Titulo}}\n    </h2>\n    <div class="date">\n      <p>{{interna.diaFormatado}}<br>{{interna.mesFormatado}}</p>\n    </div>\n  </div>\n  <div [innerHTML]="interna.Texto"></div>\n</ion-content>'/*ion-inline-end:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/detail-eleicoes/detail-eleicoes.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ModalController */]])
    ], DetailEleicoesPage);
    return DetailEleicoesPage;
}());

//# sourceMappingURL=detail-eleicoes.js.map

/***/ }),
/* 190 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConhecaPartidoPageModule", function() { return ConhecaPartidoPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__conheca_partido__ = __webpack_require__(191);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ConhecaPartidoPageModule = /** @class */ (function () {
    function ConhecaPartidoPageModule() {
    }
    ConhecaPartidoPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__conheca_partido__["a" /* ConhecaPartidoPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__conheca_partido__["a" /* ConhecaPartidoPage */]),
            ],
        })
    ], ConhecaPartidoPageModule);
    return ConhecaPartidoPageModule;
}());

//# sourceMappingURL=conheca-partido.module.js.map

/***/ }),
/* 191 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConhecaPartidoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_searchbar_searchbar__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_auth__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_utils_utils__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_conteudo_conteudo__ = __webpack_require__(23);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ConhecaPartidoPage = /** @class */ (function () {
    function ConhecaPartidoPage(navCtrl, navParams, auth, conteudo, utils, modalCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.auth = auth;
        this.conteudo = conteudo;
        this.utils = utils;
        this.modalCtrl = modalCtrl;
        this.tipoConteudo = 10015;
        this.start = 100;
        this.skip = 0;
        this.NadaEncontrado = false;
        this.searching = true;
        this.anosList = [];
        this.loadAnos();
    }
    ConhecaPartidoPage.prototype.loadAnos = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.conteudo.getConteudo(_this.auth.getToken(), _this.start, _this.skip, _this.tipoConteudo)
                .then(function (data) {
                _this.anosList = _this.anosList.concat(data);
                console.log(data);
                if (_this.anosList.length == 0) {
                    _this.NadaEncontrado = true;
                }
            });
        });
    };
    ConhecaPartidoPage.prototype.searchToggle = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__components_searchbar_searchbar__["a" /* SearchbarComponent */]);
        modal.present();
    };
    ConhecaPartidoPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ConhecaPartidoPage');
    };
    ConhecaPartidoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-conheca-partido',template:/*ion-inline-start:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/conheca-partido/conheca-partido.html"*/'<ion-header color="header">\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title text-center>Conheça o MDB</ion-title>\n    <ion-buttons end>\n    <button (click)="searchToggle()" class="search-header">\n      <ion-icon name="search"></ion-icon>\n    </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n  <ion-list>\n    <ion-item *ngFor="let l of anosList" >\n      <div class="headerR">\n      <ion-avatar item-start>\n        <img src="{{l.Imagem}}" *ngIf="l.Imagem!=undefined">\n        <img src="assets/imgs/logomdb.png" *ngIf="l.Imagem==undefined">\n      </ion-avatar>\n      <h2>{{l.Titulo}}</h2>\n      </div>\n      <!-- <p>I\'ve had a pretty messed up day. If we just...</p> -->\n      <div class="infosOthers">\n        <p [innerHTML]="l.Texto"></p>\n      </div>\n    </ion-item>\n  </ion-list>\n</ion-content>\n'/*ion-inline-end:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/conheca-partido/conheca-partido.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_conteudo_conteudo__["a" /* ConteudoProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_utils_utils__["a" /* UtilsProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ModalController */]])
    ], ConhecaPartidoPage);
    return ConhecaPartidoPage;
}());

//# sourceMappingURL=conheca-partido.js.map

/***/ }),
/* 192 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetailProgramaPageModule", function() { return DetailProgramaPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__detail_programa__ = __webpack_require__(193);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var DetailProgramaPageModule = /** @class */ (function () {
    function DetailProgramaPageModule() {
    }
    DetailProgramaPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__detail_programa__["a" /* DetailProgramaPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__detail_programa__["a" /* DetailProgramaPage */]),
            ],
        })
    ], DetailProgramaPageModule);
    return DetailProgramaPageModule;
}());

//# sourceMappingURL=detail-programa.module.js.map

/***/ }),
/* 193 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetailProgramaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_searchbar_searchbar__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DetailProgramaPage = /** @class */ (function () {
    function DetailProgramaPage(navCtrl, navParams, modalCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.interna = this.navParams.get('interna');
        console.log(this.interna);
    }
    DetailProgramaPage.prototype.searchToggle = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__components_searchbar_searchbar__["a" /* SearchbarComponent */]);
        modal.present();
    };
    DetailProgramaPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DetailProgramaPage');
    };
    DetailProgramaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-detail-programa',template:/*ion-inline-start:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/detail-programa/detail-programa.html"*/'<ion-header color="header">\n  <ion-navbar>\n    <ion-title text-center>{{interna.Titulo}}</ion-title>\n    <ion-buttons end>\n      <button (click)="searchToggle()" class="search-header">\n      <i class="fa fa-search" aria-hidden="true"></i>\n    </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n  <div [innerHTML]="interna.Texto"></div>\n</ion-content>\n'/*ion-inline-end:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/detail-programa/detail-programa.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ModalController */]])
    ], DetailProgramaPage);
    return DetailProgramaPage;
}());

//# sourceMappingURL=detail-programa.js.map

/***/ }),
/* 194 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EleicoesDataPrazosPageModule", function() { return EleicoesDataPrazosPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__eleicoes_data_prazos__ = __webpack_require__(195);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var EleicoesDataPrazosPageModule = /** @class */ (function () {
    function EleicoesDataPrazosPageModule() {
    }
    EleicoesDataPrazosPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__eleicoes_data_prazos__["a" /* EleicoesDataPrazosPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__eleicoes_data_prazos__["a" /* EleicoesDataPrazosPage */]),
            ],
        })
    ], EleicoesDataPrazosPageModule);
    return EleicoesDataPrazosPageModule;
}());

//# sourceMappingURL=eleicoes-data-prazos.module.js.map

/***/ }),
/* 195 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EleicoesDataPrazosPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_auth__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_utils_utils__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_conteudo_conteudo__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__detail_eleicoes_detail_eleicoes__ = __webpack_require__(189);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__components_searchbar_searchbar__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var EleicoesDataPrazosPage = /** @class */ (function () {
    function EleicoesDataPrazosPage(navCtrl, navParams, auth, conteudo, utils, modalCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.auth = auth;
        this.conteudo = conteudo;
        this.utils = utils;
        this.modalCtrl = modalCtrl;
        // Tipo de Conteudo Noticias
        this.tipoConteudo = 15;
        this.start = 100;
        this.skip = 0;
        this.anosList = [];
        this.meses = ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'];
        this.DiaMes = [];
        this.jsonCompleto = [];
        this.searching = true;
        this.NadaEncontrado = false;
        this.loadAnos();
    }
    EleicoesDataPrazosPage.prototype.loadAnos = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.conteudo.getConteudo(_this.auth.getToken(), _this.start, _this.skip, _this.tipoConteudo)
                .then(function (data) {
                _this.anosList = _this.anosList.concat(data);
                console.log(data);
                resolve(true);
                _this.jsonCompleto = data;
                if (_this.anosList.length == 0) {
                    _this.NadaEncontrado = true;
                }
                _this.pegaDiaMes();
            });
        });
    };
    EleicoesDataPrazosPage.prototype.searchToggle = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_6__components_searchbar_searchbar__["a" /* SearchbarComponent */]);
        modal.present();
    };
    EleicoesDataPrazosPage.prototype.openDetails = function (l) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__detail_eleicoes_detail_eleicoes__["a" /* DetailEleicoesPage */], {
            interna: l
        });
    };
    EleicoesDataPrazosPage.prototype.pegaDiaMes = function () {
        for (var j = 0; j < this.jsonCompleto.length; j++) {
            var dataJson = this.jsonCompleto[j].Data.split('/');
            var dia = dataJson[0];
            var mes = dataJson[1];
            this.anosList[j].diaFormatado = dia;
            this.anosList[j].mesFormatado = this.meses[mes - 1].valueOf();
        }
    };
    EleicoesDataPrazosPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ListProgramaPage');
    };
    EleicoesDataPrazosPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-eleicoes-data-prazos',template:/*ion-inline-start:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/eleicoes-data-prazos/eleicoes-data-prazos.html"*/'<ion-header color="header">\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <i class="fa fa-bars" aria-hidden="true"></i>\n    </button>\n    <ion-title text-center>Eleiçoes 2018</ion-title>\n    <ion-buttons end>\n      <button (click)="searchToggle()" class="search-header">\n      <i class="fa fa-search" aria-hidden="true"></i>\n    </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <h2>Prazos e Datas Importantes</h2>\n  <ion-list>\n    <ion-item *ngFor="let l of anosList" (click)="openDetails(l)">\n      <ion-avatar item-start>\n        <p>{{l.diaFormatado}}<br>{{l.mesFormatado}}</p>\n        <!-- <p></p> -->\n      </ion-avatar>\n      <h2>{{l.Titulo}}</h2>\n      <!-- <p>I\'ve had a pretty messed up day. If we just...</p> -->\n      <ion-icon name="ios-arrow-forward"></ion-icon>\n    </ion-item>\n  </ion-list>\n</ion-content>\n'/*ion-inline-end:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/eleicoes-data-prazos/eleicoes-data-prazos.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_conteudo_conteudo__["a" /* ConteudoProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_utils_utils__["a" /* UtilsProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ModalController */]])
    ], EleicoesDataPrazosPage);
    return EleicoesDataPrazosPage;
}());

//# sourceMappingURL=eleicoes-data-prazos.js.map

/***/ }),
/* 196 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EleicoesOquePossoFazerPageModule", function() { return EleicoesOquePossoFazerPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__eleicoes_oque_posso_fazer__ = __webpack_require__(197);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var EleicoesOquePossoFazerPageModule = /** @class */ (function () {
    function EleicoesOquePossoFazerPageModule() {
    }
    EleicoesOquePossoFazerPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__eleicoes_oque_posso_fazer__["a" /* EleicoesOquePossoFazerPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__eleicoes_oque_posso_fazer__["a" /* EleicoesOquePossoFazerPage */]),
            ],
        })
    ], EleicoesOquePossoFazerPageModule);
    return EleicoesOquePossoFazerPageModule;
}());

//# sourceMappingURL=eleicoes-oque-posso-fazer.module.js.map

/***/ }),
/* 197 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EleicoesOquePossoFazerPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_auth__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_utils_utils__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_conteudo_conteudo__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_searchbar_searchbar__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var EleicoesOquePossoFazerPage = /** @class */ (function () {
    function EleicoesOquePossoFazerPage(navCtrl, navParams, auth, conteudo, utils, modalCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.auth = auth;
        this.conteudo = conteudo;
        this.utils = utils;
        this.modalCtrl = modalCtrl;
        // Tipo de Conteudo Noticias
        this.tipoConteudo = 19;
        this.eleicoesdescri = {};
    }
    ;
    EleicoesOquePossoFazerPage.prototype.loadDescription = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.conteudo.getPage(_this.auth.getToken(), _this.tipoConteudo)
                .then(function (data) {
                resolve(true);
                _this.eleicoesdescri = data;
                console.log(_this.eleicoesdescri, 'ELEICAO');
            });
        });
    };
    EleicoesOquePossoFazerPage.prototype.searchToggle = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_5__components_searchbar_searchbar__["a" /* SearchbarComponent */]);
        modal.present();
    };
    EleicoesOquePossoFazerPage.prototype.ionViewDidLoad = function () {
        this.loadDescription();
        console.log('ionViewDidLoad ListProgramaPage');
    };
    EleicoesOquePossoFazerPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-eleicoes-oque-posso-fazer',template:/*ion-inline-start:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/eleicoes-oque-posso-fazer/eleicoes-oque-posso-fazer.html"*/'<ion-header color="header">\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <i class="fa fa-bars" aria-hidden="true"></i>\n    </button>\n    <ion-title text-center>Eleiçoes 2018</ion-title>\n    <ion-buttons end>\n      <button (click)="searchToggle()" class="search-header">\n      <i class="fa fa-search" aria-hidden="true"></i>\n    </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <div>\n    <h2>{{eleicoesdescri.Titulo}}</h2>\n    <p [innerHTML]="eleicoesdescri.Descricao"></p>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/eleicoes-oque-posso-fazer/eleicoes-oque-posso-fazer.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_conteudo_conteudo__["a" /* ConteudoProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_utils_utils__["a" /* UtilsProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ModalController */]])
    ], EleicoesOquePossoFazerPage);
    return EleicoesOquePossoFazerPage;
}());

//# sourceMappingURL=eleicoes-oque-posso-fazer.js.map

/***/ }),
/* 198 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListCandidatosPageModule", function() { return ListCandidatosPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__list_candidatos__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pipes_search_search__ = __webpack_require__(333);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var ListCandidatosPageModule = /** @class */ (function () {
    function ListCandidatosPageModule() {
    }
    ListCandidatosPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__list_candidatos__["a" /* ListCandidatosPage */],
                __WEBPACK_IMPORTED_MODULE_3__pipes_search_search__["a" /* SearchPipe */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__list_candidatos__["a" /* ListCandidatosPage */]),
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_3__pipes_search_search__["a" /* SearchPipe */]
            ]
        })
    ], ListCandidatosPageModule);
    return ListCandidatosPageModule;
}());

//# sourceMappingURL=list-candidatos.module.js.map

/***/ }),
/* 199 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListCandidatosPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_doacao_doacao__ = __webpack_require__(98);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_searchbar_searchbar__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__quero_contribuir_quero_contribuir__ = __webpack_require__(97);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ListCandidatosPage = /** @class */ (function () {
    // searchText:string = ''
    function ListCandidatosPage(navCtrl, navParams, modalCtrl, doacao) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.doacao = doacao;
        this.descending = false;
        this.column = 'Nome';
        this.getList();
    }
    ListCandidatosPage.prototype.searchToggle = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__components_searchbar_searchbar__["a" /* SearchbarComponent */]);
        modal.present();
    };
    ListCandidatosPage.prototype.sort = function () {
        this.descending = !this.descending;
        this.order = this.descending ? 1 : -1;
    };
    ListCandidatosPage.prototype.getList = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.doacao.getList().then(function (result) {
                _this.list = result;
                console.log(_this.list);
                resolve(true);
            });
        });
    };
    ListCandidatosPage.prototype.abrirForm = function (l) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__quero_contribuir_quero_contribuir__["a" /* QueroContribuirPage */], {
            candidato: l
        });
    };
    ListCandidatosPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ListCandidatosPage');
    };
    ListCandidatosPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-list-candidatos',template:/*ion-inline-start:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/list-candidatos/list-candidatos.html"*/'<ion-header color="header">\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <i class="fa fa-bars" aria-hidden="true"></i>\n    </button>\n    <ion-title text-center>Quero Contribuir</ion-title>\n    <ion-buttons end>\n      <button (click)="searchToggle()" class="search-header">\n      <i class="fa fa-search" aria-hidden="true"></i>\n    </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n\n\n<ion-content padding>\n  <p class="description">\n    Procure um <strong>candidato ou partido</strong> para fazer uma doação\n  </p>\n  <input type="text" placeholder="Digite o nome do candidato ou partido"  [(ngModel)]="searchText" >\n  <ion-list>\n    <ion-item *ngFor="let l of list | search: searchText" (click)="abrirForm(l)">\n      <ion-avatar item-start>\n        <img src="{{l.Imagem}}" *ngIf="l.Imagem!=undefined">\n        <img src="assets/imgs/logomdb.png" *ngIf="l.Imagem==undefined">\n      </ion-avatar>\n      <h2>{{l.Nome}}</h2>\n      <!-- <p>I\'ve had a pretty messed up day. If we just...</p> -->\n    </ion-item>\n  </ion-list>\n</ion-content>\n'/*ion-inline-end:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/list-candidatos/list-candidatos.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_doacao_doacao__["a" /* DoacaoProvider */]])
    ], ListCandidatosPage);
    return ListCandidatosPage;
}());

//# sourceMappingURL=list-candidatos.js.map

/***/ }),
/* 200 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListProgramaPageModule", function() { return ListProgramaPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__list_programa__ = __webpack_require__(201);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ListProgramaPageModule = /** @class */ (function () {
    function ListProgramaPageModule() {
    }
    ListProgramaPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__list_programa__["a" /* ListProgramaPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__list_programa__["a" /* ListProgramaPage */]),
            ],
        })
    ], ListProgramaPageModule);
    return ListProgramaPageModule;
}());

//# sourceMappingURL=list-programa.module.js.map

/***/ }),
/* 201 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListProgramaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_auth__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_utils_utils__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_conteudo_conteudo__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__detail_programa_detail_programa__ = __webpack_require__(193);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__components_searchbar_searchbar__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ListProgramaPage = /** @class */ (function () {
    function ListProgramaPage(navCtrl, navParams, auth, conteudo, modalCtrl, utils) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.auth = auth;
        this.conteudo = conteudo;
        this.modalCtrl = modalCtrl;
        this.utils = utils;
        // Tipo de Conteudo Noticias
        this.tipoConteudo = 14;
        this.start = 3;
        this.skip = 0;
        this.anosList = [];
        this.loadAnos();
    }
    ListProgramaPage.prototype.loadAnos = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.conteudo.getConteudo(_this.auth.getToken(), _this.start, _this.skip, _this.tipoConteudo)
                .then(function (data) {
                _this.anosList = _this.anosList.concat(data);
                console.log(data);
                resolve(true);
            });
        });
    };
    ListProgramaPage.prototype.searchToggle = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_6__components_searchbar_searchbar__["a" /* SearchbarComponent */]);
        modal.present();
    };
    ListProgramaPage.prototype.openDetails = function (l) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__detail_programa_detail_programa__["a" /* DetailProgramaPage */], {
            interna: l
        });
    };
    ListProgramaPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ListProgramaPage');
    };
    ListProgramaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-list-programa',template:/*ion-inline-start:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/list-programa/list-programa.html"*/'<ion-header color="header">\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <i class="fa fa-bars" aria-hidden="true"></i>\n    </button>\n    <ion-title text-center>2 anos de MDB no Governo</ion-title>\n    <ion-buttons end>\n      <button (click)="searchToggle()" class="search-header">\n      <i class="fa fa-search" aria-hidden="true"></i>\n    </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <h2>O que mudou na sua vida</h2>\n  <ion-list>\n    <ion-item *ngFor="let l of anosList" (click)="openDetails(l)">\n      <ion-avatar item-start>\n        <img src="{{l.Imagem}}" *ngIf="l.Imagem!=undefined">\n        <img src="assets/imgs/logomdb.png" *ngIf="l.Imagem==undefined">\n      </ion-avatar>\n      <h2>{{l.Titulo}}</h2>\n      <!-- <p>I\'ve had a pretty messed up day. If we just...</p> -->\n      <ion-icon name="ios-arrow-forward"></ion-icon>\n    </ion-item>\n  </ion-list>\n</ion-content>\n'/*ion-inline-end:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/list-programa/list-programa.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_conteudo_conteudo__["a" /* ConteudoProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_utils_utils__["a" /* UtilsProvider */]])
    ], ListProgramaPage);
    return ListProgramaPage;
}());

//# sourceMappingURL=list-programa.js.map

/***/ }),
/* 202 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login__ = __webpack_require__(121);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var LoginPageModule = /** @class */ (function () {
    function LoginPageModule() {
    }
    LoginPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__login__["a" /* LoginPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__login__["a" /* LoginPage */])
            ],
        })
    ], LoginPageModule);
    return LoginPageModule;
}());

//# sourceMappingURL=login.module.js.map

/***/ }),
/* 203 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MaterialInstitucionalPageModule", function() { return MaterialInstitucionalPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__material_institucional__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pipes_search_categorias_search_categorias__ = __webpack_require__(334);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var MaterialInstitucionalPageModule = /** @class */ (function () {
    function MaterialInstitucionalPageModule() {
    }
    MaterialInstitucionalPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__material_institucional__["a" /* MaterialInstitucionalPage */],
                __WEBPACK_IMPORTED_MODULE_3__pipes_search_categorias_search_categorias__["a" /* SearchCategoriasPipe */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__material_institucional__["a" /* MaterialInstitucionalPage */]),
            ],
        })
    ], MaterialInstitucionalPageModule);
    return MaterialInstitucionalPageModule;
}());

//# sourceMappingURL=material-institucional.module.js.map

/***/ }),
/* 204 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MaterialInstitucionalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_institucional_institucional__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular_components_loading_loading_controller__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_utils_utils__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__social_share_social_share__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_download_download__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__components_searchbar_searchbar__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var MaterialInstitucionalPage = /** @class */ (function () {
    function MaterialInstitucionalPage(navCtrl, navParams, loadingCtrl, material, util, _download, modalCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.loadingCtrl = loadingCtrl;
        this.material = material;
        this.util = util;
        this._download = _download;
        this.modalCtrl = modalCtrl;
        this.searchText = '';
        this.active1 = true;
        this.active2 = false;
        this.active3 = false;
        this.active4 = false;
        this.getData();
    }
    MaterialInstitucionalPage.prototype.searchToggle = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_7__components_searchbar_searchbar__["a" /* SearchbarComponent */]);
        modal.present();
    };
    MaterialInstitucionalPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MaterialInstitucionalPage');
    };
    MaterialInstitucionalPage.prototype.doInfinite = function (infiniteScroll) {
        console.log('Begin async operation');
        setTimeout(function () {
            infiniteScroll.complete();
        }, 500);
    };
    MaterialInstitucionalPage.prototype.click1 = function (param) {
        this.searchText = param;
        if (param == 'Cartazes') {
            this.active1 = false;
            this.active2 = true;
            this.active3 = false;
            this.active4 = false;
        }
        else if (param == 'Posts') {
            this.active1 = false;
            this.active2 = false;
            this.active3 = true;
            this.active4 = false;
        }
        else if (param == 'Capas') {
            this.active1 = false;
            this.active2 = false;
            this.active3 = false;
            this.active4 = true;
        }
        else {
            this.active1 = true;
            this.active2 = false;
            this.active3 = false;
            this.active4 = false;
        }
    };
    MaterialInstitucionalPage.prototype.clickTodos = function () {
        this.active1 = true;
        this.active2 = false;
        this.active3 = false;
        this.active4 = false;
        this.getData();
    };
    MaterialInstitucionalPage.prototype.doRefresh = function (refresher) {
        var _this = this;
        console.log('Begin async operation', refresher);
        setTimeout(function () {
            console.log('Async operation has ended');
            _this.getData();
            refresher.complete();
        }, 1000);
    };
    MaterialInstitucionalPage.prototype.getData = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: 'Carregando...'
        });
        loading.present();
        this.material.findAll().then(function (result) {
            loading.dismiss();
            _this.listMaterial = result;
            console.log(result);
        }, function (error) {
            loading.dismiss();
            console.log(error);
        });
    };
    MaterialInstitucionalPage.prototype.getFile = function (url) {
        // this.util.downloadFile(url);
        this._download.download(url);
    };
    MaterialInstitucionalPage.prototype.postShare = function (conteudo) {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_5__social_share_social_share__["a" /* SocialSharePage */], conteudo);
        modal.present();
    };
    MaterialInstitucionalPage.prototype.clickCategoria = function (n) {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: 'Carregando...'
        });
        loading.present();
        this.material.findAll().then(function (result) {
            loading.dismiss();
            _this.listMaterial = result;
            console.log(result);
        }, function (error) {
            loading.dismiss();
            console.log(error);
        });
    };
    MaterialInstitucionalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-material-institucional',template:/*ion-inline-start:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/material-institucional/material-institucional.html"*/'<ion-header color="header">\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <i class="fa fa-bars" aria-hidden="true"></i>\n    </button>\n    <ion-title text-center>Material Institucional</ion-title>\n    <ion-buttons end>\n    <button (click)="searchToggle()" class="search-header">\n      <ion-icon name="search"></ion-icon>\n    </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n    <ion-refresher (ionRefresh)="doRefresh($event)">\n        <ion-refresher-content></ion-refresher-content>\n      </ion-refresher>\n  <section>\n    <div class="listButtons">\n      <button class="item" [ngClass]="{\'active\':active1}"(click)="clickTodos()">Todos</button>\n      <button class="item" [ngClass]="{\'active\':active2}" (click)="click1(\'Cartazes\')">Cartazes</button>\n      <button class="item" [ngClass]="{\'active\':active3}"(click)="click1(\'Posts\')">Posts</button>\n      <button class="item" [ngClass]="{\'active\':active4}"(click)="click1(\'Capas\')">Capas</button>\n    </div>\n    <div>\n      <ion-card *ngFor="let material of listMaterial | searchCategorias: searchText">\n        <img src="{{material.Imagem}}"/>\n        <ion-card-content>\n          <ion-card-title>{{material.Titulo}}</ion-card-title>\n          <p [innerHtml]= "material.SubTitulo"></p>\n        </ion-card-content>\n        <ion-row class="footer-card">\n          <ion-col>\n            <button ion-button (click)="getFile(material.Arquivo)">Baixar</button>\n          </ion-col>\n        </ion-row>\n      </ion-card>\n\n      <div class="NoContentmsg" *ngIf="listMaterial==\'\'">\n        <!-- <img src="" alt=""> -->\n        <i class="fa fa-frown-o" aria-hidden="true"></i>\n        <p>Nenhuma pesquisa encontrada</p>\n      </div>\n    </div>\n    <ion-infinite-scroll (ionInfinite)="doInfinite($event)">\n      <ion-infinite-scroll-content></ion-infinite-scroll-content>\n    </ion-infinite-scroll>\n  </section>\n</ion-content>\n'/*ion-inline-end:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/material-institucional/material-institucional.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular_components_loading_loading_controller__["a" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_institucional_institucional__["a" /* InstitucionalProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_utils_utils__["a" /* UtilsProvider */],
            __WEBPACK_IMPORTED_MODULE_6__providers_download_download__["a" /* DownloadProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ModalController */]])
    ], MaterialInstitucionalPage);
    return MaterialInstitucionalPage;
}());

//# sourceMappingURL=material-institucional.js.map

/***/ }),
/* 205 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InstitucionalProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_config__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__auth_auth__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var InstitucionalProvider = /** @class */ (function () {
    function InstitucionalProvider(http, auth) {
        this.http = http;
        this.auth = auth;
    }
    InstitucionalProvider.prototype.findAll = function () {
        var _this = this;
        var token = this.auth.getToken();
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/Conteudos?TipoConteudoId=13";
        var header = { "headers": { "Content-Type": 'application/json', "Authorization": "Bearer " + token } };
        return new Promise(function (resolve, reject) {
            _this.http.get(url, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    InstitucionalProvider.prototype.findById = function (id) {
        var _this = this;
        var token = this.auth.getToken();
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/Conteudos/" + id;
        var header = { "headers": { "Content-Type": 'application/json', "Authorization": "Bearer " + token } };
        return new Promise(function (resolve, reject) {
            _this.http.get(url, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    InstitucionalProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_3__auth_auth__["a" /* AuthProvider */]])
    ], InstitucionalProvider);
    return InstitucionalProvider;
}());

//# sourceMappingURL=institucional.js.map

/***/ }),
/* 206 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DownloadProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_file_transfer__ = __webpack_require__(92);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_android_permissions__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_file__ = __webpack_require__(207);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var DownloadProvider = /** @class */ (function () {
    function DownloadProvider(http, transfer, alertCtrl, platform, androidPermissions, modalCtrl, file) {
        var _this = this;
        this.http = http;
        this.transfer = transfer;
        this.alertCtrl = alertCtrl;
        this.platform = platform;
        this.androidPermissions = androidPermissions;
        this.modalCtrl = modalCtrl;
        this.file = file;
        this.storageDirectory = '';
        console.log('Hello DownloadProvider Provider');
        this.platform.ready().then(function () {
            // make sure this is on a device, not an emulation (e.g. chrome tools device mode)
            if (!_this.platform.is('cordova')) {
                return false;
            }
            if (_this.platform.is('ios')) {
                _this.storageDirectory = cordova.file.documentsDirectory;
            }
            else if (_this.platform.is('android')) {
                _this.storageDirectory = cordova.file.dataDirectory;
            }
            else {
                // exit otherwise, but you could add further types here e.g. Windows
                return false;
            }
        });
    }
    DownloadProvider.prototype.showAlert = function (text, message) {
        if (message === void 0) { message = undefined; }
        var alert = this.alertCtrl.create({
            title: "OK",
            buttons: [
                {
                    text: 'Fechar',
                    role: 'cancel'
                }
            ]
        });
    };
    DownloadProvider.prototype.download = function (url) {
        var _this = this;
        if (!this.platform.is('cordova')) {
            return false;
        }
        var fileTransfer = this.transfer.create();
        var fileName = url.substring(url.lastIndexOf('/') + 1);
        var directory = '';
        if (this.platform.is('ios')) {
            directory = cordova.file.documentsDirectory;
            fileTransfer.download(url, directory + fileName).then(function (entry) {
                _this.showAlert('Download concluído');
                console.log('download complete: ' + entry.toURL());
            }, function (error) {
                console.log(error);
                // handle error
            });
        }
        else if (this.platform.is('android')) {
            directory = 'file:///storage/emulated/0/Download/';
            this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE)
                .then(function (result) {
                console.log(result.hasPermission);
                if (!result.hasPermission) {
                    _this.androidPermissions.requestPermission(_this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE);
                }
                else {
                    fileTransfer.download(url, directory + fileName).then(function (entry) {
                        _this.showAlert('Download concluído');
                        console.log('download complete: ' + entry.toURL());
                    }, function (error) {
                        console.log(error);
                        // handle error
                    });
                }
            }, function (err) { return _this.androidPermissions.requestPermission(_this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE); });
        }
    };
    DownloadProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_file_transfer__["a" /* FileTransfer */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["m" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_android_permissions__["a" /* AndroidPermissions */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_file__["a" /* File */]])
    ], DownloadProvider);
    return DownloadProvider;
}());

//# sourceMappingURL=download.js.map

/***/ }),
/* 207 */,
/* 208 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NoticiasNoticiaDetalhePageModule", function() { return NoticiasNoticiaDetalhePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__noticias_noticia_detalhe__ = __webpack_require__(335);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pipes_pipes_module__ = __webpack_require__(209);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var NoticiasNoticiaDetalhePageModule = /** @class */ (function () {
    function NoticiasNoticiaDetalhePageModule() {
    }
    NoticiasNoticiaDetalhePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__noticias_noticia_detalhe__["a" /* NoticiasNoticiaDetalhePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__noticias_noticia_detalhe__["a" /* NoticiasNoticiaDetalhePage */]),
                __WEBPACK_IMPORTED_MODULE_3__pipes_pipes_module__["a" /* PipesModule */]
            ],
        })
    ], NoticiasNoticiaDetalhePageModule);
    return NoticiasNoticiaDetalhePageModule;
}());

//# sourceMappingURL=noticias-noticia-detalhe.module.js.map

/***/ }),
/* 209 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PipesModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__excerpt_excerpt__ = __webpack_require__(336);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__notifications_notifications__ = __webpack_require__(337);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PipesModule = /** @class */ (function () {
    function PipesModule() {
    }
    PipesModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [__WEBPACK_IMPORTED_MODULE_1__excerpt_excerpt__["a" /* ExcerptPipe */],
                __WEBPACK_IMPORTED_MODULE_2__notifications_notifications__["a" /* NotificationsPipe */],],
            imports: [],
            exports: [__WEBPACK_IMPORTED_MODULE_1__excerpt_excerpt__["a" /* ExcerptPipe */],
                __WEBPACK_IMPORTED_MODULE_2__notifications_notifications__["a" /* NotificationsPipe */],]
        })
    ], PipesModule);
    return PipesModule;
}());

//# sourceMappingURL=pipes.module.js.map

/***/ }),
/* 210 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificacoesPageModule", function() { return NotificacoesPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__notificacoes__ = __webpack_require__(338);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var NotificacoesPageModule = /** @class */ (function () {
    function NotificacoesPageModule() {
    }
    NotificacoesPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__notificacoes__["a" /* NotificacoesPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__notificacoes__["a" /* NotificacoesPage */]),
            ],
        })
    ], NotificacoesPageModule);
    return NotificacoesPageModule;
}());

//# sourceMappingURL=notificacoes.module.js.map

/***/ }),
/* 211 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ParticipacaoEnviarParticipacaoPageModule", function() { return ParticipacaoEnviarParticipacaoPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__participacao_enviar_participacao__ = __webpack_require__(339);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ParticipacaoEnviarParticipacaoPageModule = /** @class */ (function () {
    function ParticipacaoEnviarParticipacaoPageModule() {
    }
    ParticipacaoEnviarParticipacaoPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__participacao_enviar_participacao__["a" /* ParticipacaoEnviarParticipacaoPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__participacao_enviar_participacao__["a" /* ParticipacaoEnviarParticipacaoPage */]),
            ],
        })
    ], ParticipacaoEnviarParticipacaoPageModule);
    return ParticipacaoEnviarParticipacaoPageModule;
}());

//# sourceMappingURL=participacao-enviar-participacao.module.js.map

/***/ }),
/* 212 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PerfilPageModule", function() { return PerfilPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__perfil__ = __webpack_require__(340);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PerfilPageModule = /** @class */ (function () {
    function PerfilPageModule() {
    }
    PerfilPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__perfil__["a" /* PerfilPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__perfil__["a" /* PerfilPage */]),
            ],
        })
    ], PerfilPageModule);
    return PerfilPageModule;
}());

//# sourceMappingURL=perfil.module.js.map

/***/ }),
/* 213 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IModel; });
var IModel = /** @class */ (function () {
    function IModel() {
    }
    IModel.prototype.fromJSON = function (json) {
        for (var propName in json)
            this[propName] = json[propName];
        return this;
    };
    return IModel;
}());

//# sourceMappingURL=IModel.js.map

/***/ }),
/* 214 */,
/* 215 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PesquisaPageModule", function() { return PesquisaPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pesquisa__ = __webpack_require__(96);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PesquisaPageModule = /** @class */ (function () {
    function PesquisaPageModule() {
    }
    PesquisaPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__pesquisa__["b" /* PesquisaPage */],
                __WEBPACK_IMPORTED_MODULE_2__pesquisa__["a" /* PesquisaModalPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__pesquisa__["b" /* PesquisaPage */]),
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__pesquisa__["a" /* PesquisaModalPage */])
            ],
        })
    ], PesquisaPageModule);
    return PesquisaPageModule;
}());

//# sourceMappingURL=pesquisa.module.js.map

/***/ }),
/* 216 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProgramaGovernoPageModule", function() { return ProgramaGovernoPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__programa_governo__ = __webpack_require__(342);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__convide_amigos__ = __webpack_require__(217);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var ProgramaGovernoPageModule = /** @class */ (function () {
    function ProgramaGovernoPageModule() {
    }
    ProgramaGovernoPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__programa_governo__["a" /* ProgramaGovernoPage */],
                __WEBPACK_IMPORTED_MODULE_3__convide_amigos__["a" /* ConvideAmigosPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__programa_governo__["a" /* ProgramaGovernoPage */]),
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_3__convide_amigos__["a" /* ConvideAmigosPage */])
            ],
        })
    ], ProgramaGovernoPageModule);
    return ProgramaGovernoPageModule;
}());

//# sourceMappingURL=programa-governo.module.js.map

/***/ }),
/* 217 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConvideAmigosPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ConvideAmigosPage = /** @class */ (function () {
    function ConvideAmigosPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ConvideAmigosPage.prototype.ionViewDidLoad = function () {
    };
    ConvideAmigosPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'convide-amigos',template:/*ion-inline-start:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/programa-governo/modal-convideamigos.html"*/'<div class="container-round" text-center>\n  <header>\n    <h3 class="title-blue">Amigos que estão no movimento</h3>\n    <p>Seus amigos do Facebook</p>\n  </header>\n  <ion-list>\n      <ion-item>\n        <ion-avatar item-start>\n            <img src="http://via.placeholder.com/45x45" class="avatar">\n        </ion-avatar>\n        <h2>Nome da Pessoa</h2>\n        <p>Pedir Convite</p>\n      </ion-item>  \n      <ion-item>\n        <ion-avatar item-start>\n          <img src="http://via.placeholder.com/45x45" class="avatar">\n        </ion-avatar>\n        <h2>Nome da Pessoa</h2>\n        <p>Pedir Convite</p>\n      </ion-item>\n      <ion-item>\n        <ion-avatar item-start>\n            <img src="http://via.placeholder.com/45x45" class="avatar">\n        </ion-avatar>\n        <h2>Nome da Pessoa</h2>\n        <p>Pedir Convite</p>\n      </ion-item>  \n      <ion-item>\n        <ion-avatar item-start>\n          <img src="http://via.placeholder.com/45x45" class="avatar">\n        </ion-avatar>\n        <h2>Nome da Pessoa</h2>\n        <p>Pedir Convite</p>\n      </ion-item>\n      <ion-item>\n        <ion-avatar item-start>\n            <img src="http://via.placeholder.com/45x45" class="avatar">\n        </ion-avatar>\n        <h2>Nome da Pessoa</h2>\n        <p>Pedir Convite</p>\n      </ion-item>  \n      <ion-item>\n        <ion-avatar item-start>\n          <img src="http://via.placeholder.com/45x45" class="avatar">\n        </ion-avatar>\n        <h2>Nome da Pessoa</h2>\n        <p>Pedir Convite</p>\n      </ion-item>\n      <ion-item>\n        <ion-avatar item-start>\n            <img src="http://via.placeholder.com/45x45" class="avatar">\n        </ion-avatar>\n        <h2>Nome da Pessoa</h2>\n        <p>Pedir Convite</p>\n      </ion-item>  \n      <ion-item>\n        <ion-avatar item-start>\n          <img src="http://via.placeholder.com/45x45" class="avatar">\n        </ion-avatar>\n        <h2>Nome da Pessoa</h2>\n        <p>Pedir Convite</p>\n      </ion-item>\n  </ion-list>\n</div>\n\n'/*ion-inline-end:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/programa-governo/modal-convideamigos.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */]])
    ], ConvideAmigosPage);
    return ConvideAmigosPage;
}());

//# sourceMappingURL=convide-amigos.js.map

/***/ }),
/* 218 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GeralProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_config__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__auth_auth__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var GeralProvider = /** @class */ (function () {
    function GeralProvider(http, auth) {
        this.http = http;
        this.auth = auth;
    }
    GeralProvider.prototype.findAll = function () {
        var _this = this;
        var token = this.auth.getToken();
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/Conteudos?TipoConteudoId=14";
        var header = { "headers": { "Content-Type": 'application/json', "Authorization": "Bearer " + token } };
        return new Promise(function (resolve, reject) {
            _this.http.get(url, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result[result.length - 1]);
            }, function (error) {
                reject(error);
            });
        });
    };
    GeralProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_3__auth_auth__["a" /* AuthProvider */]])
    ], GeralProvider);
    return GeralProvider;
}());

//# sourceMappingURL=geral.js.map

/***/ }),
/* 219 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProgramaPontosPageModule", function() { return ProgramaPontosPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__programa_pontos__ = __webpack_require__(220);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ProgramaPontosPageModule = /** @class */ (function () {
    function ProgramaPontosPageModule() {
    }
    ProgramaPontosPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__programa_pontos__["a" /* ProgramaPontosPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__programa_pontos__["a" /* ProgramaPontosPage */]),
            ],
        })
    ], ProgramaPontosPageModule);
    return ProgramaPontosPageModule;
}());

//# sourceMappingURL=programa-pontos.module.js.map

/***/ }),
/* 220 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProgramaPontosPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_ponto_ponto__ = __webpack_require__(221);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_person_person__ = __webpack_require__(22);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the ProgramaPontosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ProgramaPontosPage = /** @class */ (function () {
    function ProgramaPontosPage(navCtrl, navParams, pontos, loadingCtrl, pessoa, events) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.pontos = pontos;
        this.loadingCtrl = loadingCtrl;
        this.pessoa = pessoa;
        this.events = events;
        this.tab = 'como-funciona';
        if (navParams.data) {
            this.tab = navParams.data;
            this.events.publish('menu:closed', '');
        }
        this.loadAll();
    }
    ProgramaPontosPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ProgramaPontosPage');
    };
    ProgramaPontosPage.prototype.doRefresh = function (refresher) {
        var _this = this;
        console.log('Begin async operation', refresher);
        setTimeout(function () {
            console.log('Async operation has ended');
            _this.loadAll();
            refresher.complete();
        }, 1000);
    };
    ProgramaPontosPage.prototype.loadExtrato = function () {
        var _this = this;
        this.pontos.getExtrato().then(function (result) {
            _this.extrato = result;
            console.log(result);
        }, function (error) {
        });
    };
    ProgramaPontosPage.prototype.loadMedalhas = function () {
        var _this = this;
        this.pontos.getMedalhas().then(function (result) {
            _this.medalhas = result;
            console.log(result);
        }, function (error) {
        });
    };
    ProgramaPontosPage.prototype.loadAll = function () {
        var loading = this.loadingCtrl.create({
            content: 'Carregando...'
        });
        loading.present();
        this.getPersonDetail();
        this.loadExtrato();
        this.loadMedalhas();
        loading.dismiss();
    };
    ProgramaPontosPage.prototype.getPersonDetail = function () {
        var _this = this;
        this.pessoa.get().then(function (response) {
            _this.TotalPontos = response.TotalPontos;
            _this.MedalhaAtual = response.Medalha ? response.Medalha.Nome : "";
        }, function (error) {
            console.log(error);
        });
    };
    ProgramaPontosPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-programa-pontos',template:/*ion-inline-start:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/programa-pontos/programa-pontos.html"*/'<ion-header color="header">\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title text-center>Programa de Pontos</ion-title>\n    <ion-buttons end>\n      <!-- <button (click)="searchToggle()" class="search-header">\n      <ion-icon name="search"></ion-icon>\n    </button> -->\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n    <ion-refresher (ionRefresh)="doRefresh($event)">\n        <ion-refresher-content></ion-refresher-content>\n      </ion-refresher>\n  <header class="information-user">\n    <div>\n        <small>Você possui</small>\n        <p><strong>{{TotalPontos}}</strong> pontos</p>\n    </div>\n    <div>\n        <small>Você é um</small>\n        <p><strong>{{MedalhaAtual}}</strong></p>\n    </div>\n  </header>\n  <ion-segment [(ngModel)]="tab" color="primary" class="wrap">\n    <ion-segment-button value="como-funciona">Como Funciona</ion-segment-button>\n    <ion-segment-button value="historico">Histórico</ion-segment-button>\n    <ion-segment-button value="medalhas">Medalhas</ion-segment-button>\n  </ion-segment>\n  <div padding>\n    <div [ngSwitch]="tab">\n      <div *ngSwitchCase="\'como-funciona\'">\n        <div class="block-text">\n          <p>Quanto mais você usa o aplicativo e executa determinadas ações, mais pontos você ganha. Usuários engajados têm pontuação mais alta.</p>\n        </div>\n        <ion-list class="padding-section">\n          <ion-card>\n            <ion-card-content class="flex-card">\n              <ion-icon class="set" name="phone-portrait"></ion-icon>\n              <div>\n                <ion-card-title>Interagindo com o aplicativo</ion-card-title>\n                <p>Compartilhe os conteúdos do aplicativo, dê curtidas, crie participações. Cada ação te dá mais pontos.</p>\n              </div>\n            </ion-card-content>\n          </ion-card>\n          <!-- <ion-card>\n            <ion-card-content class="flex-card">\n              <ion-icon class="set" name="contacts"></ion-icon>\n              <div>\n                <ion-card-title>Convide amigos</ion-card-title>\n                <p>Você pode convidar amigos a fazerem parte da plataforma. Cada convite aceite lhe dará mais pontos.</p>\n                <button ion-button outline>Convidar Amigos</button>\n              </div>\n            </ion-card-content>\n          </ion-card> -->\n          <ion-card>\n            <ion-card-content class="flex-card">\n              <ion-icon class="set" name="clipboard"></ion-icon>\n              <div>\n                <ion-card-title>Complete seu cadastro</ion-card-title>\n                <p>Informe seus dados completando seu cadastro e você ganhará ainda mais pontos.É simples e rápido.</p>\n                <button ion-button outline>Completar Agora</button>\n              </div>\n            </ion-card-content>\n          </ion-card>\n          <ion-card>\n            <ion-card-content class="flex-card">\n              <ion-icon class="set" name="text"></ion-icon>\n              <div>\n                <ion-card-title>Responda Pesquisas</ion-card-title>\n                <p>A cada pesquisa que você participa, você ganha novos pontos. Veja as pesquisas em aberto e participe.</p>\n                <button ion-button outline>Ver Pesquisas</button>\n              </div>\n            </ion-card-content>\n          </ion-card>\n        </ion-list>\n      </div>\n      <div *ngSwitchCase="\'historico\'">\n        <div class="block-text">\n          <p>Confira seu histórico de ações e os pontos ganhos em cada uma através da lista abaixo.</p>\n        </div>\n        <div class="box-round">\n          <header>Histórico de Pontos</header>\n          <ion-list>\n            <ion-item *ngFor="let  ext of extrato">\n              <ion-icon name="add" item-start></ion-icon>\n              <div>\n                <small>{{ext.DataFormatada}}</small>\n                <h3>{{ext.RegraPonto.NomeTipo}}</h3>\n              </div>\n              <div item-end class="plus">+{{ext.Pontos}}pts</div>\n            </ion-item>\n          </ion-list>\n        </div>\n      </div>\n      <div *ngSwitchCase="\'medalhas\'">\n        <div class="block-text">\n          <p>Quanto mais pontos você fizer, mais medalhas você conquista. Confira as medalhas disponveis e a pontuação que você precisa obter para cada uma.            </p>\n        </div>\n        <div class="box-round">\n          <header>Categoria</header>\n          <ion-list>\n            <ion-item *ngFor="let medalha of medalhas">\n              <div>\n                <h3>{{medalha.Nome}}</h3>\n              </div>\n              <div item-end class="plus">{{medalha.Pontos}}pts</div>\n            </ion-item>\n          </ion-list>\n        </div>\n      </div>\n    </div>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/programa-pontos/programa-pontos.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_ponto_ponto__["a" /* PontoProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_person_person__["a" /* PersonProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */]])
    ], ProgramaPontosPage);
    return ProgramaPontosPage;
}());

//# sourceMappingURL=programa-pontos.js.map

/***/ }),
/* 221 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PontoProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_config__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__auth_auth__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var PontoProvider = /** @class */ (function () {
    function PontoProvider(http, auth) {
        this.http = http;
        this.auth = auth;
    }
    PontoProvider.prototype.getExtrato = function () {
        var _this = this;
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/Pontos/Extrato";
        var token = this.auth.getToken();
        var header = { "headers": { "authorization": 'bearer ' + token } };
        return new Promise(function (resolve, reject) {
            _this.http.get(url, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    PontoProvider.prototype.getMedalhas = function () {
        var _this = this;
        var url = __WEBPACK_IMPORTED_MODULE_2__app_config__["a" /* CONFIG_PROJECT */].baseApi + "/Pontos/Medalhas";
        var token = this.auth.getToken();
        var header = { "headers": { "authorization": 'bearer ' + token } };
        return new Promise(function (resolve, reject) {
            _this.http.get(url, header)
                .map(function (res) { return res; })
                .subscribe(function (result) {
                resolve(result);
            }, function (error) {
                reject(error);
            });
        });
    };
    PontoProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_3__auth_auth__["a" /* AuthProvider */]])
    ], PontoProvider);
    return PontoProvider;
}());

//# sourceMappingURL=ponto.js.map

/***/ }),
/* 222 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QueroContribuirPageModule", function() { return QueroContribuirPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__quero_contribuir__ = __webpack_require__(97);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_brmasker_ionic_3__ = __webpack_require__(64);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var QueroContribuirPageModule = /** @class */ (function () {
    function QueroContribuirPageModule() {
    }
    QueroContribuirPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__quero_contribuir__["a" /* QueroContribuirPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__quero_contribuir__["a" /* QueroContribuirPage */]),
                __WEBPACK_IMPORTED_MODULE_3_brmasker_ionic_3__["a" /* BrMaskerModule */]
            ],
        })
    ], QueroContribuirPageModule);
    return QueroContribuirPageModule;
}());

//# sourceMappingURL=quero-contribuir.module.js.map

/***/ }),
/* 223 */,
/* 224 */,
/* 225 */,
/* 226 */,
/* 227 */,
/* 228 */,
/* 229 */,
/* 230 */,
/* 231 */,
/* 232 */,
/* 233 */,
/* 234 */,
/* 235 */,
/* 236 */,
/* 237 */,
/* 238 */,
/* 239 */,
/* 240 */,
/* 241 */,
/* 242 */,
/* 243 */,
/* 244 */,
/* 245 */,
/* 246 */,
/* 247 */,
/* 248 */,
/* 249 */,
/* 250 */,
/* 251 */,
/* 252 */,
/* 253 */,
/* 254 */,
/* 255 */,
/* 256 */,
/* 257 */,
/* 258 */,
/* 259 */,
/* 260 */,
/* 261 */,
/* 262 */,
/* 263 */,
/* 264 */,
/* 265 */,
/* 266 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TutorialPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_auth__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var TutorialPage = /** @class */ (function () {
    function TutorialPage(navCtrl, navParams, auth, loading) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.auth = auth;
        this.loading = loading;
        this.hiddenNext = false;
    }
    TutorialPage.prototype.ionViewDidLoad = function () { };
    TutorialPage.prototype.goState = function () {
        var _this = this;
        var loading = this.loading.create({
            content: 'Carregando...'
        });
        var TIME_IN_MS = 3000;
        loading.present();
        this.auth.postLogin();
        var hideFooterTimeout = setTimeout(function () {
            loading.dismiss();
            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]);
        }, TIME_IN_MS);
    };
    TutorialPage.prototype.goToSlide = function () {
        this.slides.slideNext();
    };
    TutorialPage.prototype.slideChanged = function () {
        this.slides.isEnd() == true ? this.hiddenNext = true : this.hiddenNext = false;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Slides */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Slides */])
    ], TutorialPage.prototype, "slides", void 0);
    TutorialPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-tutorial',template:/*ion-inline-start:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/tutorial/tutorial.html"*/'<ion-content >\n    <ion-slides pager (ionSlideDidChange)="slideChanged()">\n\n      <ion-slide style="background: url(assets/imgs/bgTutorial1.png) no-repeat top; background-size: cover!important;">\n        <img src="./assets/imgs/tutorial1.png" class="ico">\n        <h2 ><strong>Cadastre as doações</strong> através do aplicativo\n        </h2>\n\n        <footer class="footer">\n          <button class="bt-skip" (click)="goState()">Pular</button>\n          <i class="fa fa-arrow-right" aria-hidden="true" [hidden]="hiddenNext" (click)="goToSlide()"></i>\n        </footer>\n      </ion-slide>\n\n      <ion-slide  style="background: url(assets/imgs/bgTutorial2.png) no-repeat top; background-size: cover!important;">\n        <img src="./assets/imgs/tutorial2.png" class="ico">\n        <h2><strong>Acesse informações</strong> do MDB para as campanhas</h2>\n\n        <footer class="footer">\n          <button class="bt-skip" (click)="goState()">Pular</button>\n          <i class="fa fa-arrow-right" aria-hidden="true" [hidden]="hiddenNext" (click)="goToSlide()"></i>\n        </footer>\n      </ion-slide>\n\n      <ion-slide  style="background: url(assets/imgs/bgTutorial3.png) no-repeat top; background-size: cover!important;">\n        <img src="./assets/imgs/tutorial3.png" class="ico">\n        <h2><strong>Acesse conteúdos</strong> para divulgação</h2>\n        \n\n        <footer class="footer flexend">\n          <i class="fa fa-arrow-right" aria-hidden="true" (click)="goState()"></i>\n        </footer>\n      </ion-slide>\n\n\n    </ion-slides>\n</ion-content>\n'/*ion-inline-end:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/tutorial/tutorial.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__providers_auth_auth__["a" /* AuthProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */]])
    ], TutorialPage);
    return TutorialPage;
}());

//# sourceMappingURL=tutorial.js.map

/***/ }),
/* 267 */,
/* 268 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IndiqueAmigosPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var IndiqueAmigosPage = /** @class */ (function () {
    function IndiqueAmigosPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    IndiqueAmigosPage.prototype.ionViewDidLoad = function () {
    };
    IndiqueAmigosPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'indique-amigos',template:/*ion-inline-start:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/voluntario/indique-amigos.html"*/'<div class="container-round" text-center>\n  <header>\n    <h3 class="title-blue">Indique amigos</h3>\n    <p>Ganhe X pontos a cada amigo que se cadastrar.</p>\n  </header>\n  <h4 class="title">Digite o e-mail de seu amigo para enviar sua indicação.</h4>\n  <ion-list>\n    <ion-item>\n      <ion-label stacked text-uppercase>E-mail</ion-label>\n      <ion-input type="text" placeholder="Digite o e-mail de seu amigo(a)"></ion-input>\n    </ion-item>\n    <button ion-button clear> <ion-icon name="add"></ion-icon> Adicionar mais um amigo</button>\n    <div margin-top>\n        <button ion-button block>Enviar</button>\n    </div>\n  </ion-list>\n</div>\n\n'/*ion-inline-end:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/voluntario/indique-amigos.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */]])
    ], IndiqueAmigosPage);
    return IndiqueAmigosPage;
}());

//# sourceMappingURL=indique-amigos.js.map

/***/ }),
/* 269 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(270);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(289);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),
/* 270 */,
/* 271 */,
/* 272 */,
/* 273 */,
/* 274 */,
/* 275 */,
/* 276 */,
/* 277 */,
/* 278 */,
/* 279 */,
/* 280 */,
/* 281 */,
/* 282 */,
/* 283 */,
/* 284 */,
/* 285 */,
/* 286 */,
/* 287 */,
/* 288 */,
/* 289 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_text_mask__ = __webpack_require__(361);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_text_mask___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_angular2_text_mask__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_component__ = __webpack_require__(362);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_home_home__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_status_bar__ = __webpack_require__(263);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_splash_screen__ = __webpack_require__(264);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_storage__ = __webpack_require__(364);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_facebook__ = __webpack_require__(184);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_login_login_module__ = __webpack_require__(202);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_voluntario_voluntario_module__ = __webpack_require__(368);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_notificacoes_notificacoes_module__ = __webpack_require__(210);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_comites_comites_module__ = __webpack_require__(185);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_participacao_participacao_module__ = __webpack_require__(370);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_tutorial_tutorial_module__ = __webpack_require__(372);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_pesquisa_pesquisa_module__ = __webpack_require__(215);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_programa_governo_programa_governo_module__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_convite_convite_module__ = __webpack_require__(187);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_noticias_noticia_detalhe_noticias_noticia_detalhe_module__ = __webpack_require__(208);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_perfil_perfil_module__ = __webpack_require__(212);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_material_institucional_material_institucional_module__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_programa_pontos_programa_pontos_module__ = __webpack_require__(219);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__components_searchbar_searchbar__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__providers_auth_auth__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__providers_person_person__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__angular_forms__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__angular_common_http__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__pages_list_candidatos_list_candidatos_module__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__pages_quero_contribuir_quero_contribuir_module__ = __webpack_require__(222);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__pages_list_programa_list_programa_module__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__pages_detail_programa_detail_programa_module__ = __webpack_require__(192);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__pages_eleicoes_data_prazos_eleicoes_data_prazos_module__ = __webpack_require__(194);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__pages_eleicoes_oque_posso_fazer_eleicoes_oque_posso_fazer_module__ = __webpack_require__(196);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__pages_detail_eleicoes_detail_eleicoes_module__ = __webpack_require__(188);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__pages_conheca_partido_conheca_partido_module__ = __webpack_require__(190);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__angular_http__ = __webpack_require__(267);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__providers_utils_utils__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38_brmasker_ionic_3__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__providers_agenda_agenda__ = __webpack_require__(122);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__angular_common__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41__angular_common_locales_pt__ = __webpack_require__(373);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__providers_conteudo_conteudo__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43__pipes_pipes_module__ = __webpack_require__(209);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_44__providers_comite_comite__ = __webpack_require__(120);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_45__providers_notificacao_notificacao__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_46__providers_geral_geral__ = __webpack_require__(218);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_47__providers_voluntario_voluntario__ = __webpack_require__(124);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_48__providers_institucional_institucional__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_49__ionic_native_file_transfer__ = __webpack_require__(92);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_50__ionic_native_file__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_51__providers_pesquisa_pesquisa__ = __webpack_require__(123);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_52__providers_ponto_ponto__ = __webpack_require__(221);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_53__ionic_native_android_permissions__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_54__ionic_native_camera__ = __webpack_require__(214);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_55__ionic_native_network__ = __webpack_require__(265);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_56__ionic_native_social_sharing__ = __webpack_require__(95);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_57__pages_social_share_social_share__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_58__pages_modal_sucesso_modal_sucesso__ = __webpack_require__(182);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_59__providers_doacao_doacao__ = __webpack_require__(98);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_60__providers_download_download__ = __webpack_require__(206);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




































//Modulos

























// Registrando a lingua Portuguesa na aplicação
Object(__WEBPACK_IMPORTED_MODULE_40__angular_common__["j" /* registerLocaleData */])(__WEBPACK_IMPORTED_MODULE_41__angular_common_locales_pt__["a" /* default */], 'pt');
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_5__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_23__components_searchbar_searchbar__["a" /* SearchbarComponent */],
                __WEBPACK_IMPORTED_MODULE_57__pages_social_share_social_share__["a" /* SocialSharePage */],
                __WEBPACK_IMPORTED_MODULE_58__pages_modal_sucesso_modal_sucesso__["a" /* ModalSucessoPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_3_angular2_text_mask__["TextMaskModule"],
                __WEBPACK_IMPORTED_MODULE_27__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_10__pages_login_login_module__["LoginPageModule"],
                __WEBPACK_IMPORTED_MODULE_11__pages_voluntario_voluntario_module__["a" /* VoluntarioPageModule */],
                __WEBPACK_IMPORTED_MODULE_13__pages_comites_comites_module__["ComitesPageModule"],
                __WEBPACK_IMPORTED_MODULE_12__pages_notificacoes_notificacoes_module__["NotificacoesPageModule"],
                __WEBPACK_IMPORTED_MODULE_14__pages_participacao_participacao_module__["a" /* ParticipacaoPageModule */],
                __WEBPACK_IMPORTED_MODULE_15__pages_tutorial_tutorial_module__["a" /* TutorialPageModule */],
                __WEBPACK_IMPORTED_MODULE_16__pages_pesquisa_pesquisa_module__["PesquisaPageModule"],
                __WEBPACK_IMPORTED_MODULE_18__pages_convite_convite_module__["ConvitePageModule"],
                __WEBPACK_IMPORTED_MODULE_17__pages_programa_governo_programa_governo_module__["ProgramaGovernoPageModule"],
                __WEBPACK_IMPORTED_MODULE_20__pages_perfil_perfil_module__["PerfilPageModule"],
                __WEBPACK_IMPORTED_MODULE_19__pages_noticias_noticia_detalhe_noticias_noticia_detalhe_module__["NoticiasNoticiaDetalhePageModule"],
                __WEBPACK_IMPORTED_MODULE_21__pages_material_institucional_material_institucional_module__["MaterialInstitucionalPageModule"],
                __WEBPACK_IMPORTED_MODULE_22__pages_programa_pontos_programa_pontos_module__["ProgramaPontosPageModule"],
                __WEBPACK_IMPORTED_MODULE_28__pages_list_candidatos_list_candidatos_module__["ListCandidatosPageModule"],
                __WEBPACK_IMPORTED_MODULE_30__pages_list_programa_list_programa_module__["ListProgramaPageModule"], __WEBPACK_IMPORTED_MODULE_31__pages_detail_programa_detail_programa_module__["DetailProgramaPageModule"],
                __WEBPACK_IMPORTED_MODULE_33__pages_eleicoes_oque_posso_fazer_eleicoes_oque_posso_fazer_module__["EleicoesOquePossoFazerPageModule"],
                __WEBPACK_IMPORTED_MODULE_32__pages_eleicoes_data_prazos_eleicoes_data_prazos_module__["EleicoesDataPrazosPageModule"], __WEBPACK_IMPORTED_MODULE_34__pages_detail_eleicoes_detail_eleicoes_module__["DetailEleicoesPageModule"],
                __WEBPACK_IMPORTED_MODULE_35__pages_conheca_partido_conheca_partido_module__["ConhecaPartidoPageModule"],
                __WEBPACK_IMPORTED_MODULE_36__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_43__pipes_pipes_module__["a" /* PipesModule */],
                __WEBPACK_IMPORTED_MODULE_38_brmasker_ionic_3__["a" /* BrMaskerModule */],
                __WEBPACK_IMPORTED_MODULE_26__angular_forms__["FormsModule"],
                __WEBPACK_IMPORTED_MODULE_38_brmasker_ionic_3__["a" /* BrMaskerModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */], {
                    menuType: 'overlay',
                    backButtonText: ' '
                }, {
                    links: [
                        { loadChildren: '../pages/cadastro/cadastro-tipoperfil.module#CadastroTipoPerfilPageModule', name: 'cadastro-tipoperfil', segment: 'cadastro-tipoperfil', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/comites/comites-comites-detalhe/comites-comites-detalhe.module#ComitesComitesDetalhePageModule', name: 'comites-detalhe', segment: 'comites-detalhe/:id', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/cadastro/cadastro.module#CadastroPageModule', name: 'cadastro', segment: 'cadastro', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/comites/comites.module#ComitesPageModule', name: 'comites', segment: 'comites', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/convite/convite.module#ConvitePageModule', name: 'convite', segment: 'convite', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/dados-complementares/dados-complementares.module#DadosComplementaresPageModule', name: 'dados-complementares', segment: 'dados-complementares', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/detail-eleicoes/detail-eleicoes.module#DetailEleicoesPageModule', name: 'DetailEleicoesPage', segment: 'detail-eleicoes', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/conheca-partido/conheca-partido.module#ConhecaPartidoPageModule', name: 'ConhecaPartidoPage', segment: 'conheca-partido', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/detail-programa/detail-programa.module#DetailProgramaPageModule', name: 'DetailProgramaPage', segment: 'detail-programa', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/eleicoes-data-prazos/eleicoes-data-prazos.module#EleicoesDataPrazosPageModule', name: 'EleicoesDataPrazosPage', segment: 'eleicoes-data-prazos', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/eleicoes-oque-posso-fazer/eleicoes-oque-posso-fazer.module#EleicoesOquePossoFazerPageModule', name: 'EleicoesOquePossoFazerPage', segment: 'eleicoes-oque-posso-fazer', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/evento/evento.module#EventoPageModule', name: 'evento', segment: 'evento/:id', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/list-candidatos/list-candidatos.module#ListCandidatosPageModule', name: 'ListCandidatosPage', segment: 'list-candidatos', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/list-programa/list-programa.module#ListProgramaPageModule', name: 'ListProgramaPage', segment: 'list-programa', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login-login-esqueci/login-login-esqueci.module#LoginLoginEsqueciPageModule', name: 'esqueci-senha', segment: 'login-login-esqueci', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'login', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/material-institucional/material-institucional.module#MaterialInstitucionalPageModule', name: 'MaterialInstitucionalPage', segment: 'material-institucional', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/noticias-noticia-detalhe/noticias-noticia-detalhe.module#NoticiasNoticiaDetalhePageModule', name: 'noticias', segment: 'noticias/:id', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/notificacoes/notificacoes-notificacoes-detalhe/notificacoes-notificacoes-detalhe.module#NotificacoesNotificacoesDetalhePageModule', name: 'notificacoes', segment: 'notificacoes/:id', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/notificacoes/notificacoes.module#NotificacoesPageModule', name: 'NotificacoesPage', segment: 'notificacoes', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/participacao/participacao-enviar-participacao/participacao-enviar-participacao.module#ParticipacaoEnviarParticipacaoPageModule', name: 'form-participacao', segment: 'form-participacao', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/participacao/participacao-participacao-detalhe/participacao-participacao-detalhe.module#ParticipacaoParticipacaoDetalhePageModule', name: 'participacao-detalhe', segment: 'participacao/:id', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/perfil/perfil.module#PerfilPageModule', name: 'perfil', segment: 'perfil', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/pesquisa/pesquisa-pesquisa-resposta/pesquisa-pesquisa-resposta.module#PesquisaPesquisaRespostaPageModule', name: 'pesquisa-resposta', segment: 'pesquisa-resposta/:id', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/pesquisa/pesquisa.module#PesquisaPageModule', name: 'pesquisa-opniao', segment: 'pesquisa', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/programa-governo/programa-governo.module#ProgramaGovernoPageModule', name: 'ProgramaGovernoPage', segment: 'programa-governo', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/programa-pontos/programa-pontos.module#ProgramaPontosPageModule', name: 'ProgramaPontosPage', segment: 'programa-pontos', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/quero-contribuir/quero-contribuir.module#QueroContribuirPageModule', name: 'quero-contribuir', segment: 'quero-contribuir', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/voluntario/voluntario-detalhe/voluntario-detalhe.module#VoluntarioDetalhePageModule', name: 'voluntario-detalhe', segment: 'voluntario-detalhe/:id', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_8__ionic_storage__["a" /* IonicStorageModule */].forRoot({
                    name: '__mydb',
                    driverOrder: ['indexeddb', 'sqlite', 'websql']
                }),
                __WEBPACK_IMPORTED_MODULE_3_angular2_text_mask__["TextMaskModule"],
                __WEBPACK_IMPORTED_MODULE_29__pages_quero_contribuir_quero_contribuir_module__["QueroContribuirPageModule"]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_5__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_23__components_searchbar_searchbar__["a" /* SearchbarComponent */],
                __WEBPACK_IMPORTED_MODULE_57__pages_social_share_social_share__["a" /* SocialSharePage */],
                __WEBPACK_IMPORTED_MODULE_58__pages_modal_sucesso_modal_sucesso__["a" /* ModalSucessoPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_6__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_7__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_9__ionic_native_facebook__["a" /* Facebook */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ErrorHandler"], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_24__providers_auth_auth__["a" /* AuthProvider */],
                __WEBPACK_IMPORTED_MODULE_25__providers_person_person__["a" /* PersonProvider */],
                __WEBPACK_IMPORTED_MODULE_39__providers_agenda_agenda__["a" /* AgendaProvider */],
                __WEBPACK_IMPORTED_MODULE_37__providers_utils_utils__["a" /* UtilsProvider */],
                __WEBPACK_IMPORTED_MODULE_42__providers_conteudo_conteudo__["a" /* ConteudoProvider */],
                __WEBPACK_IMPORTED_MODULE_44__providers_comite_comite__["a" /* ComiteProvider */],
                __WEBPACK_IMPORTED_MODULE_45__providers_notificacao_notificacao__["a" /* NotificationProvider */],
                __WEBPACK_IMPORTED_MODULE_46__providers_geral_geral__["a" /* GeralProvider */],
                __WEBPACK_IMPORTED_MODULE_47__providers_voluntario_voluntario__["a" /* VoluntarioProvider */],
                __WEBPACK_IMPORTED_MODULE_48__providers_institucional_institucional__["a" /* InstitucionalProvider */],
                __WEBPACK_IMPORTED_MODULE_49__ionic_native_file_transfer__["a" /* FileTransfer */],
                __WEBPACK_IMPORTED_MODULE_55__ionic_native_network__["a" /* Network */],
                __WEBPACK_IMPORTED_MODULE_50__ionic_native_file__["a" /* File */],
                __WEBPACK_IMPORTED_MODULE_51__providers_pesquisa_pesquisa__["a" /* PesquisaProvider */],
                __WEBPACK_IMPORTED_MODULE_52__providers_ponto_ponto__["a" /* PontoProvider */],
                __WEBPACK_IMPORTED_MODULE_53__ionic_native_android_permissions__["a" /* AndroidPermissions */],
                __WEBPACK_IMPORTED_MODULE_54__ionic_native_camera__["a" /* Camera */],
                __WEBPACK_IMPORTED_MODULE_56__ionic_native_social_sharing__["a" /* SocialSharing */],
                __WEBPACK_IMPORTED_MODULE_59__providers_doacao_doacao__["a" /* DoacaoProvider */],
                [{ provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["LOCALE_ID"], useValue: 'pt' }],
                __WEBPACK_IMPORTED_MODULE_60__providers_download_download__["a" /* DownloadProvider */],
            ],
            schemas: [
                __WEBPACK_IMPORTED_MODULE_1__angular_core__["CUSTOM_ELEMENTS_SCHEMA"],
                __WEBPACK_IMPORTED_MODULE_1__angular_core__["NO_ERRORS_SCHEMA"]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),
/* 290 */,
/* 291 */,
/* 292 */,
/* 293 */,
/* 294 */,
/* 295 */,
/* 296 */,
/* 297 */,
/* 298 */,
/* 299 */,
/* 300 */,
/* 301 */,
/* 302 */,
/* 303 */,
/* 304 */,
/* 305 */,
/* 306 */,
/* 307 */,
/* 308 */,
/* 309 */,
/* 310 */,
/* 311 */,
/* 312 */,
/* 313 */,
/* 314 */,
/* 315 */,
/* 316 */,
/* 317 */,
/* 318 */,
/* 319 */,
/* 320 */,
/* 321 */,
/* 322 */,
/* 323 */,
/* 324 */,
/* 325 */,
/* 326 */,
/* 327 */,
/* 328 */,
/* 329 */,
/* 330 */,
/* 331 */,
/* 332 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConvitePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ConvitePage = /** @class */ (function () {
    function ConvitePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ConvitePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ConvitePage');
    };
    ConvitePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-convite',template:/*ion-inline-start:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/convite/convite.html"*/'<ion-content>\n  <div padding>\n    <header class="login-header" text-center>\n        <img src="http://via.placeholder.com/100x90" class="brand">\n        <h3 class="title-blue">Você precisa ser convidado!</h3>\n        <div class="block-text">\n          <p>Para se cadastrar em nosso movimento, você precisa ter um número de convite, que pode ser obtido através de um amigo que já faça parte desse movimento.</p>\n        </div>\n    </header>\n    <ion-list>\n      <ion-item>\n        <ion-label floating text-uppercase>Email</ion-label>\n        <ion-input type="email"></ion-input>\n      </ion-item>\n    \n      <ion-item>\n        <ion-label floating text-uppercase>Número do Convite</ion-label>\n        <ion-input type="text"></ion-input>\n      </ion-item>\n\n    </ion-list>\n    <button ion-button full text-uppercase margin-top>Entrar</button>\n  </div>\n  <footer class="container-footer" text-center>\n      <h2 class="title">Não tem convite?</h2>\n      <div class="block-text">\n        <p>Faça login com o Facebook e descubra quais amigos possuem convites para te enviar.</p>\n      </div>\n      <button ion-button outline full text-uppercase margin-top color="light">Ou faça login usando Facebook</button>\n  </footer>\n</ion-content>\n<ion-footer>\n  <ion-toolbar (click)="goPage(\'login\')" text-center>\n    Voltar para a tela de <strong>Login</strong>\n  </ion-toolbar>\n</ion-footer>\n\n\n'/*ion-inline-end:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/convite/convite.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */]])
    ], ConvitePage);
    return ConvitePage;
}());

//# sourceMappingURL=convite.js.map

/***/ }),
/* 333 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var SearchPipe = /** @class */ (function () {
    function SearchPipe() {
    }
    SearchPipe.prototype.transform = function (items, searchText) {
        if (!items)
            return [];
        if (!searchText)
            return items;
        searchText = searchText.toLowerCase();
        return items.filter(function (it) {
            return it.Nome.toLowerCase().includes(searchText);
        });
    };
    SearchPipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"])({
            name: 'search',
        })
    ], SearchPipe);
    return SearchPipe;
}());

//# sourceMappingURL=search.js.map

/***/ }),
/* 334 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchCategoriasPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * Generated class for the SearchCategoriasPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
var SearchCategoriasPipe = /** @class */ (function () {
    function SearchCategoriasPipe() {
    }
    SearchCategoriasPipe.prototype.transform = function (items, searchText) {
        if (!items)
            return [];
        if (!searchText)
            return items;
        searchText = searchText.toLowerCase();
        return items.filter(function (it) {
            return it.CategoriaConteudo.Nome.toLowerCase().includes(searchText);
        });
    };
    SearchCategoriasPipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"])({
            name: 'searchCategorias',
        })
    ], SearchCategoriasPipe);
    return SearchCategoriasPipe;
}());

//# sourceMappingURL=search-categorias.js.map

/***/ }),
/* 335 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NoticiasNoticiaDetalhePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_conteudo_conteudo__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_auth__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_utils_utils__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_person_person__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__social_share_social_share__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__components_searchbar_searchbar__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var NoticiasNoticiaDetalhePage = /** @class */ (function () {
    function NoticiasNoticiaDetalhePage(navCtrl, navParams, _conteudo, auth, loading, utils, person, modalCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._conteudo = _conteudo;
        this.auth = auth;
        this.loading = loading;
        this.utils = utils;
        this.person = person;
        this.modalCtrl = modalCtrl;
        this.conteudo = {};
        this.noticiasList = [];
        this.tipoConteudo = 2;
    }
    NoticiasNoticiaDetalhePage.prototype.searchToggle = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_7__components_searchbar_searchbar__["a" /* SearchbarComponent */]);
        modal.present();
    };
    NoticiasNoticiaDetalhePage.prototype.ionViewDidLoad = function () {
        this.loadDetail();
        this.loadNews();
    };
    NoticiasNoticiaDetalhePage.prototype.loadDetail = function () {
        var _this = this;
        var loading = this.loading.create();
        loading.present();
        this.id = this.navParams.data.id;
        return new Promise(function (resolve) {
            _this._conteudo.getDetalhe(_this.auth.getToken(), _this.id)
                .then(function (data) {
                _this.conteudo = data;
                resolve(true);
                console.log(_this.conteudo, 'not detalhes');
                loading.dismiss();
            });
        });
    };
    NoticiasNoticiaDetalhePage.prototype.loadNews = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this._conteudo.getConteudo(_this.auth.getToken(), 10, 0, _this.tipoConteudo)
                .then(function (data) {
                var noticias = [];
                for (var i = 0; i < data.length; i++) {
                    if (data[i].Id != _this.conteudo.Id) {
                        noticias.push(data[i]);
                    }
                }
                _this.noticiasList = noticias;
                resolve(true);
            });
        });
    };
    NoticiasNoticiaDetalhePage.prototype.openDetail = function (page, id) {
        this.navCtrl.push(page, { id: id });
    };
    NoticiasNoticiaDetalhePage.prototype.postLike = function (conteudo) {
        var _this = this;
        var idPost = conteudo.Id;
        this.person.get()
            .then(function (response) {
            var person = response;
            _this._conteudo.postLike(idPost, person.Id)
                .then(function (response) {
                if (response.Curtiu && !conteudo.JaCurtiu) {
                    conteudo.Curtidas += 1;
                }
                else {
                    conteudo.Curtidas -= 1;
                }
                conteudo.JaCurtiu = response.Curtiu;
                if (response.GanhouPonto) {
                    _this.utils.showModalSucesso(response.Mensagem, "Continue interagindo", "OK");
                }
            });
        });
    };
    NoticiasNoticiaDetalhePage.prototype.postShare = function (conteudo) {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_6__social_share_social_share__["a" /* SocialSharePage */], conteudo);
        modal.present();
    };
    NoticiasNoticiaDetalhePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-noticias-noticia-detalhe',template:/*ion-inline-start:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/noticias-noticia-detalhe/noticias-noticia-detalhe.html"*/'<ion-header color="header">\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <i class="fa fa-bars" aria-hidden="true"></i>\n    </button>\n    <ion-title text-center>Notícias</ion-title>\n    <ion-buttons end>\n      <button (click)="searchToggle()" class="search-header">\n      <ion-icon name="search"></ion-icon>\n    </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <img [src]="conteudo.Imagem" class="img-featured">\n  <div padding>\n    <header class="buttons-event">\n      <small class="date">{{conteudo.DataHora}}</small>\n      <div class="buttons">\n      <button ion-button icon-left clear small (click)="postLike(conteudo)" [ngClass]="{\'liked\': conteudo.JaCurtiu}">\n        <ion-icon name="heart"></ion-icon>\n        <div>{{conteudo.Curtidas}}</div>\n      </button>\n      <button class="share" ion-button icon-right text-left clear small (click)="postShare(conteudo)">\n        <ion-icon name="share"></ion-icon>\n      </button>\n    </div>\n    </header>\n    <section padding>\n      <small text-uppercase class="blue-color bold">{{conteudo.CategoriaConteudo?.Nome}}</small>\n      <h2 class="bold">{{conteudo.Titulo}}</h2>\n      <div class="block-text" margin-top [innerHtml]="conteudo.Texto"></div>\n    </section>\n    <section class="padding-section">\n      <h5 class="bold">Veja outras notícias</h5>\n      <ion-slides pager spaceBetween="-40">\n        <ion-slide *ngFor="let noticia of (noticiasList | slice: -3)">\n          <ion-card text-left (click)="openDetail(\'noticias\', noticia.Id)">\n            <ion-card-content>\n              <small text-uppercase class="blue-color">{{noticia.CategoriaConteudo?.Nome}}</small>\n              <ion-card-title>{{noticia.Titulo}}</ion-card-title>\n              <div [innerHtml]="noticia.Texto | excerpt:50"></div>\n            </ion-card-content>\n            <ion-row class="footer-card">\n              <ion-col>\n                <button ion-button icon-left clear small (click)="postLike(noticia)" [ngClass]="{\'liked\': noticia.JaCurtiu}">\n                  <ion-icon name="heart"></ion-icon>\n                  <div>{{noticia.Curtidas}}</div>\n                </button>\n              </ion-col>\n              <ion-col text-right>\n                <ion-note>{{noticia.Hora}}</ion-note>\n              </ion-col>\n            </ion-row>\n          </ion-card>\n        </ion-slide>\n      </ion-slides>\n\n    </section>\n  </div>\n</ion-content>\n\n'/*ion-inline-end:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/noticias-noticia-detalhe/noticias-noticia-detalhe.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_conteudo_conteudo__["a" /* ConteudoProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_utils_utils__["a" /* UtilsProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_person_person__["a" /* PersonProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ModalController */]])
    ], NoticiasNoticiaDetalhePage);
    return NoticiasNoticiaDetalhePage;
}());

//# sourceMappingURL=noticias-noticia-detalhe.js.map

/***/ }),
/* 336 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ExcerptPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var ExcerptPipe = /** @class */ (function () {
    function ExcerptPipe() {
    }
    ExcerptPipe.prototype.transform = function (text, length) {
        if (!text || !length) {
            return text;
        }
        if (text.length > length) {
            return text.substr(0, length) + '...';
        }
        return text;
    };
    ExcerptPipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"])({
            name: 'excerpt',
            pure: false
        })
    ], ExcerptPipe);
    return ExcerptPipe;
}());

//# sourceMappingURL=excerpt.js.map

/***/ }),
/* 337 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotificationsPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var NotificationsPipe = /** @class */ (function () {
    function NotificationsPipe() {
    }
    NotificationsPipe.prototype.transform = function (value) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        var valueNum = parseInt(value);
        if (valueNum > 999) {
            return "999+";
        }
        return valueNum.toString();
    };
    NotificationsPipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"])({
            name: 'notifications',
        })
    ], NotificationsPipe);
    return NotificationsPipe;
}());

//# sourceMappingURL=notifications.js.map

/***/ }),
/* 338 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotificacoesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_notificacao_notificacao__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular_components_loading_loading_controller__ = __webpack_require__(36);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var NotificacoesPage = /** @class */ (function () {
    function NotificacoesPage(navCtrl, navParams, notification, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.notification = notification;
        this.loadingCtrl = loadingCtrl;
        this.getNotifications();
    }
    NotificacoesPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad NotificacoesPage');
    };
    NotificacoesPage.prototype.doRefresh = function (refresher) {
        var _this = this;
        console.log('Begin async operation', refresher);
        setTimeout(function () {
            console.log('Async operation has ended');
            _this.getNotifications();
            refresher.complete();
        }, 1000);
    };
    NotificacoesPage.prototype.openDetail = function (page, id) {
        this.navCtrl.push(page, id);
    };
    NotificacoesPage.prototype.getNotifications = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: 'Carregando...'
        });
        loading.present();
        this.notification.Get().then(function (response) {
            loading.dismiss();
            _this.notificationsList = response;
            console.log(response);
        }, function (error) {
            loading.dismiss();
            console.log(error);
        });
    };
    NotificacoesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-notificacoes',template:/*ion-inline-start:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/notificacoes/notificacoes.html"*/'<ion-header color="header">\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title text-center>Notificações</ion-title>\n    <ion-buttons end>\n      <!-- <button (click)="searchToggle()" class="search-header">\n      <ion-icon name="search"></ion-icon>\n    </button> -->\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n    <ion-refresher (ionRefresh)="doRefresh($event)">\n        <ion-refresher-content></ion-refresher-content>\n      </ion-refresher>\n  <ion-list margin-top>\n    <ion-card (click)="openDetail(\'notificacoes\', notification.Id)" *ngFor="let notification of notificationsList">\n      <ion-card-content>\n        <div>\n          <ion-card-title>{{notification.Titulo}}</ion-card-title>\n          <p [innerHTML]="notification.Mensagem"></p>\n          <ion-note>  <ion-icon name="calendar"></ion-icon> {{notification.DataCriacao}}</ion-note>\n        </div>\n      </ion-card-content>\n    </ion-card>\n    <p *ngIf="!notificationsList"> Não há notificações no momento</p>\n  </ion-list>\n  <ion-infinite-scroll (ionInfinite)="doInfinite($event)">\n    <ion-infinite-scroll-content></ion-infinite-scroll-content>\n  </ion-infinite-scroll>\n</ion-content>\n'/*ion-inline-end:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/notificacoes/notificacoes.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_notificacao_notificacao__["a" /* NotificationProvider */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular_components_loading_loading_controller__["a" /* LoadingController */]])
    ], NotificacoesPage);
    return NotificacoesPage;
}());

//# sourceMappingURL=notificacoes.js.map

/***/ }),
/* 339 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ParticipacaoEnviarParticipacaoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ParticipacaoEnviarParticipacaoPage = /** @class */ (function () {
    function ParticipacaoEnviarParticipacaoPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.tab = "local";
    }
    ParticipacaoEnviarParticipacaoPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ParticipacaoEnviarParticipacaoPage');
    };
    ParticipacaoEnviarParticipacaoPage.prototype.doRefresh = function (refresher) {
        var _this = this;
        console.log('Begin async operation', refresher);
        setTimeout(function () {
            console.log('Async operation has ended');
            _this.choiceCategorie();
            refresher.complete();
        }, 1000);
    };
    ParticipacaoEnviarParticipacaoPage.prototype.saveLocal = function () {
        this.tab = "categoria";
    };
    ParticipacaoEnviarParticipacaoPage.prototype.choiceCategorie = function () {
        this.tab = "tipo";
    };
    ParticipacaoEnviarParticipacaoPage.prototype.saveTipe = function () {
        this.tab = "descricao";
    };
    ParticipacaoEnviarParticipacaoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-participacao-enviar-participacao',template:/*ion-inline-start:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/participacao/participacao-enviar-participacao/participacao-enviar-participacao.html"*/'<ion-header color="header">\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title text-center>Enviar Participação</ion-title>\n    <ion-buttons end>\n      <!-- <button (click)="searchToggle()" class="search-header">\n      <ion-icon name="search"></ion-icon>\n    </button> -->\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n    <ion-refresher (ionRefresh)="doRefresh($event)">\n        <ion-refresher-content></ion-refresher-content>\n      </ion-refresher>\n  <section [ngSwitch]="tab">\n      <div *ngSwitchCase="\'local\'">\n        <div class="container-stage">\n          <div class="number">1</div>\n          <div class="text">\n            <strong>Selecione o local</strong>\n          </div>\n        </div>\n        <ion-card>\n\n          <ion-card-content>\n              <button ion-button round full icon-left outline> <ion-icon name="pin"></ion-icon> Selecionar minha localização</button>\n              <ion-row>\n                <ion-col col-5>\n                  <ion-item>\n                    <ion-label stacked>Estado</ion-label>\n                    <ion-select placeholder="Selecione">\n                      <ion-option value="number"></ion-option>\n                    </ion-select>\n                  </ion-item>\n                </ion-col>\n                <ion-col col-7>\n                  <ion-item>\n                    <ion-label stacked>Cidade</ion-label>\n                    <ion-select placeholder="Selecione">\n                      <ion-option value="number"></ion-option>\n                    </ion-select>\n                  </ion-item>\n                </ion-col>\n                <ion-col col-12>\n                  <ion-item>\n                    <ion-label stacked text-uppercase>Rua</ion-label>\n                    <ion-input type="text" placeholder="Digite o nome da sua rua"></ion-input>\n                  </ion-item>\n                </ion-col>\n                <ion-col col-5>\n                  <ion-item>\n                    <ion-label stacked text-uppercase>Número</ion-label>\n                    <ion-input type="number"></ion-input>\n                  </ion-item>\n                </ion-col>\n                <ion-col col-7>\n                  <ion-item>\n                    <ion-label stacked text-uppercase>Bairro</ion-label>\n                    <ion-input type="text"></ion-input>\n                  </ion-item>\n                </ion-col>\n                <ion-col col-12 text-center margin-top>\n                    <button ion-button round full (click)="saveLocal()"> Salvar</button>\n                </ion-col>\n              </ion-row>\n          </ion-card-content>\n\n        </ion-card>\n      </div>\n      <div *ngSwitchCase="\'categoria\'">\n        <div class="container-stage">\n          <div class="number">2</div>\n          <div class="text">\n            <strong>Selecione a Categoria</strong>\n          </div>\n        </div>\n        <ul class="list-categories">\n          <li (click)="choiceCategorie()">\n            <ion-icon name="bus"></ion-icon>\n            <p>Transporte</p>\n          </li>\n          <li (click)="choiceCategorie()">\n            <ion-icon name="leaf"></ion-icon>\n            <p>Meio Ambiente</p>\n          </li>\n          <li (click)="choiceCategorie()">\n            <ion-icon name="medkit"></ion-icon>\n            <p>Hospital</p>\n          </li>\n          <li (click)="choiceCategorie()">\n            <ion-icon name="build"></ion-icon>\n            <p>Conservação</p>\n          </li>\n          <li (click)="choiceCategorie()">\n            <ion-icon name="book"></ion-icon>\n            <p>Ordem Pública</p>\n          </li>\n          <li (click)="choiceCategorie()">\n            <ion-icon name="alert"></ion-icon>\n            <p>Defesa Civil</p>\n          </li>\n          <li (click)="choiceCategorie()">\n            <ion-icon name="trash"></ion-icon>\n            <p>Limpeza</p>\n          </li>\n          <li (click)="choiceCategorie()">\n            <ion-icon name="car"></ion-icon>\n            <p>Trânsito</p>\n          </li>\n          <li (click)="choiceCategorie()">\n            <ion-icon name="water"></ion-icon>\n            <p>Esgoto</p>\n          </li>\n          <li (click)="choiceCategorie()">\n            <ion-icon name="school"></ion-icon>\n            <p>Educação</p>\n          </li>\n          <li (click)="choiceCategorie()">\n            <ion-icon name="apps"></ion-icon>\n            <p>Outros</p>\n          </li>\n        </ul>\n      </div>\n      <div *ngSwitchCase="\'tipo\'">\n        <div class="container-stage">\n          <div class="number">3</div>\n          <div class="text">\n            <strong>Selecione o Tipo</strong>\n          </div>\n        </div>\n        <ion-list>\n          <ion-card (click)="saveTipe()">\n            <ion-card-content class="flex-card">\n              <div>\n                <ion-card-title>Foco na Dengue</ion-card-title>\n              </div>\n              <ion-icon class="set" name="ios-arrow-forward"></ion-icon>\n            </ion-card-content>\n          </ion-card>\n          <ion-card (click)="saveTipe()">\n            <ion-card-content class="flex-card">\n              <div>\n                <ion-card-title>Desmatamento</ion-card-title>\n              </div>\n              <ion-icon class="set" name="ios-arrow-forward"></ion-icon>\n            </ion-card-content>\n          </ion-card>\n          <ion-card (click)="saveTipe()">\n            <ion-card-content class="flex-card">\n              <div>\n                <ion-card-title>Queimada</ion-card-title>\n              </div>\n              <ion-icon class="set" name="ios-arrow-forward"></ion-icon>\n            </ion-card-content>\n          </ion-card>\n        </ion-list>\n      </div>\n      <div *ngSwitchCase="\'descricao\'">\n        <div class="container-stage">\n          <div class="number">4</div>\n          <div class="text">\n            <strong>Descreva sua Participação</strong>\n          </div>\n        </div>\n        <ion-card>\n          <ion-card-content>\n            <ion-list>\n              <ion-item>\n                <ion-label stacked text-uppercase>Descrição</ion-label>\n                <ion-textarea></ion-textarea>\n              </ion-item>\n            </ion-list>\n            <ion-buttons end>\n              <button ion-button icon-left full outline margin-top>\n                <ion-icon name="image"></ion-icon>\n                Escolher uma imagem da galeria\n              </button>\n              <button ion-button icon-left full outline margin-top color="basegreen">\n                <ion-icon name="camera"></ion-icon>\n                Fotograr\n              </button>\n            </ion-buttons>\n            <div margin-top>\n              <button ion-button round full (click)="saveLocal()"> Salvar</button>\n            </div>\n          </ion-card-content>\n        </ion-card>\n      </div>\n  </section>\n</ion-content>\n'/*ion-inline-end:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/participacao/participacao-enviar-participacao/participacao-enviar-participacao.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */]])
    ], ParticipacaoEnviarParticipacaoPage);
    return ParticipacaoEnviarParticipacaoPage;
}());

//# sourceMappingURL=participacao-enviar-participacao.js.map

/***/ }),
/* 340 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PerfilPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_person_person__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__models_PersonDetail__ = __webpack_require__(341);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_utils_utils__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_camera__ = __webpack_require__(214);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var PerfilPage = /** @class */ (function () {
    function PerfilPage(navCtrl, navParams, person, alertCtrl, utils, loadingCtrl, platform, actionSheetCtrl, camera) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.person = person;
        this.alertCtrl = alertCtrl;
        this.utils = utils;
        this.loadingCtrl = loadingCtrl;
        this.platform = platform;
        this.actionSheetCtrl = actionSheetCtrl;
        this.camera = camera;
        this.estados = new Array();
        this.tab = "meus-dados";
        this.selectOptEstados = {
            title: 'Selecione o estado',
            subTitle: 'Selecione o estado',
            checked: true
        };
        this.selectOptCidades = {
            title: 'Selecione a cidade',
            subTitle: 'Selecione a cidade',
            checked: true
        };
        this.user = new __WEBPACK_IMPORTED_MODULE_3__models_PersonDetail__["a" /* PersonDetail */]();
        this.getEstados();
        this.getPersonDetail();
        if (this.user.Estado != '' && this.user.Estado != null) {
            this.selectOptEstados.checked = false;
        }
    }
    PerfilPage.prototype.ionViewDidLoad = function () {
        // console.log('ionViewDidLoad PerfilPage');
    };
    PerfilPage.prototype.doRefresh = function (refresher) {
        var _this = this;
        console.log('Begin async operation', refresher);
        setTimeout(function () {
            console.log('Async operation has ended');
            _this.getPersonDetail();
            refresher.complete();
        }, 1000);
    };
    PerfilPage.prototype.getPersonDetail = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: 'Carregando...'
        });
        loading.present();
        this.person.get()
            .then(function (response) {
            loading.dismiss();
            _this.user = new __WEBPACK_IMPORTED_MODULE_3__models_PersonDetail__["a" /* PersonDetail */]().fromJSON(response);
            if (_this.user.Estado != '' && _this.user.Estado != null) {
                _this.onSelectCidade(_this.user.Estado);
            }
        }, function (error) {
            loading.dismiss();
            console.log(error);
            _this.utils.showAlert('ocorreu um erro. Tente novamente mais tarde.');
        });
    };
    PerfilPage.prototype.onSelectCidade = function (uf) {
        var _this = this;
        this.utils.getCidades(uf).then(function (result) { _this.cidades = result; }, function (error) { });
        if (this.user.Cidade != '' && this.user.Cidade != null) {
            this.selectOptCidades.checked = false;
        }
    };
    PerfilPage.prototype.getEstados = function () {
        var _this = this;
        this.utils.getEstados().then(function (result) { _this.estados = result; }, function (error) { });
    };
    PerfilPage.prototype.putPerson = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: 'Salvando...'
        });
        loading.present();
        this.person.put(this.user)
            .then(function (response) {
            loading.dismiss();
            _this.utils.showAlert(response.Titulo, response.Mensagem);
        }, function (error) {
            loading.dismiss();
            console.log(error);
            _this.utils.showAlert(error.error.Message);
        });
    };
    PerfilPage.prototype.putFoto = function (image) {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: 'Salvando...'
        });
        loading.present();
        this.person.putFoto(image)
            .then(function (response) {
            loading.dismiss();
            _this.user.CaminhoFoto = response.Url;
            if (response.GerouPonto) {
                _this.utils.showModalSucesso(response.Titulo, response.Mensagem, "OK");
            }
        }, function (error) {
            loading.dismiss();
            console.log(error);
            // this.utils.showAlert(error.error.Message);
        });
    };
    PerfilPage.prototype.uploadFoto = function (arquivo) {
        this.user.Foto = arquivo.path[0].files[0].name;
        this.user.File = arquivo.path[0].files[0];
        var fotoSize = arquivo.path[0].files[0].size / 1024;
        if (fotoSize > 4096) {
            // $scope.alertNativo();
            var alert_1 = this.alertCtrl.create({
                title: 'Ops',
                subTitle: "O tamanho máximo da foto é de 4M.",
                buttons: ['OK']
            });
            alert_1.present();
        }
    };
    PerfilPage.prototype.presentActionSheet = function () {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            title: 'Escolha uma imagem',
            buttons: [
                {
                    text: 'Abrir Galeria',
                    handler: function () {
                        _this.takePicture(_this.camera.PictureSourceType.PHOTOLIBRARY);
                    }
                },
                {
                    text: 'Usar a Camera',
                    handler: function () {
                        _this.takePicture(_this.camera.PictureSourceType.CAMERA);
                    }
                },
                {
                    text: 'Cancelar',
                    role: 'cancel'
                }
            ]
        });
        actionSheet.present();
    };
    PerfilPage.prototype.takePicture = function (sourceType) {
        var _this = this;
        // Create options for the Camera Dialog
        var options = {
            quality: 75,
            sourceType: sourceType,
            saveToPhotoAlbum: false,
            correctOrientation: true,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
        };
        this.camera.getPicture(options).then(function (imagePath) {
            _this.putFoto(imagePath);
        }, function (err) {
            // this.presentToast('Erro ao selecionar a imagem');
        });
    };
    PerfilPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-perfil',template:/*ion-inline-start:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/perfil/perfil.html"*/'<ion-header color="header">\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title text-center>Meu Perfil</ion-title>\n    <ion-buttons end>\n      <!-- <button (click)="searchToggle()" class="search-header">\n      <ion-icon name="search"></ion-icon>\n    </button> -->\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n    <ion-refresher (ionRefresh)="doRefresh($event)">\n        <ion-refresher-content></ion-refresher-content>\n      </ion-refresher>\n  <ion-segment [(ngModel)]="tab" color="primary" class="wrap">\n    <ion-segment-button value="meus-dados">Meus <br/> Dados</ion-segment-button>\n    <ion-segment-button value="endereco">Endereço</ion-segment-button>\n    <ion-segment-button value="dados-sociais">Dados Sociais</ion-segment-button>\n    <!-- <ion-segment-button value="minhas-preferencias">Minhas Preferências</ion-segment-button> -->\n  </ion-segment>\n  <div [ngSwitch]="tab" padding>\n    <div *ngSwitchCase="\'meus-dados\'">\n      <ion-row>\n        <ion-col col-4>\n          <div class="avatar-change" (click)="presentActionSheet()">\n            <img *ngIf="user.CaminhoFoto != \'\'" src="{{user.CaminhoFoto}}" class="avatar">\n            <img *ngIf="user.CaminhoFoto == \'\'" src="http://mypolis.com.br/images/avatar-list.png" class="avatar">\n            <span>Trocar</span>\n          </div>\n        </ion-col>\n        <ion-col col-8>\n          <ion-item>\n            <ion-label stacked text-uppercase>Nome</ion-label>\n            <ion-input type="text" value="{{user.Nome}}" [(ngModel)]="user.Nome"></ion-input>\n          </ion-item>\n        </ion-col>\n        <ion-col col-12>\n          <ion-item>\n            <ion-label stacked text-uppercase>E-mail</ion-label>\n            <ion-input type="text" value="{{user.Email}}"[(ngModel)]="user.Email"></ion-input>\n          </ion-item>\n        </ion-col>\n        <ion-col col-12>\n          <ion-item>\n            <ion-label stacked text-uppercase>Celular</ion-label>\n            <ion-input type="text" value="{{user.Celular}}" [(ngModel)]="user.Celular"></ion-input>\n          </ion-item>\n        </ion-col>\n        <ion-col col-12>\n          <ion-item>\n            <ion-label stacked text-uppercase>Telefone Fixo</ion-label>\n            <ion-input type="text" value="{{user.Telefone}}"[(ngModel)]="user.Telefone"></ion-input>\n          </ion-item>\n        </ion-col>\n        <ion-col col-12>\n          <ion-item>\n            <ion-label stacked>Sexo</ion-label>\n            <ion-select [(ngModel)]="user.Sexo" name="Sexo" multiple="false" >\n              <ion-option value="1">Masculino</ion-option>\n              <ion-option value="2">Feminino</ion-option>\n              <ion-option value="0">Outro</ion-option>\n            </ion-select>\n          </ion-item>\n        </ion-col>\n        <ion-col col-12>\n          <ion-item>\n            <ion-label stacked text-uppercase>RG</ion-label>\n            <ion-input type="text" value="{{user.RG}}"[(ngModel)]="user.RG"></ion-input>\n          </ion-item>\n        </ion-col>\n        <ion-col col-12>\n          <ion-item>\n            <ion-label stacked text-uppercase>CPF</ion-label>\n            <ion-input type="text" value="{{user.CPF}}"[(ngModel)]="user.CPF"></ion-input>\n          </ion-item>\n        </ion-col>\n        <ion-col col-12>\n          <ion-item>\n            <ion-label stacked>Estado Civil</ion-label>\n            <ion-select [(ngModel)]="user.EstadoCivil" name="EstadoCivil" multiple="false">\n              <ion-option value="1" selected>Solteiro</ion-option>\n              <ion-option value="2">Casado</ion-option>\n              <ion-option value="3">Separado</ion-option>\n              <ion-option value="4">Divorciado</ion-option>\n              <ion-option value="5">Viúvo</ion-option>\n            </ion-select>\n          </ion-item>\n        </ion-col>\n        <ion-col col-12>\n          <ion-item>\n            <ion-label stacked text-uppercase>Senha</ion-label>\n            <ion-input type="password" [(ngModel)]="user.Senha"></ion-input>\n          </ion-item>\n        </ion-col>\n        <ion-col col-12>\n          <ion-item>\n            <ion-label stacked text-uppercase>Confirme sua senha</ion-label>\n            <ion-input type="password" [(ngModel)]="user.ConfirmarSenha"></ion-input>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n    </div>\n    <div *ngSwitchCase="\'endereco\'">\n      <ion-row>\n        <ion-col col-12>\n          <ion-item>\n            <ion-label stacked>Estado</ion-label>\n            <ion-select [(ngModel)]="user.Estado" multiple="false" (ionChange)="onSelectCidade($event)"  [selectOptions]="selectOptEstados">\n              <ion-option *ngFor="let estado of estados" [value]="estado.sigla">{{estado.nome}}</ion-option>\n            </ion-select>\n          </ion-item>\n        </ion-col>\n        <ion-col col-12>\n          <ion-item>\n            <ion-label stacked>Cidade</ion-label>\n            <ion-select [(ngModel)]="user.Cidade" [selectOptions]="selectOptCidades">\n              <ion-option *ngFor="let cidade of cidades" value="{{cidade}}" [attr.selected]="(user.Cidade == cidade) ? true : null">{{cidade}}</ion-option>\n            </ion-select>\n          </ion-item>\n        </ion-col>\n        <ion-col col-12>\n          <ion-item>\n            <ion-label stacked text-uppercase>Rua</ion-label>\n            <ion-input type="text" value="{{user.Rua}}"[(ngModel)]="user.Rua"></ion-input>\n          </ion-item>\n        </ion-col>\n        <ion-col col-12>\n          <ion-item>\n            <ion-label stacked text-uppercase>Número</ion-label>\n            <ion-input type="text" value="{{user.Numero}}"[(ngModel)]="user.Numero"></ion-input>\n          </ion-item>\n        </ion-col>\n        <ion-col col-12>\n          <ion-item>\n            <ion-label stacked text-uppercase>Complemento</ion-label>\n            <ion-input type="text" value="{{user.Complemento}}"[(ngModel)]="user.Complemento"></ion-input>\n          </ion-item>\n        </ion-col>\n        <ion-col col-12>\n          <ion-item>\n            <ion-label stacked text-uppercase>CEP</ion-label>\n            <ion-input type="text" value="{{user.CEP}}" [(ngModel)]="user.CEP"></ion-input>\n          </ion-item>\n        </ion-col>\n        <ion-col col-12>\n          <ion-item>\n            <ion-label stacked text-uppercase>Bairro</ion-label>\n            <ion-input type="text" value="{{user.Bairro}}"[(ngModel)]="user.Bairro"></ion-input>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n    </div>\n    <div *ngSwitchCase="\'dados-sociais\'">\n      <ion-row>\n        <ion-col col-12>\n          <ion-item>\n            <ion-label stacked text-uppercase>Facebook</ion-label>\n            <ion-input type="text" value="{{user.Facebook}}" [(ngModel)]="user.Facebook"></ion-input>\n          </ion-item>\n        </ion-col>\n        <ion-col col-12>\n          <ion-item>\n            <ion-label stacked text-uppercase>Instagram</ion-label>\n            <ion-input type="text" value="{{user.Instagram}}" [(ngModel)]="user.Instagram"></ion-input>\n          </ion-item>\n        </ion-col>\n        <ion-col col-12>\n          <ion-item>\n            <ion-label stacked text-uppercase>Twitter</ion-label>\n            <ion-input type="text" value="{{user.Twitter}}" [(ngModel)]="user.Twitter"></ion-input>\n          </ion-item>\n        </ion-col>\n      </ion-row>\n    </div>\n    <!-- <div *ngSwitchCase="\'minhas-preferencias\'">\n      <ion-row>\n        <ul class="list-default">\n          <li>\n            <div>\n              <ion-icon name="body" item-start></ion-icon>Política\n            </div>\n            <ion-icon name="heart" class="heart select" item-end></ion-icon>\n          </li>\n          <li>\n            <div><ion-icon name="videocam" item-start></ion-icon>Cinema</div>\n            <ion-icon name="heart" class="heart" item-end></ion-icon>\n          </li>\n          <li>\n            <div><ion-icon name="person" item-start></ion-icon>Interação</div>\n            <ion-icon name="heart" class="heart" item-end></ion-icon>\n          </li>\n          <li>\n            <div><ion-icon name="list-box" item-start></ion-icon>Educação</div>\n            <ion-icon name="heart" class="heart" item-end></ion-icon>\n          </li>\n        </ul>\n      </ion-row>\n    </div> -->\n    <footer margin-top>\n      <button ion-button full text-uppercase (click)="putPerson()">Salvar</button>\n    </footer>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/perfil/perfil.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_person_person__["a" /* PersonProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_utils_utils__["a" /* UtilsProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_camera__["a" /* Camera */]])
    ], PerfilPage);
    return PerfilPage;
}());

//# sourceMappingURL=perfil.js.map

/***/ }),
/* 341 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PersonDetail; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__IModel__ = __webpack_require__(213);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var PersonDetail = /** @class */ (function (_super) {
    __extends(PersonDetail, _super);
    function PersonDetail() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    PersonDetail = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])()
    ], PersonDetail);
    return PersonDetail;
}(__WEBPACK_IMPORTED_MODULE_1__IModel__["a" /* IModel */]));

//# sourceMappingURL=PersonDetail.js.map

/***/ }),
/* 342 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProgramaGovernoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__convide_amigos__ = __webpack_require__(217);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_geral_geral__ = __webpack_require__(218);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular_components_loading_loading_controller__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__models_Conteudo__ = __webpack_require__(343);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__social_share_social_share__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_utils_utils__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_person_person__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_conteudo_conteudo__ = __webpack_require__(23);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var ProgramaGovernoPage = /** @class */ (function () {
    function ProgramaGovernoPage(navCtrl, navParams, modalCtrl, geral, loadingCtrl, utils, person, _conteudo) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.geral = geral;
        this.loadingCtrl = loadingCtrl;
        this.utils = utils;
        this.person = person;
        this._conteudo = _conteudo;
        this.programa = new __WEBPACK_IMPORTED_MODULE_5__models_Conteudo__["a" /* Conteudo */]();
        this.getPrograma();
    }
    ProgramaGovernoPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ProgramaGovernoPage');
    };
    ProgramaGovernoPage.prototype.doRefresh = function (refresher) {
        var _this = this;
        console.log('Begin async operation', refresher);
        setTimeout(function () {
            console.log('Async operation has ended');
            _this.getPrograma();
            refresher.complete();
        }, 1000);
    };
    ProgramaGovernoPage.prototype.inviteFriends = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__convide_amigos__["a" /* ConvideAmigosPage */]);
        modal.present();
    };
    ProgramaGovernoPage.prototype.getPrograma = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: 'Carregando...'
        });
        loading.present();
        this.geral.findAll().then(function (response) {
            loading.dismiss();
            _this.programa = new __WEBPACK_IMPORTED_MODULE_5__models_Conteudo__["a" /* Conteudo */]().fromJSON(response);
            console.log(response);
        }, function (error) {
            loading.dismiss();
            console.log(error);
        });
    };
    ProgramaGovernoPage.prototype.postLike = function (conteudo) {
        var _this = this;
        var idPost = conteudo.Id;
        this.person.get()
            .then(function (response) {
            var person = response;
            _this._conteudo.postLike(idPost, person.Id)
                .then(function (response) {
                if (response.Curtiu && !conteudo.JaCurtiu) {
                    conteudo.Curtidas += 1;
                }
                else {
                    conteudo.Curtidas -= 1;
                }
                conteudo.JaCurtiu = response.Curtiu;
                if (response.GanhouPonto) {
                    _this.utils.showModalSucesso(response.Mensagem, "Continue interagindo", "OK");
                }
            });
        });
    };
    ProgramaGovernoPage.prototype.postShare = function (conteudo) {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_6__social_share_social_share__["a" /* SocialSharePage */], conteudo);
        modal.present();
    };
    ProgramaGovernoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-programa-governo',template:/*ion-inline-start:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/programa-governo/programa-governo.html"*/'<ion-header color="header">\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title text-center>Programa de Governo</ion-title>\n    <ion-buttons end>\n      <!-- <button (click)="searchToggle()" class="search-header">\n      <ion-icon name="search"></ion-icon>\n    </button> -->\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content *ngIf="programa">\n    <ion-refresher (ionRefresh)="doRefresh($event)">\n        <ion-refresher-content></ion-refresher-content>\n      </ion-refresher>\n  <img src="{{programa.Imagem}}" class="img-featured">\n  <div padding>\n    <!-- <header class="buttons-event right grey"> -->\n        <header class="buttons-event right">\n      <button ion-button icon-left clear small [ngClass]="{\'liked\': programa.JaCurtiu}" (click)="postLike(programa)">\n        <ion-icon name="heart"></ion-icon>\n        <div>{{programa.Curtidas}}</div>\n      </button>\n      <button class="share" ion-button icon-right text-left clear small (click)="postShare(programa)">\n        <ion-icon name="share"></ion-icon>\n      </button>\n    </header>\n\n    <div class="block-text" margin-top>\n     <h5 class="bold">{{programa.Titulo}}</h5>\n      <p text-uppercase *ngIf="programa.Chamada">\n        <strong>{{programa.Chamada}}</strong>\n      </p>\n      <p [innerHTML]="programa.Texto"></p>\n    </div>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/programa-governo/programa-governo.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_geral_geral__["a" /* GeralProvider */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular_components_loading_loading_controller__["a" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_7__providers_utils_utils__["a" /* UtilsProvider */],
            __WEBPACK_IMPORTED_MODULE_8__providers_person_person__["a" /* PersonProvider */],
            __WEBPACK_IMPORTED_MODULE_9__providers_conteudo_conteudo__["a" /* ConteudoProvider */]])
    ], ProgramaGovernoPage);
    return ProgramaGovernoPage;
}());

//# sourceMappingURL=programa-governo.js.map

/***/ }),
/* 343 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Conteudo; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__IModel__ = __webpack_require__(213);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var Conteudo = /** @class */ (function (_super) {
    __extends(Conteudo, _super);
    function Conteudo() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Conteudo = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])()
    ], Conteudo);
    return Conteudo;
}(__WEBPACK_IMPORTED_MODULE_1__IModel__["a" /* IModel */]));

//# sourceMappingURL=Conteudo.js.map

/***/ }),
/* 344 */,
/* 345 */,
/* 346 */,
/* 347 */,
/* 348 */,
/* 349 */,
/* 350 */,
/* 351 */,
/* 352 */,
/* 353 */,
/* 354 */,
/* 355 */,
/* 356 */,
/* 357 */,
/* 358 */,
/* 359 */,
/* 360 */,
/* 361 */,
/* 362 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(263);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(264);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_network__ = __webpack_require__(265);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_comites_comites__ = __webpack_require__(186);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_pesquisa_pesquisa__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_home_home__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_material_institucional_material_institucional__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_programa_pontos_programa_pontos__ = __webpack_require__(220);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_tutorial_tutorial__ = __webpack_require__(266);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_login_login__ = __webpack_require__(121);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__providers_auth_auth__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__providers_person_person__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_list_programa_list_programa__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_eleicoes_oque_posso_fazer_eleicoes_oque_posso_fazer__ = __webpack_require__(197);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_eleicoes_data_prazos_eleicoes_data_prazos__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_conheca_partido_conheca_partido__ = __webpack_require__(191);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__providers_notificacao_notificacao__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_list_candidatos_list_candidatos__ = __webpack_require__(199);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





















var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen, auth, person, notification, loadingCtrl, alertCtrl, events, network) {
        var _this = this;
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.auth = auth;
        this.person = person;
        this.notification = notification;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.events = events;
        this.network = network;
        this.disconnect = false;
        events.subscribe('profile:count', function (profile, count) {
            _this.profile = profile;
            _this.count = count;
        });
        events.subscribe('menu:closed', function () {
            // your action here
        });
        this.initializeApp();
        this.network.onDisconnect().subscribe(function () {
            _this.disconnect = true;
            _this.onDisconnect();
        });
    }
    MyApp.prototype.onDisconnect = function () {
        console.log('Sem conexão com internet. O conteúdo mais recente não poderá ser exibido. :-(');
        this.alertCtrl.create({
            // title: this.service.config.name,
            // subTitle: 'Sem conexão com internet. O conteúdo mais recente não poderá ser exibido.',
            // buttons: [{ text: 'Ok' }]
            title: 'Sem internet',
            subTitle: 'O conteúdo mais recente não poderá ser exibido.',
            buttons: ['OK'],
            cssClass: 'alertCustomCssError'
        }).present();
    };
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
            if (_this.auth.isLogged()) {
                _this.preencherPages();
                _this.rootPage = __WEBPACK_IMPORTED_MODULE_7__pages_home_home__["a" /* HomePage */];
            }
            else {
                _this.preencherPages();
                _this.rootPage = __WEBPACK_IMPORTED_MODULE_10__pages_tutorial_tutorial__["a" /* TutorialPage */];
            }
        });
    };
    MyApp.prototype.openPage = function (page, params) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.setRoot(page.component, params);
    };
    MyApp.prototype.openPageHistorico = function () {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_9__pages_programa_pontos_programa_pontos__["a" /* ProgramaPontosPage */], 'historico');
    };
    MyApp.prototype.openPagePush = function (page, params) {
        this.nav.push(page, params);
    };
    MyApp.prototype.logoutApp = function () {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_11__pages_login_login__["a" /* LoginPage */]);
    };
    MyApp.prototype.preencherPages = function () {
        this.pages = [
            { title: 'Quero doar', component: __WEBPACK_IMPORTED_MODULE_19__pages_list_candidatos_list_candidatos__["a" /* ListCandidatosPage */], icon: 'fa fa-usd', description: null, cssClass: 'quero-contribuir' },
            { title: 'Início', component: __WEBPACK_IMPORTED_MODULE_7__pages_home_home__["a" /* HomePage */], icon: 'fa fa-home', cssClass: 'iniciocss' },
            { title: 'Dois anos de MDB no Governo', component: __WEBPACK_IMPORTED_MODULE_14__pages_list_programa_list_programa__["a" /* ListProgramaPage */], icon: 'fa fa-star', },
            { title: 'Eleições 2018', component: __WEBPACK_IMPORTED_MODULE_15__pages_eleicoes_oque_posso_fazer_eleicoes_oque_posso_fazer__["a" /* EleicoesOquePossoFazerPage */], icon: 'fa fa-info-circle', description: 'O que posso fazer?' },
            { title: 'Eleições 2018', component: __WEBPACK_IMPORTED_MODULE_16__pages_eleicoes_data_prazos_eleicoes_data_prazos__["a" /* EleicoesDataPrazosPage */], icon: 'fa fa-calendar', description: 'Prazos e datas importantes' },
            { title: 'Conheça o MDB', component: __WEBPACK_IMPORTED_MODULE_17__pages_conheca_partido_conheca_partido__["a" /* ConhecaPartidoPage */], icon: 'fa fa-flag ', description: null },
            { title: 'Material Institucional', component: __WEBPACK_IMPORTED_MODULE_8__pages_material_institucional_material_institucional__["a" /* MaterialInstitucionalPage */], icon: 'fa fa-picture-o', description: 'Banners, post...' },
            { title: 'Diretórios', component: __WEBPACK_IMPORTED_MODULE_5__pages_comites_comites__["a" /* ComitesPage */], icon: 'fa fa-globe' },
            { title: 'Pesquisa de Opinião', component: __WEBPACK_IMPORTED_MODULE_6__pages_pesquisa_pesquisa__["b" /* PesquisaPage */], icon: 'fa fa-comment', description: null },
        ];
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/app/app.html"*/'<ion-menu [content]="content">\n  <ion-header color="header">\n    <ion-toolbar class="menu-header">\n     \n    </ion-toolbar>\n\n  </ion-header>\n\n  <ion-content>\n    <style>\n      .list-ios>.item-block {\n        border-radius: 0 !important;\n        margin-bottom: 0 !important;\n      }\n      .iniciocss .pzin{\n\n    margin-left: -14%!important;\n      }\n\n      .item-ios {\n        padding-left: 0;\n      }\n      /* ion-icon{\n        font-size: 1.5em;\n      } */\n      .quero-contribuir{\n        padding-top: 3% !important;\n        background:#f9f9f9;\n        opacity: 190;\n        padding-bottom: 0% !important;\n      }\n      \n      .quero-contribuir p{\n        color: #333!important;\n      }\n    </style>\n    <ion-list>\n      <button menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)" [ngClass]="p.cssClass ? p.cssClass : \'\'">\n        <i class="{{p.icon}}" style="float: left; margin: -1px 15px 2px 0px; font-size: 19px; width: 10%;"></i> \n        <p style="float: left;color:#333;">{{p.title}}<br *ngIf="p.description!=null">\n        <span  class="pzin" *ngIf="p.description!=null"><small style="padding-left: 0;\n        margin: 0;" >{{p.description}}</small></span></p>\n      </button>\n    </ion-list>\n  </ion-content>\n\n  <ion-footer class="socials">\n    <ion-toolbar color="white">\n      <div class="flex">\n        <nav>\n          <a href="#" target="_blank">\n            <img src="assets/imgs/ico-facebook.png">\n          </a>\n          <a href="#" target="_blank">\n            <img src="assets/imgs/ico-twitter.png">\n          </a>\n          <a href="#" target="_blank">\n            <img src="assets/imgs/ico-youtube.png">\n          </a>\n          <a href="#" target="_blank">\n            <img src="assets/imgs/ico-instagram.png">\n          </a>\n        </nav>\n        <a href="#" class="signature" target="_blank">\n          <img src="assets/imgs/logomdbMenu.png">\n        </a>\n      </div>\n    </ion-toolbar>\n  </ion-footer>\n\n</ion-menu>\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>'/*ion-inline-end:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_12__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_13__providers_person_person__["a" /* PersonProvider */],
            __WEBPACK_IMPORTED_MODULE_18__providers_notificacao_notificacao__["a" /* NotificationProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_network__["a" /* Network */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),
/* 363 */,
/* 364 */,
/* 365 */,
/* 366 */,
/* 367 */,
/* 368 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VoluntarioPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__voluntario__ = __webpack_require__(369);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__indique_amigos__ = __webpack_require__(268);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var VoluntarioPageModule = /** @class */ (function () {
    function VoluntarioPageModule() {
    }
    VoluntarioPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__voluntario__["a" /* VoluntarioPage */],
                __WEBPACK_IMPORTED_MODULE_3__indique_amigos__["a" /* IndiqueAmigosPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__voluntario__["a" /* VoluntarioPage */]),
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_3__indique_amigos__["a" /* IndiqueAmigosPage */])
            ],
        })
    ], VoluntarioPageModule);
    return VoluntarioPageModule;
}());

//# sourceMappingURL=voluntario.module.js.map

/***/ }),
/* 369 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VoluntarioPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_searchbar_searchbar__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_voluntario_voluntario__ = __webpack_require__(124);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular_components_loading_loading_controller__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__social_share_social_share__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_utils_utils__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_person_person__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_conteudo_conteudo__ = __webpack_require__(23);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var VoluntarioPage = /** @class */ (function () {
    function VoluntarioPage(navCtrl, navParams, voluntario, loadingCtrl, utils, person, modalCtrl, _conteudo) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.voluntario = voluntario;
        this.loadingCtrl = loadingCtrl;
        this.utils = utils;
        this.person = person;
        this.modalCtrl = modalCtrl;
        this._conteudo = _conteudo;
        this.getData();
    }
    VoluntarioPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad VoluntarioPage');
    };
    VoluntarioPage.prototype.openDetail = function (page, id) {
        this.navCtrl.push(page, id);
    };
    VoluntarioPage.prototype.searchToggle = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__components_searchbar_searchbar__["a" /* SearchbarComponent */]);
        modal.present();
    };
    VoluntarioPage.prototype.doRefresh = function (refresher) {
        var _this = this;
        console.log('Begin async operation', refresher);
        setTimeout(function () {
            console.log('Async operation has ended');
            _this.getData();
            refresher.complete();
        }, 1000);
    };
    VoluntarioPage.prototype.getData = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: 'Carregando...'
        });
        loading.present();
        this.voluntario.findAll().then(function (result) {
            loading.dismiss();
            _this.listSejaVoluntario = result;
            console.log(result);
        }, function (error) {
            loading.dismiss();
            console.log(error);
        });
    };
    VoluntarioPage.prototype.postLike = function (conteudo) {
        var _this = this;
        var idPost = conteudo.Id;
        this.person.get()
            .then(function (response) {
            var person = response;
            _this._conteudo.postLike(idPost, person.Id)
                .then(function (response) {
                if (response.Curtiu && !conteudo.JaCurtiu) {
                    conteudo.Curtidas += 1;
                }
                else {
                    conteudo.Curtidas -= 1;
                }
                conteudo.JaCurtiu = response.Curtiu;
                if (response.GanhouPonto) {
                    _this.utils.showModalSucesso(response.Mensagem, "Continue interagindo", "OK");
                }
            });
        });
    };
    VoluntarioPage.prototype.postShare = function (conteudo) {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_5__social_share_social_share__["a" /* SocialSharePage */], conteudo);
        modal.present();
    };
    VoluntarioPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-voluntario',template:/*ion-inline-start:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/voluntario/voluntario.html"*/'<ion-header color="header">\n  \n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title text-center>Seja um Voluntário</ion-title>\n    <ion-buttons end>\n      <!-- <button (click)="searchToggle()" class="search-header">\n      <ion-icon name="search"></ion-icon>\n    </button> -->\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n    <ion-refresher (ionRefresh)="doRefresh($event)">\n        <ion-refresher-content></ion-refresher-content>\n      </ion-refresher>\n  <ion-list margin-top>\n    <ion-card (click)="openDetail(\'voluntario-detalhe\', v.Id)" *ngFor="let v of listSejaVoluntario">\n      <ion-card-content>\n        <div>\n          <ion-card-title>{{v.Titulo}}</ion-card-title>\n          <p>{{v.Chamada}}</p>\n        </div>\n      </ion-card-content>\n    </ion-card>\n  </ion-list>\n</ion-content>\n'/*ion-inline-end:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/voluntario/voluntario.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__providers_voluntario_voluntario__["a" /* VoluntarioProvider */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular_components_loading_loading_controller__["a" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_6__providers_utils_utils__["a" /* UtilsProvider */],
            __WEBPACK_IMPORTED_MODULE_7__providers_person_person__["a" /* PersonProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_8__providers_conteudo_conteudo__["a" /* ConteudoProvider */]])
    ], VoluntarioPage);
    return VoluntarioPage;
}());

//# sourceMappingURL=voluntario.js.map

/***/ }),
/* 370 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ParticipacaoPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__participacao__ = __webpack_require__(371);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__participacao_enviar_participacao_participacao_enviar_participacao_module__ = __webpack_require__(211);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var ParticipacaoPageModule = /** @class */ (function () {
    function ParticipacaoPageModule() {
    }
    ParticipacaoPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__participacao__["a" /* ParticipacaoPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_3__participacao_enviar_participacao_participacao_enviar_participacao_module__["ParticipacaoEnviarParticipacaoPageModule"],
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__participacao__["a" /* ParticipacaoPage */]),
            ],
        })
    ], ParticipacaoPageModule);
    return ParticipacaoPageModule;
}());

//# sourceMappingURL=participacao.module.js.map

/***/ }),
/* 371 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ParticipacaoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ParticipacaoPage = /** @class */ (function () {
    function ParticipacaoPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ParticipacaoPage.prototype.doRefresh = function (refresher) {
        console.log('Begin async operation', refresher);
        setTimeout(function () {
            console.log('Async operation has ended');
            refresher.complete();
        }, 1000);
    };
    ParticipacaoPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ParticipacaoPage');
    };
    ParticipacaoPage.prototype.openDetail = function (page, id) {
        this.navCtrl.push(page, id);
    };
    ParticipacaoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-participacao',template:/*ion-inline-start:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/participacao/participacao.html"*/'<ion-header color="header">\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title text-center>Minha Participação</ion-title>\n    <ion-buttons end>\n      <!-- <button (click)="searchToggle()" class="search-header">\n      <ion-icon name="search"></ion-icon>\n    </button> -->\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n    <ion-refresher (ionRefresh)="doRefresh($event)">\n        <ion-refresher-content></ion-refresher-content>\n      </ion-refresher>\n  <h4 class="bold">Colabore com a mudança do Brasil</h4>\n  <div class="block-text">\n    <p>Mostre suas ideias e os problemas que acontecem em sua cidade.</p>\n  </div>\n\n  <button ion-button full text-uppercase (click)="openDetail(\'form-participacao\', 1)">Enviar Participação</button>\n\n  <div class="block" margin-top>\n    <h6 class="bold">Minhas Contribuições</h6>\n    <ion-list>\n      <ion-card (click)="openDetail(\'participacao-detalhe\', 1)">\n        <ion-card-content class="flex-card">\n          <div>\n            <small text-uppercase class="blue-color">Segurança</small>\n            <ion-card-title>Rio de Janeiro - RJ</ion-card-title>\n            <ion-note>10 de Janeiro de 2018</ion-note>\n          </div>\n          <ion-icon class="set" name="ios-arrow-forward"></ion-icon>\n        </ion-card-content>\n      </ion-card>\n      <ion-card (click)="openDetail(\'participacao-detalhe\', 1)">\n        <ion-card-content class="flex-card">\n          <div>\n            <small text-uppercase class="blue-color">Transporte Público</small>\n            <ion-card-title>São Paulo - SP</ion-card-title>\n            <ion-note>12 de fevereiro de 2018</ion-note>\n          </div>\n          <ion-icon class="set" name="ios-arrow-forward"></ion-icon>\n        </ion-card-content>\n      </ion-card>\n      <ion-card (click)="openDetail(\'participacao-detalhe\', 1)">\n        <ion-card-content class="flex-card">\n          <div>\n            <small text-uppercase class="blue-color">Ordem Pública</small>\n            <ion-card-title>Belo Horizonte - MG</ion-card-title>\n            <ion-note>15 de março de 2018</ion-note>\n          </div>\n          <ion-icon class="set" name="ios-arrow-forward"></ion-icon>\n        </ion-card-content>\n      </ion-card>\n      <ion-card (click)="openDetail(\'participacao-detalhe\', 1)">\n        <ion-card-content class="flex-card">\n          <div>\n            <small text-uppercase class="blue-color">Outros</small>\n            <ion-card-title>Brasília - SF</ion-card-title>\n            <ion-note>09 de abril de 2018</ion-note>\n          </div>\n          <ion-icon class="set" name="ios-arrow-forward"></ion-icon>\n        </ion-card-content>\n      </ion-card>\n    </ion-list>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/participacao/participacao.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */]])
    ], ParticipacaoPage);
    return ParticipacaoPage;
}());

//# sourceMappingURL=participacao.js.map

/***/ }),
/* 372 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TutorialPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__tutorial__ = __webpack_require__(266);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var TutorialPageModule = /** @class */ (function () {
    function TutorialPageModule() {
    }
    TutorialPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__tutorial__["a" /* TutorialPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__tutorial__["a" /* TutorialPage */]),
            ],
        })
    ], TutorialPageModule);
    return TutorialPageModule;
}());

//# sourceMappingURL=tutorial.module.js.map

/***/ })
],[269]);
//# sourceMappingURL=main.js.map