webpackJsonp([8],{

/***/ 376:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CadastroPageModule", function() { return CadastroPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__cadastro__ = __webpack_require__(387);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_brmasker_ionic_3__ = __webpack_require__(64);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var CadastroPageModule = /** @class */ (function () {
    function CadastroPageModule() {
    }
    CadastroPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__cadastro__["a" /* CadastroPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__cadastro__["a" /* CadastroPage */]),
                __WEBPACK_IMPORTED_MODULE_3_brmasker_ionic_3__["a" /* BrMaskerModule */]
            ],
        })
    ], CadastroPageModule);
    return CadastroPageModule;
}());

//# sourceMappingURL=cadastro.module.js.map

/***/ }),

/***/ 387:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CadastroPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_person_person__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__login_login__ = __webpack_require__(121);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_brmasker_ionic_3__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_app_config__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_http__ = __webpack_require__(267);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the CadastroPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CadastroPage = /** @class */ (function () {
    //public maskdate = [/[0-9]/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/];
    //public maskcel = ['(', /[0-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, /\d/];
    //public masktel = ['(', /[0-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, /\d/];
    function CadastroPage(navCtrl, navParams, loadingCtrl, alertCtrl, person, http) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.person = person;
        this.http = http;
        this.maskdate = [/[0-9]/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/];
        this.masktel = ['(', /[0-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
        this.Formulario = false;
        this.Sucesso = false;
        this.form = {
            RoleId: 2,
            Nome: '',
            Email: '',
            Numero: '',
            Senha: '',
            ConfirmarSenha: '',
            clienteId: 4
        };
    }
    CadastroPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CadastroPage');
    };
    CadastroPage.prototype.goPage = function (page) {
        this.navCtrl.push(page);
    };
    CadastroPage.prototype.onRegister = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: 'Carregando...'
        });
        var alert = this.alertCtrl.create({
            title: "OK",
            buttons: [
                {
                    text: 'Fechar',
                    role: 'cancel'
                }
            ]
        });
        var headers = new __WEBPACK_IMPORTED_MODULE_6__angular_http__["a" /* Headers */]({});
        console.log(headers);
        var options = new __WEBPACK_IMPORTED_MODULE_6__angular_http__["d" /* RequestOptions */]({ headers: headers });
        loading.present();
        var body = {
            Nome: this.form.Nome,
            Email: this.form.Email,
            Celular: this.form.Numero,
            Senha: this.form.Senha,
            ConfirmarSenha: this.form.ConfirmarSenha,
            RoleId: '2',
            clienteId: 4,
        };
        return this.http.post("" + __WEBPACK_IMPORTED_MODULE_5__providers_app_config__["a" /* CONFIG_PROJECT */].baseApi + '/Pessoa/CadastroBasico', body, { headers: headers }).map(function (res) { return res.json(); })
            .subscribe(function (data) {
            if (data.success == true) {
                loading.dismiss(),
                    _this.alertCtrl.create({
                        title: 'Parabéns',
                        subTitle: data.Mensagem,
                        buttons: [{ text: 'Ok' }],
                        cssClass: 'alertCustomCssError'
                    }).present();
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__login_login__["a" /* LoginPage */]);
                return false;
            }
            else {
                _this.alertCtrl.create({
                    title: 'Erro',
                    subTitle: data.Descricao,
                    buttons: [{ text: 'Ok' }],
                    cssClass: 'alertCustomCssError'
                }).present();
                loading.dismiss();
            }
        }, function (err) {
            console.log("ERROR!: ", err);
            //  this.loading.dismiss();ion
            var mensagem = err._body.replace('{', '').replace('"Message"', '').replace(':', '').replace('}', '').replace('"', '').replace('"', '');
            if (mensagem.indexOf('encontra cadastrado') != -1) {
                _this.alertCtrl.create({
                    title: 'Erro',
                    subTitle: "Esse e-mail já se encontra cadastrado. Informe outro e-mail ou <span (click)='EsqueciPage()'>clique aqui</span> caso tenha esquecido sua senha.",
                    buttons: [{ text: 'Ok' }],
                    cssClass: 'alertCustomCssError'
                }).present();
                loading.dismiss();
            }
            else {
                _this.alertCtrl.create({
                    title: 'Erro',
                    subTitle: mensagem,
                    buttons: [{ text: 'Ok' }],
                    cssClass: 'alertCustomCssError'
                }).present();
                loading.dismiss();
            }
        });
        // this.person.post(model)
        //   .then((response) => {
        //     setTimeout(() => {
        //       loading.dismiss();
        //       this.navCtrl.setRoot(HomePage);
        //     }, 2000);
        //   }, (error) => {
        //     setTimeout(() => {
        //       loading.dismiss();
        //       alert.setTitle(error.error.Message);
        //       alert.present();
        //     }, 2000);
        //   });
    };
    CadastroPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-cadastro',template:/*ion-inline-start:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/cadastro/cadastro.html"*/'<ion-content>\n  <header class="login-header background" text-center>\n    <img src="assets/imgs/logo.png" class="brand">\n      <h3 class="title-blue">Cadastro</h3>\n  </header>\n  <div padding>\n    <div class="block-text" text-center>\n      <p>Preencha os dados abaixo. <br/> É bem simples e rápido!</p>\n    </div>\n    <div class="container-stage">\n      <div class="text">\n        <strong>Preencha seus dados iniciais:</strong>\n        <p>*Todos os dados devem ser preenchidos</p>\n      </div>\n    </div>\n    <form autocomplete="off" #loginform="ngForm">\n    <ion-list>\n      <ion-item class="input-block-color">\n        <ion-input type="text" placeholder="Nome" [(ngModel)]="form.Nome" name="Nome"></ion-input>\n      </ion-item>\n\n      <ion-item class="input-block-color">\n        <ion-input type="email" placeholder="E-mail" [(ngModel)]="form.Email" name="Email"></ion-input>\n      </ion-item>\n\n      <ion-item class="input-block-color">\n\n        <!-- <ion-input placeholder="Telefone"  [textMask]="{mask: maskcel}" type="tel" [(ngModel)]="form.Numero" name="Numero"></ion-input>  -->\n        <ion-input placeholder="Telefone"  [brmasker]="{phone: true}" type="tel" [(ngModel)]="form.Numero" name="Numero"></ion-input> \n\n      </ion-item>\n\n      <ion-item class="input-block-color" style="  margin-bottom: 0!important;    border-radius: 10px 10px 0px 0px;">\n        <ion-input type="password" placeholder="Senha" [(ngModel)]="form.Senha" name="Senha"></ion-input>\n      </ion-item>\n\n      <ion-item class="input-block-color" style="  border-radius: 0px 0px 10px 10px;border-top: 0.55px solid #c8c7cc;    border-bottom: 0;">\n        <ion-input type="password" placeholder="Confirmar senha" [(ngModel)]="form.ConfirmarSenha" name="ConfirmarSenha"> </ion-input>\n      </ion-item>\n\n      <!-- <ion-item margin-top class="checkbox" style="background-color: transparent;border-bottom:none">\n        <ion-label><strong>Li</strong> e <strong>aceito</strong> os termos de uso</ion-label>\n        <ion-checkbox checked="true" [(ngModel)]="model.CheckTermo" name="CheckTermo"></ion-checkbox>\n      </ion-item> -->\n\n    </ion-list>\n    <button ion-button full text-uppercase (click)="onRegister()" >Enviar</button>\n  </form>\n    <button ion-button outline full text-uppercase margin-top>\n      <img class="ico-general-button" src="assets/imgs/ico-facebook-blue.png" width="11" height="19"> Logar pelo Facebook\n    </button>\n  </div>\n\n</ion-content>\n<ion-footer>\n  <ion-toolbar (click)="goPage(\'login\')" text-center style="color:#fff">\n    Voltar a tela de <strong> Login</strong>\n  </ion-toolbar>\n</ion-footer>\n\n\n'/*ion-inline-end:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/cadastro/cadastro.html"*/,
        }),
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_4_brmasker_ionic_3__["a" /* BrMaskerModule */]
            ]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_person_person__["a" /* PersonProvider */],
            __WEBPACK_IMPORTED_MODULE_6__angular_http__["b" /* Http */]])
    ], CadastroPage);
    return CadastroPage;
}());

//# sourceMappingURL=cadastro.js.map

/***/ })

});
//# sourceMappingURL=8.js.map