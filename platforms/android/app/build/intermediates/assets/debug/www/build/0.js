webpackJsonp([0],{

/***/ 378:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EventoPageModule", function() { return EventoPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__evento__ = __webpack_require__(389);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__modal_amigos__ = __webpack_require__(384);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var EventoPageModule = /** @class */ (function () {
    function EventoPageModule() {
    }
    EventoPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__evento__["a" /* EventoPage */],
                __WEBPACK_IMPORTED_MODULE_3__modal_amigos__["a" /* EventosAmigosPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__evento__["a" /* EventoPage */])
            ],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_2__evento__["a" /* EventoPage */],
                __WEBPACK_IMPORTED_MODULE_3__modal_amigos__["a" /* EventosAmigosPage */]
            ]
        })
    ], EventoPageModule);
    return EventoPageModule;
}());

//# sourceMappingURL=evento.module.js.map

/***/ }),

/***/ 384:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EventosAmigosPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var EventosAmigosPage = /** @class */ (function () {
    function EventosAmigosPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    EventosAmigosPage.prototype.ionViewDidLoad = function () {
    };
    EventosAmigosPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'amigos-evento',template:/*ion-inline-start:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/evento/modal-amigos.html"*/'<div class="container-round" text-center>\n  <header>\n    <h3 class="title-blue">Amigos que comparecerão</h3>\n    <p>Seus amigos do Facebook</p>\n  </header>\n  <ion-list>\n      <ion-item>\n        <ion-avatar item-start>\n            <img src="http://via.placeholder.com/45x45" class="avatar">\n            <ion-icon name="checkmark-circle"></ion-icon>\n        </ion-avatar>\n        <h2>Nome da Pessoa</h2>\n      </ion-item>  \n      <ion-item>\n        <ion-avatar item-start>\n            <img src="http://via.placeholder.com/45x45" class="avatar">\n            <ion-icon name="checkmark-circle"></ion-icon>\n        </ion-avatar>\n        <h2>Nome da Pessoa</h2>\n      </ion-item>\n      <ion-item>\n        <ion-avatar item-start>\n            <img src="http://via.placeholder.com/45x45" class="avatar">\n            <ion-icon name="checkmark-circle"></ion-icon>\n        </ion-avatar>\n        <h2>Nome da Pessoa</h2>\n      </ion-item>  \n      <ion-item>\n        <ion-avatar item-start>\n            <img src="http://via.placeholder.com/45x45" class="avatar">\n            <ion-icon name="checkmark-circle"></ion-icon>\n        </ion-avatar>\n        <h2>Nome da Pessoa</h2>\n      </ion-item>\n      <ion-item>\n        <ion-avatar item-start>\n            <img src="http://via.placeholder.com/45x45" class="avatar">\n            <ion-icon name="checkmark-circle"></ion-icon>\n        </ion-avatar>\n        <h2>Nome da Pessoa</h2>\n      </ion-item>  \n      <ion-item>\n        <ion-avatar item-start>\n            <img src="http://via.placeholder.com/45x45" class="avatar">\n            <ion-icon name="checkmark-circle"></ion-icon>\n        </ion-avatar>\n        <h2>Nome da Pessoa</h2>\n      </ion-item>\n      <ion-item>\n        <ion-avatar item-start>\n            <img src="http://via.placeholder.com/45x45" class="avatar">\n            <ion-icon name="checkmark-circle"></ion-icon>\n        </ion-avatar>\n        <h2>Nome da Pessoa</h2>\n      </ion-item>  \n      <ion-item>\n        <ion-avatar item-start>\n            <img src="http://via.placeholder.com/45x45" class="avatar">\n            <ion-icon name="checkmark-circle"></ion-icon>\n        </ion-avatar>\n        <h2>Nome da Pessoa</h2>\n      </ion-item>  \n  </ion-list>\n</div>\n  \n  '/*ion-inline-end:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/evento/modal-amigos.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */]])
    ], EventosAmigosPage);
    return EventosAmigosPage;
}());

//# sourceMappingURL=modal-amigos.js.map

/***/ }),

/***/ 389:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EventoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__modal_amigos__ = __webpack_require__(384);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_searchbar_searchbar__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_auth_auth__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_agenda_agenda__ = __webpack_require__(122);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_person_person__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_utils_utils__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_conteudo_conteudo__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__social_share_social_share__ = __webpack_require__(40);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var EventoPage = /** @class */ (function () {
    function EventoPage(navCtrl, navParams, modalCtrl, alertCtrl, agenda, auth, utils, person, _conteudo) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.alertCtrl = alertCtrl;
        this.agenda = agenda;
        this.auth = auth;
        this.utils = utils;
        this.person = person;
        this._conteudo = _conteudo;
        this.start = 3;
        this.skip = 0;
        this.getData();
    }
    EventoPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad EventoPage');
    };
    EventoPage.prototype.openDetail = function (page, id) {
        this.navCtrl.push(page, { id: id });
    };
    EventoPage.prototype.showAllFriends = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__modal_amigos__["a" /* EventosAmigosPage */]);
        modal.present();
    };
    EventoPage.prototype.searchToggle = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__components_searchbar_searchbar__["a" /* SearchbarComponent */]);
        modal.present();
    };
    EventoPage.prototype.confirmEvent = function () {
        var confirm = this.alertCtrl.create({
            title: 'Confirmar presença no evento?',
            buttons: [
                {
                    text: 'Sim eu irei!',
                    handler: function () {
                    }
                },
                {
                    text: 'Cancelar',
                    handler: function () {
                    }
                }
            ]
        });
        confirm.present();
    };
    EventoPage.prototype.getData = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.agenda.getDetalhe(_this.auth.getToken(), _this.navParams.data.id)
                .then(function (data) {
                _this.evento = data;
                console.log(data);
                _this.getOutros(data.Id);
                resolve(true);
            });
        });
    };
    EventoPage.prototype.getOutros = function (id) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.agenda.getLista(_this.auth.getToken(), _this.start, _this.skip)
                .then(function (data) {
                var lista = [];
                for (var i = 0; i < data.length; i++) {
                    if (data[i].Id != id) {
                        lista.push(data[i]);
                    }
                }
                _this.outros = lista;
                resolve(true);
            });
        });
    };
    EventoPage.prototype.postLike = function (conteudo) {
        var _this = this;
        var idPost = conteudo.Id;
        this.person.get()
            .then(function (response) {
            var person = response;
            _this._conteudo.postLike(idPost, person.Id)
                .then(function (response) {
                if (response.Curtiu && !conteudo.JaCurtiu) {
                    conteudo.Curtidas += 1;
                }
                else {
                    conteudo.Curtidas -= 1;
                }
                conteudo.JaCurtiu = response.Curtiu;
                if (response.GanhouPonto) {
                    _this.utils.showModalSucesso(response.Mensagem, "Continue interagindo", "OK");
                }
            });
        });
    };
    EventoPage.prototype.postShare = function (conteudo) {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_9__social_share_social_share__["a" /* SocialSharePage */], conteudo);
        modal.present();
    };
    EventoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-evento',template:/*ion-inline-start:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/evento/evento.html"*/'<ion-header color="header">\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title text-center>Evento</ion-title>\n    <ion-buttons end>\n      <!-- <button (click)="searchToggle()" class="search-header">\n      <ion-icon name="search"></ion-icon>\n    </button> -->\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content *ngIf="evento">\n  <img src="{{evento.Imagem}}" class="img-featured">\n  <div padding>\n    <header class="buttons-event">\n      <ion-row>\n        <ion-col>\n          <div>\n            <h4 class="bold">{{evento.Titulo}}</h4>\n            <ion-note><ion-icon name="pin"></ion-icon> {{evento.Local}} </ion-note>\n          </div>\n        </ion-col>\n        <ion-col text-right col-3 col-md-2>\n          <button ion-button icon-left clear small (click)="postLike(evento)" [ngClass]="{\'liked\': evento.JaCurtiu}">\n            <ion-icon name="heart"></ion-icon>\n            <div>{{evento.Curtidas}}</div>\n          </button>\n          <button class="share" ion-button icon-right text-left clear small (click)="postShare(evento)">\n            <ion-icon name="share"></ion-icon>\n          </button>\n        </ion-col>\n      </ion-row>\n    </header>\n    <section>\n      <p class="date-event"><ion-icon name="calendar"></ion-icon> {{evento.DataBR}}, às {{evento.Hora}}</p>\n      <!-- <section class="friends-event">\n        <strong>Amigos do Facebook que confirmaram:</strong>\n        <div class="row-box">\n          <ul class="list-friends">\n            <li>\n              <img src="assets/imgs/avatar.jpg">\n              <ion-icon name="checkmark-circle"></ion-icon>\n            </li>\n            <li>\n              <img src="assets/imgs/avatar.jpg">\n              <ion-icon name="checkmark-circle"></ion-icon>\n            </li>\n            <li>\n              <img src="assets/imgs/avatar.jpg">\n              <ion-icon name="checkmark-circle"></ion-icon>\n            </li>\n            <li class="count" (click)="showAllFriends()">\n              <span>+10</span>\n            </li>\n          </ul>\n          <button ion-button (click)="confirmEvent()">Eu vou!</button>\n        </div>\n      </section> -->\n      <div class="block-text" margin-top>\n        <p [innerHtml]="evento.Texto"></p>\n      </div>\n    </section>\n    <section class="padding-section">\n      <h5 class="bold">Veja outros eventos</h5>\n      <ion-slides pager spaceBetween="-40" class="list-cards">\n        <ion-slide *ngFor="let outro of outros">\n          <ion-card (click)="openDetail(\'evento\',outro.Id)">\n            <div class="row">\n              <div class="hour">23 <strong>mai</strong> <small> 10h </small></div>\n              <div class="col" text-left>\n                <ion-card-content>\n                  <ion-card-title>{{outro.Titulo}}</ion-card-title>\n                  {{outro.Local}}\n                </ion-card-content>\n              </div>\n            </div>\n          </ion-card>\n        </ion-slide>\n      </ion-slides>\n\n    </section>\n  </div>\n</ion-content>\n\n'/*ion-inline-end:"/Users/matheuscarvalho/Documents/Projetos/MDB-maisquevoto/src/pages/evento/evento.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_5__providers_agenda_agenda__["a" /* AgendaProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_7__providers_utils_utils__["a" /* UtilsProvider */],
            __WEBPACK_IMPORTED_MODULE_6__providers_person_person__["a" /* PersonProvider */],
            __WEBPACK_IMPORTED_MODULE_8__providers_conteudo_conteudo__["a" /* ConteudoProvider */]])
    ], EventoPage);
    return EventoPage;
}());

//# sourceMappingURL=evento.js.map

/***/ })

});
//# sourceMappingURL=0.js.map